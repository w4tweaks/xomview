object CompareTree: TCompareTree
  Left = 543
  Top = 330
  Width = 382
  Height = 360
  BorderIcons = []
  Caption = 'Compare Tree'
  Color = clBtnFace
  Constraints.MinHeight = 150
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 374
    Height = 283
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 1
      Width = 3
      Height = 281
      Cursor = crHSplit
    end
    object InTree: TTreeView
      Left = 1
      Top = 1
      Width = 184
      Height = 281
      Align = alLeft
      Indent = 19
      ReadOnly = True
      TabOrder = 0
    end
    object OutTree: TTreeView
      Left = 188
      Top = 1
      Width = 185
      Height = 281
      Align = alClient
      Indent = 19
      ReadOnly = True
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 283
    Width = 374
    Height = 50
    Align = alBottom
    Constraints.MinHeight = 50
    Constraints.MinWidth = 374
    TabOrder = 1
    object Button2: TButton
      Left = 240
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Close'
      ModalResult = 2
      TabOrder = 0
    end
    object Button1: TButton
      Left = 64
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Import'
      Enabled = False
      ModalResult = 1
      TabOrder = 1
    end
  end
end
