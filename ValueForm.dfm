object ChgValForm: TChgValForm
  Left = 334
  Top = 320
  Width = 270
  Height = 163
  Caption = 'Change Values'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Page: TPageControl
    Left = 0
    Top = 25
    Width = 262
    Height = 107
    ActivePage = TabSheet9
    Align = alClient
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'XString'
      TabVisible = False
      object ValXString: TComboBox
        Left = 16
        Top = 32
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'XFloat'
      ImageIndex = 1
      TabVisible = False
      object ValXFloat: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 8
        LabelPosition = lpAbove
        LabelText = ' '
        LabelSpacing = 5
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'XVector'
      ImageIndex = 2
      TabVisible = False
      object ValXVectorX: TEditX
        Left = 24
        Top = 8
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'X'
        LabelSpacing = 5
        TabOrder = 0
      end
      object ValXVectorY: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'Y'
        LabelSpacing = 5
        TabOrder = 1
      end
      object ValXVectorZ: TEditX
        Left = 24
        Top = 56
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'Z'
        LabelSpacing = 5
        TabOrder = 2
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'XInt'
      ImageIndex = 3
      TabVisible = False
      object ValXInt: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpAbove
        LabelText = ' '
        LabelSpacing = 5
        MaxValue = 2147483647
        MinValue = -1
        TabOrder = 0
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'XBool'
      ImageIndex = 4
      TabVisible = False
      object ValXBool: TComboBox
        Left = 16
        Top = 32
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'True'
        Items.Strings = (
          'False'
          'True')
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'XUint'
      ImageIndex = 5
      TabVisible = False
      object ValXUint: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpAbove
        LabelText = ' '
        LabelSpacing = 5
        MaxValue = 32767
        MinValue = -32768
        TabOrder = 0
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'XColor'
      ImageIndex = 6
      TabVisible = False
      object ValXColorR: TEditX
        Left = 24
        Top = 0
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpLeft
        LabelText = 'R'
        LabelSpacing = 5
        MaxValue = 255
        TabOrder = 0
      end
      object ValXColorG: TEditX
        Left = 24
        Top = 24
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpLeft
        LabelText = 'G'
        LabelSpacing = 5
        MaxValue = 255
        TabOrder = 1
      end
      object ValXColorB: TEditX
        Left = 24
        Top = 48
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpLeft
        LabelText = 'B'
        LabelSpacing = 5
        MaxValue = 255
        TabOrder = 2
      end
      object ValXColorA: TEditX
        Left = 24
        Top = 72
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpLeft
        LabelText = 'A'
        LabelSpacing = 5
        MaxValue = 255
        TabOrder = 3
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'XByte'
      ImageIndex = 7
      TabVisible = False
      object ValXByte: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 1
        Precision = 0
        LabelPosition = lpAbove
        LabelText = ' '
        LabelSpacing = 5
        MaxValue = 255
        TabOrder = 0
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'XPoint'
      ImageIndex = 8
      TabVisible = False
      object ValXPointX: TEditX
        Left = 24
        Top = 24
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'X'
        LabelSpacing = 5
        TabOrder = 0
      end
      object ValXPointY: TEditX
        Left = 24
        Top = 48
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'Y'
        LabelSpacing = 5
        TabOrder = 1
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'XFColor'
      ImageIndex = 9
      TabVisible = False
      object ValXFColorB: TEditX
        Left = 24
        Top = 56
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'B'
        LabelSpacing = 5
        MaxValue = 3
        MinValue = -3
        TabOrder = 0
      end
      object ValXFColorG: TEditX
        Left = 24
        Top = 32
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'G'
        LabelSpacing = 5
        MaxValue = 3
        MinValue = -3
        TabOrder = 1
      end
      object ValXFColorR: TEditX
        Left = 24
        Top = 8
        Width = 121
        Height = 19
        FloatDiv = 0.01
        Precision = 6
        LabelPosition = lpLeft
        LabelText = 'R'
        LabelSpacing = 5
        MaxValue = 3
        MinValue = -3
        TabOrder = 2
      end
    end
  end
  object Apply: TButton
    Left = 178
    Top = 61
    Width = 71
    Height = 25
    Caption = 'Apply'
    ModalResult = 1
    TabOrder = 1
    OnClick = ApplyClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 262
    Height = 25
    Align = alTop
    BiDiMode = bdLeftToRight
    Caption = 'Value'
    ParentBiDiMode = False
    TabOrder = 2
  end
end
