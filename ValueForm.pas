unit ValueForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, XomLib, StdCtrls, ExtCtrls, PanelX;

type
  TChgValForm = class(TForm)
    Page: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    ValXFloat: TEditX;
    ValXVectorX: TEditX;
    ValXVectorY: TEditX;
    ValXVectorZ: TEditX;
    Apply: TButton;
    ValXString: TComboBox;
    ValXInt: TEditX;
    ValXBool: TComboBox;
    ValXUint: TEditX;
    ValXColorR: TEditX;
    ValXColorG: TEditX;
    ValXColorB: TEditX;
    ValXPointX: TEditX;
    ValXPointY: TEditX;
    Panel1: TPanel;
    ValXByte: TEditX;
    TabSheet10: TTabSheet;
    ValXFColorB: TEditX;
    ValXFColorG: TEditX;
    ValXFColorR: TEditX;
    ValXColorA: TEditX;
    procedure FormShow(Sender: TObject);
    procedure ApplyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChgValForm: TChgValForm;
  CntrVal: TCntrVal;

implementation

{$R *.dfm}



function GetValues(const Strings:TStrings):TStrings;
var i,ValPos:integer;
Res:String;
begin
Result:=TStringList.Create;
for  i:=0 to Strings.Count-1 do begin
      Res := Strings.Strings[i];
      ValPos := Pos('=', Res);
      if ValPos > 0 then
        Delete(Res, 1, ValPos);
      Result.Add(Res);
end;
end;

procedure TChgValForm.FormShow(Sender: TObject);
var
Pnt:Pointer;
vc:Cardinal;
begin
 Panel1.Caption:=CntrVal.IdName;
 Pnt:=CntrVal.Point;
 case CntrVal.XType of
      XBool:    begin
                Page.ActivePageIndex:=4;
                ValXBool.Text:=BoolToStr(Boolean(byte(Pnt^)), true);
                end;
      XByte:    begin
                Page.ActivePageIndex:=7;
                ValXByte.FloatVal:=byte(Pnt^);
                end;
      XString:  begin
                Page.ActivePageIndex:=0;
                ValXString.Items:=GetValues(But.GlValueList.Strings);
                ValXString.Text:=Xom.GetString(TestByte128(Pnt));
                end;
      XFFloat,XFloat:   begin
                Page.ActivePageIndex:=1;
                ValXFloat.FloatVal:=single(Pnt^);
                end;
      XInt:     begin
                Page.ActivePageIndex:=3;
                ValXInt.FloatVal:=integer(Pnt^);
                end;
      XCode:    begin

                end;
      XVector:  Begin
                Page.ActivePageIndex:=2;
                vc:=Longword(Pnt);
                ValXVectorX.FloatVal:=Single(Pointer(vc)^);
                ValXVectorY.FloatVal:=Single(Pointer(vc + 4)^);
                ValXVectorZ.FloatVal:=Single(Pointer(vc + 8)^);
                end;
      XFColor:  Begin
                Page.ActivePageIndex:=6;
                vc:=Longword(Pnt);
                ValXFColorR.FloatVal:=Single(Pointer(vc)^);
                ValXFColorG.FloatVal:=Single(Pointer(vc + 4)^);
                ValXFColorB.FloatVal:=Single(Pointer(vc + 8)^);
                end;
      XColor:  Begin
                Page.ActivePageIndex:=6;
                vc:=Longword(Pnt);
                ValXColorR.FloatVal:=byte(Pointer(vc)^);
                ValXColorG.FloatVal:=byte(Pointer(vc + 1)^);
                ValXColorB.FloatVal:=byte(Pointer(vc + 2)^);
                ValXColorA.FloatVal:=byte(Pointer(vc + 3)^);
                end;
      XUInt:    begin
                Page.ActivePageIndex:=5;
                ValXUInt.FloatVal:=SmallInt(Pnt^);
                end;
      XPoint:   begin
                Page.ActivePageIndex:=8;
                vc:=Longword(Pnt);
                ValXPointX.FloatVal:=Single(Pointer(vc)^);
                ValXPointY.FloatVal:=Single(Pointer(vc + 4)^);
                end;
      end;

end;


procedure TChgValForm.ApplyClick(Sender: TObject);
var
OldPnt,Pnt,NewPnt:Pointer;
VirtualBufer: TMemoryStream;
Size:Integer;
vc:Cardinal;
val:Cardinal;
valf:Single;
begin
// ������� �����
OldPnt := StrArray[CntrVal.Cntr].point;
 VirtualBufer := TMemoryStream.Create;
 Pnt:=CntrVal.Point;   // ����� �� ��������
 Size:= StrArray[CntrVal.Cntr].size;
 VirtualBufer.Write(OldPnt^,Cardinal(Pnt)-Cardinal(OldPnt));
 case CntrVal.XType of
      XBool:    begin
                val:=ValXBool.ItemIndex;
                VirtualBufer.Write(val,1);
                Inc(Longword(Pnt),1);
                end;
      XByte:    begin
                val:=Round(ValXByte.GetFloatVal);
                VirtualBufer.Write(val,1);
                Inc(Longword(Pnt),1);
                end;
      XString:  begin
                Xom.WriteXName(VirtualBufer,ValXString.Text);
                TestByte128(Pnt);
                end;
      XFFloat,XFloat:   begin
                valf:=ValXFloat.GetFloatVal;
                VirtualBufer.Write(valf,4);
                Inc(Longword(Pnt),4);
                end;
      XInt:     begin
                val:=Round(ValXInt.GetFloatVal);
                VirtualBufer.Write(val,4);
                Inc(Longword(Pnt),4);
                end;
      XCode:    begin

                end;
      XVector:  Begin
                valf:=ValXVectorX.GetFloatVal;
                VirtualBufer.Write(valf,4);
                valf:=ValXVectorY.GetFloatVal;
                VirtualBufer.Write(valf,4);
                valf:=ValXVectorZ.GetFloatVal;
                VirtualBufer.Write(valf,4);
                Inc(Longword(Pnt),4*3);
                end;
      XFColor:  Begin
                valf:=ValXFColorR.GetFloatVal;
                VirtualBufer.Write(valf,4);
                valf:=ValXFColorG.GetFloatVal;
                VirtualBufer.Write(valf,4);
                valf:=ValXFColorB.GetFloatVal;
                VirtualBufer.Write(valf,4);
                Inc(Longword(Pnt),4*3);
                end;
      XColor:  Begin
                val:=Round(ValXColorR.GetFloatVal);
                VirtualBufer.Write(val,1);
                val:=Round(ValXColorG.GetFloatVal);
                VirtualBufer.Write(val,1);
                val:=Round(ValXColorB.GetFloatVal);
                VirtualBufer.Write(val,1);
                val:=Round(ValXColorA.GetFloatVal);
                VirtualBufer.Write(val,1);
                Inc(Longword(Pnt),1*4);
                end;
      XUInt:    begin
                val:=Round(ValXUInt.GetFloatVal);
                VirtualBufer.Write(val,2);
                Inc(Longword(Pnt),2);
                end;
      XPoint:   begin
                valf:=ValXPointX.GetFloatVal;
                VirtualBufer.Write(valf,4);
                valf:=ValXPointY.GetFloatVal;
                VirtualBufer.Write(valf,4);
                Inc(Longword(Pnt),4*2);
                end;
 end;
 VirtualBufer.Write(Pnt^,Size-(Cardinal(Pnt)-Cardinal(OldPnt)));
 Size := VirtualBufer.Position;
 NewPnt := AllocMem(Size);
 Move(VirtualBufer.Memory^, NewPnt^, Size);
 VirtualBufer.Free;

 if StrArray[CntrVal.Cntr].Update then
 FreeMem(StrArray[CntrVal.Cntr].point);   // ��������
  StrArray[CntrVal.Cntr].Update := true;
  StrArray[CntrVal.Cntr].point := NewPnt;
  StrArray[CntrVal.Cntr].size := Size;
end;

end.
