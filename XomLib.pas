unit XomLib;

interface

uses OpenGLx, IdGlobal, SysUtils, Classes, Dialogs, Windows,
  ComCtrls, StdCtrls, ExtCtrls, Graphics, Buttons, Valedit, XTrackPanel, OpenGLLib,
  Math,Menus;
{$DEFINE NOMULTI}
type
  Tver    = array[0..2] of GLfloat;
  AVer    = array of TVer;
  Aval    = array of GLfloat;
  AAval   = array of Aval;
  TFace   = array[0..3] of Word;
  AFace   = array of TFace;
  TFace3  = array[0..2] of Word;
  AFace3  = array of TFace3;
  Tpnt    = array[0..1] of GLfloat;
  ATCoord = array of Tpnt;
  TUColor = array[0..3] of Byte;
  AUColor = array of TUColor;
  Tvector = record
    X, Y, Z: GLfloat;
  end;

  TBox = record
    Xmin, Ymin, Zmin: GLfloat;
    Xmax, Ymax, Zmax: GLfloat;
  end;

  Color4d = array[0..3] of GLfloat;
  TMatrix = array[1..4, 1..4] of GLfloat;

  TDrawType = (DrMesh, DrBlend, DrBone, DrGenAnim,DrSelect,DrBoxes);

  TMaterial = record
    Name: string;
    use: Boolean;
    Abi, Dif, Spe, Emi: Color4d;
    Shi: Single;
  end;

  TAttribute = record
    Material: TMaterial;
    Texture: Integer;
    TextureId: Integer;
    TextureClamp:Boolean;
    Texture2D: Boolean;
    T2DMatrix:TMatrix;
    ZBufferFuncVal: Integer;
    CullFace: Boolean;
    ZBuffer: Boolean;
    Multi: Boolean;
    AlphaTest: Boolean;
    AlphaTestVal: Single;
    Blend: Boolean;
    MultiT1: Integer;
    MultiT2: Integer;
    BlendVal1: Integer;
    BlendVal2: Integer;
  end;

  TTransTypes = (TTNone, TTMatrix, TTVector, TTJoint);

  TTrans = record
    TransType: TTransTypes;
    Pos: Tver;
    Rot: Tver;
    RotOrient: Tver;
    JointOrient: Tver;
    Size: Tver;
    TrMatrix: TMatrix;
  end;

  TAnimInfo = record
    True:Boolean;
    Tm:TMatrix;
    TCoord: Tver;
    Child:Integer;
  end;

  TXCtrls = record
    DrawBoxes,
    AnimButton,
    EditAnimBut,
    RotateButton,
    Dummy,Move,Rotate,Scale
    : PBoolean;
 //   JointButton: TSpeedButton;
    GlValueList: TValueListEditor;
    AnimBox: TComboBox;
    TrackPanel: TTrackPanel;
 //   ValueList: TValueListEditor;
    Status, StatusM: TStatusPanel;
    TreeViewM, ClassTree: TTreeView;
    XLabel, XImageLab, TreeLabel: TLabel;
    XImage: TImage;
    Handle: THandle;
    Canvas: TCanvas;
  end;
  TKeyFrame = array [0..5] of GLfloat;
  PKeyFrame = ^TKeyFrame;

  TGrafKey = record
        Frame:PKeyFrame;
        Rect:TRect;
        end;

  TTypeKey = record
    objname: string;
//    fullname: string;
    NoAddOp: Boolean;
    ktype: Integer;
  end;

  TKeyData = record
    keytype: TTypeKey;
    Data: array of TKeyFrame;
  end;

  PKeyData = ^TKeyData;

  TMenuItem = class (Menus.TMenuItem)
    public
    KeyType: TTypeKey;
    end;

  AAnimInfo = array of TAnimInfo;

  TAnimClip = class
{  private
    FAnimInfo:AAnimInfo;
    FNamesObj:TStringList;  }
  public
    Name: string;
    Time: Single;
    Keys: array of TKeyData;
    constructor Create;
    function GenAnimFrame(Name: string; T: TTrans; Bone: Boolean; ActiveMove:Boolean):TAnimInfo;
 //   function  GetAnimFrame(Name: string):TAnimInfo;
    procedure LoadXAnimClip(FileName: string);
    procedure SaveXAnimClip(FileName: string);
    procedure AddKey(KData:PKeyData;CurTime:Single;FirstValue:Single;NewValue:Single;AType:Integer);
    function  FindKey(KData:PKeyData;SelKey:PKeyFrame):integer;
    function  FindTimeKey(KData:PKeyData;FTime:Single):integer;
    function  FindKeyNameType(FName:String;Atype:integer):PKeyData;
    procedure Copy(Source:TAnimClip);
    procedure SortKeys;
    procedure SortKeyData(KData:PKeyData);
    procedure AddKData(KType:TTypeKey);
    procedure DeleteKData(KType:TTypeKey);
    procedure DeleteKey(Key:PKeyFrame);
    procedure UpdateAnimClip;
    procedure Clear;
    destructor Destroy;override;
  end;

  AAnimClip = array of TAnimClip;



  TMesh = class
  //  AnimClips: AAnimClip;
    Indx: Integer;
    Name: string;
    XType: string;
    Vert: AVer;
    RVert: AVer;
    Weight: AAVal;
    Normal: AVer;
    Face: AFace;
    Color: AUColor;
    TextCoord: ATCoord;
    TextCoord2: ATCoord;
    Transform: TTrans;
    Attribute: TAttribute;
    BoneName: string;
 //   BoneNoInv: Boolean;
    BoneMatrix: TMatrix;
    InvBoneMatrix: TMatrix;
    SizeBox: TBox;
    IsLink:Boolean;
    Bones: array of TMesh;
    Childs: array of TMesh;
    constructor Create;
    procedure Draw(DrawType: TDrawType);
    procedure GetBox(var Box: TBox);

    function GetBone(CntrIndx: Integer; Mesh: TMesh): TMesh;

    procedure glBuildTriangleGeo(CntrIndx: Integer);
    procedure glBuildCollGeo(CntrIndx: Integer);
    procedure glLoad2DMipmaps(target: GLenum; Components, Width, Height: GLint;
      Format, atype: GLenum; Data: Pointer);
    procedure glClearCollGeo;
    procedure DrawSelectBox(SelectBox: TBox);
    procedure SaveAsXom3D(FileName: string);
    function GetTextureGL(Point: Pointer): Boolean;
    procedure ReadXNode(VirtualBufer: TMemoryStream;
      XTextureStr: TStringList; Tree: TTreeView; Node: TTreeNode);
    function GetMeshOfName(ObjName:String):TMesh;
    function GetMeshOfID(Id:Integer):TMesh;
    destructor Destroy;override;
  end;

  THRTimer = class(TObject)
    constructor Create;
    function StartTimer: Boolean;
    procedure ReadTimer;
  private
    StartTime: Single;
    ClockRate: Single;
  public
    Value: Single;
    MaxTime: Single;
    Exists: Boolean;
  end;

  TTransView = record
    xpos, ypos, zpos: GLfloat;
    xrot, yrot, zrot: GLfloat;
    zoom, Per: GLfloat;
  end;
  
  T3DModel = record
    glList: Integer;
    SizeBox: TBox;
    Texture: Integer;
    Matrix: TMatrix;
    Mult: Boolean;
  end;
  A3DModels = array of T3DModel;
  XValTypes = (XString,XFloat,XFFloat,XVector,XInt,XBool,XUint,XFColor,XColor,XCode,XPoint,XByte);
  XTypes = (XNone,
    DetailEntityStore,
    LandFrameStore,
    GSProfile,
    GSProfileList,
    WeaponSettingsData,
    SchemeData,
    SchemeColective,
    TeamDataColective,
    HighScoreData,
    LevelDetails,
    LockedContainer,
    ParticleEmitterContainer,
    BaseWeaponContainer,
    MeleeWeaponPropertiesContainer,
    GunWeaponPropertiesContainer,
    SentryGunWeaponPropertiesContai,
    FlyingPayloadWeaponPropertiesCo,
    JumpingPayloadWeaponPropertiesC,
    HomingPayloadWeaponPropertiesCo,
    PayloadWeaponPropertiesContaine,
    MineFactoryContainer,
    EffectDetailsContainer,
    StoredTeamData,
    CampaignData,
    PC_LandChunk,
    PC_LandFrame,
    XAlphaTest,
    XAnimClipLibrary,
    XAnimInfo,
    XBinModifier,
    XBitmapDescriptor,
    XBlendModeGL,
    XBone,
    XChildSelector,
    XCollisionGeometry,
    XCollisionData,
    XColor4ubSet,
    XColorResourceDetails,
    XConstColorSet,
    XContainerResourceDetails,
    XCoord3fSet,
    XCullFace,
    XCustomDescriptor,
    XDataBank,
    XDepthTest,
    XDetailObjectsData,
    XEnvironmentMapShader,
    XExpandedAnimInfo,
    XJointTransform,
    XFloatResourceDetails,
    XFortsExportedData,
    XGraphSet,
    XGroup,
    XImage,
    XIndexedTriangleSet,
    XIndexedTriangleStripSet,
    XIndexSet,
    XInteriorNode,
    XIntResourceDetails,
    XLightingEnable,
    XMaterial,
    XMatrix,
    XMeshDescriptor,
    XMultiTexCoordSet,
    XMultiTexShader,
    XNormal3fSet,
    XNullDescriptor,
    XOglTextureMap,
    XPalette,
    XPaletteWeightSet,
    XPointLight,
    XPositionData,
    XShape,
    XSimpleShader,
    XSkin,
    XSkinShape,
    XSpriteSetDescriptor,
    XStringResourceDetails,
    XTexCoord2fSet,
    XTextDescriptor,
    XTexturePlacement2D,
    XTexFont,
    XTransform,
    XUintResourceDetails,
    XVectorResourceDetails,
    XWeightSet,
    XZBufferWriteEnable,
    W3DTemplateSet,
    XXomInfoNode,
    XExportAttributeString,
    XMax);

  TCntrVal = class
    constructor Create;
  public
    IdName:String;
    Point: Pointer;
    XType: XValTypes;
    Cntr: Integer;
  end;

  TIndex = record
    ID: Integer;
    Values: array [0..12] of Integer;
  end;

  TStEdByte = record
    //sbyte,ebyte:smallint;
    id: Integer;
  end;

  TConteiner = record
    point: Pointer; // ������ �� ����� � ������
    size: Integer; // ������ ����������
    Update: Boolean; // ��������
    CTNR: Boolean; // ����� ���������
    Xtype: XTypes; // ��� ����������
  end;

  XConteiners = array of TConteiner;

  TgaHead = record
    zero: Word;
    typeTGA: Word;
    zero2: Longword;
    zero3: Longword;
    Width: Word;
    Height: Word;
    ColorBit: Word;
  end;

  TRGBA = record
    b, g, r, a: Byte;
  end;
  ARGBA  = array [0..1] of TRGBA;
  PARGBA = ^ARGBA;

  TRGB = record
    b, g, r: Byte;
  end;
  ARGB  = array [0..1] of TRGB;
  PARGB = ^ARGB;

  AB  = array [0..1] of Byte;
  PAB = ^AB;

type
  TXom = class
  public
    Mesh3D: TMesh;
//    AnimClips: TAnimClips;
    LastXImageIndex: Integer;
    Conteiners, BaseConteiner,
    LastCounter: Integer;
    TreeArray: array of Boolean;
    TreeCount: Integer;
    saidx: Integer;
    BaseNode: TTreeNode;
    Buf: Pointer;
    STOffset: Pointer;
    constructor Create;
    procedure LoadXomFileName(FileName: string; var OutCaption: string);
    procedure SaveXom(SaveDialog: TSaveDialog; Conteiners: XConteiners);
    procedure XomImportObj(InNode, OutNode: TTreeNode);
    function AddClassTree(CntrIndx: Integer; TreeView: TTreeView;
      TreeNode: TTreeNode; HideChild: Boolean = false): TTreeNode;
    procedure SelectClassTree(CntrIndx: Integer; var Mesh: TMesh);

    procedure WriteXName(var Memory:TMemoryStream; Name:String);
    function GetString(indx: Integer): string;
  end;

    TAnimClips = class
  private
    FAnimClips:AAnimClip;
    FStrings:TStringList;
  //  FKeyList:TStringList;
  protected
    function GetItem(Index: String): TAnimClip;
    procedure SetItem(Index: String; Value: TAnimClip);
  public
    cIndx:integer;
    Name:String;
    constructor Create;
    destructor Destroy; override;
    procedure ClearClips;
    function AddClip(Clip:TAnimClip):Integer;
    function GetNameAnimType(AType: Cardinal): string;
    function BuildAnim(CntrIndx: Integer):Integer;
    procedure SaveAnimToXom(Xom:TXom);
    function GetItemID(Index: Integer): TAnimClip;
    property Items[Index:String]:TAnimClip read GetItem write SetItem;
//    property KeyList:TStringList read FKeyList write FKeyList;
  end;



//function GetFullName(Name:String):String;
function BuildTree(XomTree, Tree: TTreeView; InNode, Node: TTreeNode): XTypes;
function ReadName(var Stream: TMemoryStream): string;

function Byte128(_byte: Integer): Integer;
function TestByte128(var _p: Pointer): Integer;
procedure WriteXByte(var Memory: TMemoryStream; Val: Integer);
procedure WriteXByteP(var p: Pointer; Val: Integer);


function ToTime(milisec: Longword): Single;

procedure oglAxes;
procedure oglGrid(GridMax: Integer);
procedure Light(var Color: Color4D);

function GetMatrix2(Pos, Rot1, Rot2, Rot3, Size: Tver): TMatrix;
function GetMatrix(Pos, Rot, Size: Tver): TMatrix;
procedure MatrixDecompose(XMatrix: TMatrix; var XPos: Tver;
  var XRot: Tver; var XSize: Tver);
function MatrixInvert(const m: TMatrix): TMatrix;
function MatrXMatr(M1, M2: TMatrix): TMatrix;
function MatrXVert(Matrix: TMatrix; V0, V1, V2: Single): Tver;
function ValXVert(Val: Single; vect: TVer): TVer;
function VertAddVert(v1, v2: TVer): TVer;

procedure ViewTga(XIndex: Integer; ImageT32: TImage; XGrid: Boolean;
  XLabel: TLabel);
procedure SaveTGA(Name: string; Image32: TImage; Index: Integer;
  XLabel: TLabel);
function ImportXImage(XIndex: Integer; Name: string): Boolean;
Procedure UpdateAnimTree(TV:TTreeView);
procedure UpdateAnimTreeMenu(AddItem,DeleteItem:TMenuItem);

const
 APPVER = 'XomView v.2.7 Beta by AlexBond';
 PCharXTypes:array[XTypes] of PChar = ('XNone',
    'DetailEntityStore',
    'LandFrameStore',
    'GSProfile',
    'GSProfileList',
    'WeaponSettingsData',
    'SchemeData',
    'SchemeColective',
    'TeamDataColective',
    'HighScoreData',
    'LevelDetails',
    'LockedContainer',
    'ParticleEmitterContainer',
    'BaseWeaponContainer',
    'MeleeWeaponPropertiesContainer',
    'GunWeaponPropertiesContainer',
    'SentryGunWeaponPropertiesContai',
    'FlyingPayloadWeaponPropertiesCo',
    'JumpingPayloadWeaponPropertiesC',
    'HomingPayloadWeaponPropertiesCo',
    'PayloadWeaponPropertiesContaine',
    'MineFactoryContainer',
    'EffectDetailsContainer',
    'StoredTeamData',
    'CampaignData',
    'PC_LandChunk',
    'PC_LandFrame',
    'XAlphaTest',
    'XAnimClipLibrary',
    'XAnimInfo',
    'XBinModifier',
    'XBitmapDescriptor',
    'XBlendModeGL',
    'XBone',
    'XChildSelector',
    'XCollisionGeometry',
    'XCollisionData',
    'XColor4ubSet',
    'XColorResourceDetails',
    'XConstColorSet',
    'XContainerResourceDetails',
    'XCoord3fSet',
    'XCullFace',
    'XCustomDescriptor',
    'XDataBank',
    'XDepthTest',
    'XDetailObjectsData',
    'XEnvironmentMapShader',
    'XExpandedAnimInfo',
    'XJointTransform',
    'XFloatResourceDetails',
    'XFortsExportedData',
    'XGraphSet',
    'XGroup',
    'XImage',
    'XIndexedTriangleSet',
    'XIndexedTriangleStripSet',
    'XIndexSet',
    'XInteriorNode',
    'XIntResourceDetails',
    'XLightingEnable',
    'XMaterial',
    'XMatrix',
    'XMeshDescriptor',
    'XMultiTexCoordSet',
    'XMultiTexShader',
    'XNormal3fSet',
    'XNullDescriptor',
    'XOglTextureMap',
    'XPalette',
    'XPaletteWeightSet',
    'XPointLight',
    'XPositionData',
    'XShape',
    'XSimpleShader',
    'XSkin',
    'XSkinShape',
    'XSpriteSetDescriptor',
    'XStringResourceDetails',
    'XTexCoord2fSet',
    'XTextDescriptor',
    'XTexturePlacement2D',
    'XTexFont',
    'XTransform',
    'XUintResourceDetails',
    'XVectorResourceDetails',
    'XWeightSet',
    'XZBufferWriteEnable',
    'W3DTemplateSet',
    'XXomInfoNode',
    'XExportAttributeString',
    'XMax');
  MaxMaps    = 10000;
  MaxType    = 1000;
  MODEL3DBUF = 10000;

  FontGL  = 2000;
  MaxDeph = 100000.0;  // ������������ �������

  Blendval: array[0..7] of Integer =
    (GL_ZERO, GL_ONE,
    GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR,
    GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR,
    GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  objAxes          = 1;
  objTarget        = 2;
  objGrid          = 3;
  objBox           = 4;
  objOrbitX        = 5;
  objOrbitY        = 6;
  objOrbitZ        = 7;
  objCollGeo       = 10;
  objTriangleGeo   = 11;

  ambient: color4d = (0.1, 0.1, 0.1, 1.0);
  //  ambient2: color4d = ( 0.1, 0.1, 0.1, 1.0 );
  l_position: color4d    = (0.0, 32.0, 0.0, 1.0);
  mat_diffuse: color4d   = (0.6, 0.6, 0.6, 1.0);
  mat_specular: color4d  = (1.0, 1.0, 1.0, 1.0);
  tmp_fog_color: color4d = (0.0, 0.2, 0.5, 1.0);
  mat_shininess: GLfloat = 50.0;

  ATypePosition = $0102;
  ATypeRotation = $0103;
  ATypeScale    = $0104;
  AType2DTex    = $0401;
  ATypeReScale  = $0904;
  ATypeTexture  = $1100;
  ATypeChilds   = $0100;
  ATypeX        = 0;
  ATypeY        = 1;
  ATypeZ        = 2;

  TORAD   = Pi / 180;
  RADIANS = 180 / Pi;

  ZereVector: Tver = (0, 0, 0);
  TestVert: Tver   = (0.0, 0, 0);

  Ctnr  = $524e5443;//'CTNR';
  Ctnr2 = $53525453; //'STRS'

  TypeStr: PChar       = 'TYPE';
  FileHeadType: PChar  = 'MOIK';
  Schm: PChar          = 'SCHM';
  StrS: PChar          = 'STRS';
  CtnrS: PChar         = 'CTNR';
  Space: PChar         = '   ';
  
  nVn  = #13#10;

var
  Xom: TXom;
  GrafKeys:array of TGrafKey;
  EdMode,AnimEd:Boolean;
  SelectKey,SKey: PKeyFrame;
  SelectKdata:PKeyData;
  SelectType:Integer;
  SelectObjName:String;
  ShowGraph:Boolean;
//  AnimClip: TAnimClip;
  MAxis,AAxis:TAxis;
    PSelect, PTarget: TPoint;
    wd,wdup:TXYZ;
  SelectObj, SelectObjOn: Integer;
  Bbitpoint, bBitPoint2: Pointer;

  But:TXCtrls;

  AnimTimer: THRTimer;
  AnimClips:TAnimClips;
  CurAnimClip:TAnimClip;
  BaseClip:TAnimClip;

  MaxAnimTime: Single;
  Xbit: Integer;
  Active3DModel: Integer = MODEL3DBUF;
  TransView,TVModel: TTransView;
  GLwidth, GLheight: Integer;
  position2: color4d = (0.0, 0.0, 0.0, 1.0);
  MainBox: TBox;
  NTexture: Integer = 0;
  LastTexture: Integer = 0;
  XomImport, ImageReady, Xom3DReady, AnimReady: Boolean;
  SelectMode,
  MoveMode, MoveReady,
  RotateMode, RotateReady,
  ScaleMode, ScaleReady,
  Particle_mode,
  ZoomActivePox, TextureB, FillMode, MoveFirts,
  ScaleFirts,RotateFirts,
  CtrlMove, Ctrl,ShiftOn, FullSize, ChillMode: Boolean;
  ChillMax:Integer;
  AddClick,DeleteClick:TNotifyEvent;
  StrArray: XConteiners;
  ActiveMesh:TMesh;
  ObjMatrix:TMatrix;
  WUM:boolean = false;

implementation

procedure glColorPointer(size: TGLint; _type: TGLenum;
  stride: TGLsizei; const _pointer: PGLvoid); stdcall; external opengl32;

procedure glTexCoordPointer(size: TGLint; _type: TGLenum;
  stride: TGLsizei; const _pointer: PGLvoid); stdcall; external opengl32;
//procedure glColorTable(target: TGLint; internalformat: TGLenum; width: TGLsizei;format:TGLenum;_type: TGLenum; const _pointer: PGLvoid);stdcall;external opengl32;

procedure glDisableClientState(_array: TGLenum); stdcall; external opengl32;

// ������������ ���� �� RGBA � ������� ���� ��� GL

{function GetFullName(Name:String):String;
begin
Result:=Name;
end; }

procedure UpdateAnimTreeMenu(AddItem,DeleteItem:TMenuItem);
var
Item1,Item2,Item3:TMenuItem;
Item1d,Item2d:TMenuItem;
ObjMesh:TMesh;
SL:TStringList;

        function MakeItem(Item:TMenuItem;Name:String):TMenuItem;
        begin
        Result := TMenuItem.Create(AddItem);
        Result.Caption := Name;
        Item.Add(Result);
        end;



        procedure AddObjMenu(Mesh:TMesh;Skip:Boolean=False);
        var
        kData:PkeyData;
        i:integer;
        KeyType:TTypeKey;
                    function TestXYZItems(Item:TMenuItem;ktype:integer):TMenuItem;
                    begin
                    Result:=nil;
                    case Byte(ktype shr 24) of
                        ATypeX: Result:=MakeItem(Item,'X');
                        ATypeY: Result:=MakeItem(Item,'Y');
                        ATypeZ: Result:=MakeItem(Item,'Z');
                    end;
                    end;

                function TestTypeItem(kType:Integer):TMenuItem;
                
                begin
                kData:=CurAnimClip.FindKeyNameType(Mesh.Name,kType);
                if kData=nil then begin
                        if kType=ATypeTexture then
                                Result := MakeItem(Item1,'Childs')
                        else
                        Result := TestXYZItems(Item2,kType);
                        KeyType.objname:=Mesh.Name;
                        KeyType.ktype:=kType;
                        Result.KeyType:=KeyType;
                        Result.OnClick:=AddClick;
                        end else begin
                        if kType=ATypeTexture then
                                Result := MakeItem(Item1d,'Childs')
                        else
                        Result := TestXYZItems(Item2d,kType);
                        Result.KeyType:=kData.KeyType;
                        Result.OnClick:=DeleteClick;
                        end;
                end;
        begin
        if (not Skip)or( (Mesh.XType='SS')and Mesh.Attribute.Texture2D) then begin
        Item1 := MakeItem(AddItem,Mesh.Name);
        Item1d := MakeItem(DeleteItem,Mesh.Name);
        SL.Add(Mesh.Name);
        if Mesh.Transform.TransType<>TTNone then begin
                Item2 := MakeItem(Item1,'Position');
                Item2d := MakeItem(Item1d,'Position');
                        Item3:=TestTypeItem(ATypePosition+(ATypeX shl 24));
                        Item3:=TestTypeItem(ATypePosition+(ATypeY shl 24));
                        Item3:=TestTypeItem(ATypePosition+(ATypeZ shl 24));
                        if Item2.Count=0 then Item2.Destroy;
                        if Item2d.Count=0 then Item2d.Destroy;
                Item2 := MakeItem(Item1,'Rotate');
                Item2d := MakeItem(Item1d,'Rotate');
                        Item3:=TestTypeItem(ATypeRotation+(ATypeX shl 24));
                        Item3:=TestTypeItem(ATypeRotation+(ATypeY shl 24));
                        Item3:=TestTypeItem(ATypeRotation+(ATypeZ shl 24));
                         if Item2.Count=0 then Item2.Destroy;
                         if Item2d.Count=0 then Item2d.Destroy;
                Item2 := MakeItem(Item1,'Size');
                Item2d := MakeItem(Item1d,'Size');
                        Item3:=TestTypeItem(ATypeReScale+(ATypeX shl 24));
                        Item3:=TestTypeItem(ATypeReScale+(ATypeY shl 24));
                        Item3:=TestTypeItem(ATypeReScale+(ATypeZ shl 24));
                         if Item2.Count=0 then Item2.Destroy;
                         if Item2d.Count=0 then Item2d.Destroy;
                end;
        if Mesh.Attribute.Texture2D then  begin
                Item2 := MakeItem(Item1,'Texture');
                Item2d := MakeItem(Item1d,'Texture');
                        Item3:=TestTypeItem(AType2DTex+(ATypeX shl 24));
                        Item3:=TestTypeItem(AType2DTex+(ATypeY shl 24));
                         if Item2.Count=0 then Item2.Destroy;
                         if Item2d.Count=0 then Item2d.Destroy;
                        end;

        if Mesh.XType='CS' then begin
                Item2 := TestTypeItem(ATypeTexture);
                end;
        if Item1.Count=0 then Item1.Destroy;
        if Item1d.Count=0 then Item1d.Destroy;
                end;

        for i := 0 to Length(Mesh.Childs) - 1 do
                begin
                if (Mesh.Childs[i] <> nil)
                and (SL.IndexOf(Mesh.Childs[i].Name)=-1)
                and(Mesh.Childs[i].XType<>'BO')
                then
                        AddObjMenu(Mesh.Childs[i],
                        (Mesh.Childs[i].XType='BM')
                        or(Mesh.Childs[i].XType='SH')
                        or(Mesh.Childs[i].XType='SK')
                        or(Mesh.Childs[i].XType='SS')
                        );
                 end;
        end;


begin
ObjMesh:=Xom.Mesh3D;
SL:=TStringList.Create;
AddItem.Clear;
DeleteItem.Clear;
AddObjMenu(ObjMesh,True);
SL.Free;
end;

Procedure UpdateAnimTree(TV:TTreeView);
var
MainNode,Node,Node2,Node3,Node4:TTreeNode;
N,i,ii,nn,KType:integer;
Test:boolean;
s:string;
begin
if CurAnimClip<>nil then
with CurAnimClip do begin
TV.Items.Clear;
ChillMode:=False;
        MainNode:=TV.Items.Add(nil,Name);
        MainNode.ImageIndex:=9;
        MainNode.SelectedIndex:=9;
        N:=Length(Keys);
        for i:=0 to N-1 do begin
          Test:=false;
          If (ActiveMesh<>nil) and (ActiveMesh.Name<>Keys[i].keytype.objname) then
          continue;
          if (i>0) then Test:=(Keys[i].keytype.objname = Keys[i-1].keytype.objname);
          if  Test then else
          begin
          Node:=TV.Items.AddChild(MainNode,Keys[i].keytype.objname);
          Node.ImageIndex:=5;
          Node.SelectedIndex:=5;
         // if ActiveMesh=nil then
          Node.Data:=Xom.Mesh3D.GetMeshOfName(Keys[i].keytype.objname);

          end;
          if Test and (word(Keys[i].keytype.ktype)=word(Keys[i-1].keytype.ktype)) then
          else begin

          case Word(Keys[i].keytype.ktype) of
          ATypePosition: begin
                Node2:=TV.Items.AddChild(Node,'Position');
                end;
          ATypeRotation: begin
                Node2:=TV.Items.AddChild(Node,'Rotate');
                end;
          ATypeReScale,ATypeScale: begin
                Node2:=TV.Items.AddChild(Node,'Size');
                end;
          ATypeTexture,ATypeChilds: begin
                Node2:=TV.Items.AddChild(Node,'Childs');
                if ActiveMesh<>nil then begin
                                ChillMode:=true;
                                ChillMax:=Length(ActiveMesh.Childs);
                                end;
                end;
          AType2DTex: begin
                Node2:=TV.Items.AddChild(Node,'Texture');
                end;
          end;
          end;
          if ActiveMesh=nil then Continue;
          if (SelectType<>0) and  (SelectType<>Keys[i].keytype.ktype) then
           Continue;

          case Byte(Keys[i].keytype.ktype shr 24) of
          ATypeX: Node3:=TV.Items.AddChild(Node2,'X');

          ATypeY: Node3:=TV.Items.AddChild(Node2,'Y');

          ATypeZ: Node3:=TV.Items.AddChild(Node2,'Z');
          end;
          Node3.ImageIndex:=7;
          Node3.SelectedIndex:=7;
          if ActiveMesh<>nil then begin
                Node3.Data:=Pointer(Keys[i].keytype.ktype);
                kType:=Word(Keys[i].keytype.ktype);

                if (kType=AType2DTex) or
                (kType=ATypeTexture) or
                (kType=ATypeChilds) or
                (ActiveMesh.Transform.TransType=TTNone)
                then EdMode:=false else EdMode:=true;

                end;
          nn:=Length(Keys[i].Data);
          for ii:=0 to nn-1 do begin
                s:=format('%.2fs = %.2f',[
                Keys[i].Data[ii][4],Keys[i].Data[ii][5]]);
                Node4:=TV.Items.AddChild(Node3,s);
                Node4.ImageIndex:=8;
                Node4.SelectedIndex:=8;
               if ActiveMesh<>nil then Node4.Data:=@Keys[i].Data[ii];
             end;
        end;
MainNode.Expand(true);
end;
end;

procedure glColor4_256(RGBA: Cardinal);
var
  color4: color4d;
begin
  color4[0] := (RGBA and $ff) / 255.0;
  RGBA := RGBA shr 8;
  color4[1] := (RGBA and $ff) / 255.0;
  RGBA := RGBA shr 8;
  color4[2] := (RGBA and $ff) / 255.0;
  RGBA := RGBA shr 8;
  color4[3] := (RGBA and $ff) / 255.0;
  glColor4fv(@color4);
end;

procedure glxDrawBitmapText(Canvas:TCanvas; wdx, wdy, wdz: GLdouble; bitPoint2: Pointer;
  Name: string; select: Boolean);
var
  vp: TGLVectori4;
  PMatrix, MvMatrix: TGLMatrixd4;
  wtx, wty, wtz: GLdouble;
  Len: Integer;
begin
  // �������� ���������� ��������� ����������� ������
  glGetIntegerv(GL_VIEWPORT, @vp);
  glGetDoublev(GL_MODELVIEW_MATRIX, @MvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, @PMatrix);
  gluProject(wdx, wdy, wdz, MvMatrix, PMatrix, vp,
    @wdx, @wdy, @wdz);
  // ���� ����� ������� ����� ������ ������������� �� ����� ������ � ������
  if select then
  begin
    glBegin(GL_QUADS);
    // ��������� ������ ������ ���� �� ����
  {  if not MainForm.HideName1.Checked then
      Len := MainForm.Canvas.TextWidth(Name) div 2
    else
      Len := 0;   }
      Len := Canvas.TextWidth(Name) div 2;

    gluUnProject(wdx + 4 + Len, wdy + 4, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
    glVertex3f(wtx, wty, wtz);

    gluUnProject(wdx - 4, wdy + 4, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
    glVertex3f(wtx, wty, wtz);

    gluUnProject(wdx - 4, wdy - 4, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
    glVertex3f(wtx, wty, wtz);

    gluUnProject(wdx + 4 + Len, wdy - 4, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
    glVertex3f(wtx, wty, wtz);

    glEnd;
  end 
  else // ����� ������ ���������� ������
  begin
  // ���������� � ���� ��������� �����
  glPushAttrib(GL_COLOR_MATERIAL);
  glDisable(GL_LIGHTING);
  glEnable(GL_ALPHA_TEST);
  glColor3f(1, 1, 1);
    gluUnProject(wdx - 8, wdy - 8, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
    glRasterPos3f(wtx, wty, wtz);
    glDrawPixels(16, 16, $80E1, GL_UNSIGNED_BYTE, bitPoint2);
    // ���� ����� �������, ������� ��� �� �����
 //   if not MainForm.HideName1.Checked then
 //   begin
      gluUnProject(wdx + 10, wdy, wdz, MvMatrix, PMatrix, vp,
        @wtx, @wty, @wtz);
      glRasterPos3f(wtx, wty, wtz);
      glListBase(FontGL);
      glCallLists(Length(Name), GL_UNSIGNED_BYTE, Pointer(Name));
  //  end;
  glDisable(GL_ALPHA_TEST);
  glEnable(GL_LIGHTING);
  glPopAttrib;  // ���������� ��������� �� �����
  end;

end;

constructor TAnimClip.Create;
begin
end;

procedure TAnimClip.Copy(Source:TAnimClip);
var
 i,n,ii,nn:integer;
begin
 self.Name:=Source.Name;
 self.Time:=Source.Time;
 n:=Length(Source.Keys);
 SetLength(self.Keys,n);
 for i:=0 to n-1 do
        begin
        self.Keys[i].keytype:=Source.Keys[i].keytype;
        nn:=Length(Source.Keys[i].Data);
        SetLength(self.Keys[i].Data,nn);
        for ii:=0 to nn-1 do
                self.Keys[i].Data[ii]:=Source.Keys[i].Data[ii];
        end;
end;

procedure TAnimClip.LoadXAnimClip(FileName: string);
var
  VirtualBufer: TMemoryStream;
  inx2, Num, i, j: Integer;
begin
  VirtualBufer := TMemoryStream.Create;
  VirtualBufer.LoadFromFile(FileName);

//
  Name:=ReadName(VirtualBufer);
  VirtualBufer.Read(Time,4);
  Num:=0;
  VirtualBufer.Read(Num, 2);
  SetLength(Keys,Num);

  for i := 0 to num - 1 do
  begin
    Keys[i].keytype.objname:=ReadName(VirtualBufer);
//    Keys[i].keytype.fullname:=GetFullName(Keys[i].keytype.objname);
    VirtualBufer.Read(Keys[i].keytype.ktype, 4);
    inx2:=0;
    VirtualBufer.Read(inx2, 2);
    SetLength(Keys[i].Data,inx2);

    for j := 0 to inx2 - 1 do
    begin
      VirtualBufer.Read(Keys[i].Data[j][0], 6 * 4);
    end;
  end;
//  
  VirtualBufer.Free;
end;

procedure TAnimClip.SaveXAnimClip(FileName: string);
var
  VirtualBufer: TMemoryStream;
  inx2, Num, i, j: Integer;
  s: string;
begin
  VirtualBufer := TMemoryStream.Create;
  s := Name;
  VirtualBufer.Write(PChar(s)^, Length(s) + 1);
  VirtualBufer.Write(Time,4);
  Num := Length(Keys);
  VirtualBufer.Write(Num, 2);

  for i := 0 to num - 1 do
  begin
    s := Keys[i].keytype.objname;
    VirtualBufer.Write(PChar(s)^, Length(s) + 1);
    VirtualBufer.Write(Keys[i].keytype.ktype, 4);
    inx2 := Length(Keys[i].Data);
    VirtualBufer.Write(inx2, 2);
    for j := 0 to inx2 - 1 do
    begin
      VirtualBufer.Write(Keys[i].Data[j][0], 6 * 4);
    end;
  end;

  VirtualBufer.SaveToFile(FileName);
  VirtualBufer.Free;
end;

procedure TAnimClip.Clear;
var
  j: Integer;
begin
  for j := 0 to Length(Keys) - 1 do
    Keys[j].Data := nil;
  Keys := nil;
end;

destructor TAnimClip.Destroy;
begin
  Clear;
{  SetLength(FAnimInfo,0);
  FAnimInfo:=nil;
  FNamesObj.Free   }
end;

constructor THRTimer.Create;
var
  QW: Int64;
begin
  inherited Create;
  Exists := QueryPerformanceFrequency(QW);
  ClockRate := QW//.QuadPart;
end;

function THRTimer.StartTimer: Boolean;
var
  QW: Int64;
begin
  Result := QueryPerformanceCounter(QW);
  StartTime := QW;//.QuadPart;
  Value := 0.0;
end;

procedure THRTimer.ReadTimer;
var
  ET: Int64;
begin
  QueryPerformanceCounter(ET);
  // Result := 1000.0 * (ET.QuadPart - StartTime) / ClockRate;
  Value := (ET - StartTime) / ClockRate;
  if Value > MaxTime then
    StartTimer;
end;

{ TAnimClips}

constructor TAnimClips.Create;
begin
  FStrings:=TStringList.Create;
//  FKeyList:=TStringList.Create;
end;

destructor TAnimClips.Destroy;
begin
  ClearClips;
  FAnimClips:=nil;
//  FKeyList.Free;
  FStrings.Free;
end;

procedure TAnimClips.ClearClips;
  var
  i:integer;
begin
  for i := 0 to Length(FAnimClips) - 1 do
    FAnimClips[i].Destroy;
  FAnimClips := nil;
  FStrings.Clear;
//  FKeyList.Clear;
end;

procedure TAnimClips.SaveAnimToXom(Xom:TXom);
var
  inx, inx1, i, j, inx2, x, k, k2, k3: Integer;
  NumKeys,Zero,xSize:integer;
  s, s1: string;
  VirtualBufer :TMemoryStream;
  KeyTypes: array of TTypeKey;

        function GetID(const KType:TTypeKey):Integer;
        var ik,nk:integer;
        begin
              nk:=Length(KeyTypes)-1;
              for ik:=0 to nk do
                if (KeyTypes[ik].ktype=KType.ktype) and
                 (KeyTypes[ik].objname=KType.objname)then
                 begin Result:=ik; exit; end;
         Result:=-1;
        end;

        procedure TestKeyType(const kType:TTypeKey);
        begin
          if GetID(KType)=-1 then
             begin
             Inc(NumKeys);
             SetLength(KeyTypes,NumKeys);
             KeyTypes[NumKeys-1]:=kType;
             end;
        end;


procedure  SortKeyTypes;
// ���������� ������ �� ������
var
i,j,n,test:integer;
X:TTypeKey;
begin
n:=Length(KeyTypes)-1;
for i:=1 to n do
  for j:=n downto i do
        begin
        test:=CompareStr(KeyTypes[j-1].objname,KeyTypes[j].objname);
        if (test>0)or ((test=0) and (
        (Word(KeyTypes[j-1].ktype)>Word(KeyTypes[j].ktype))
        or
        ((Word(KeyTypes[j-1].ktype)=Word(KeyTypes[j].ktype))
        and
        (KeyTypes[j-1].ktype>KeyTypes[j].ktype))))
                then begin
                X:=KeyTypes[j-1];
                KeyTypes[j-1]:=KeyTypes[j];
                KeyTypes[j]:=X;
                end;
        end;
end;


begin
  Zero:=0;
  if StrArray[CIndx].Update then
      FreeMem(StrArray[CIndx].point);

    VirtualBufer := TMemoryStream.Create;

    Xom.WriteXName(VirtualBufer, Name);  // Name
    // ������� ��� ����� �� ���� ������
    NumKeys:=0;
    SetLength(KeyTypes,NumKeys);    

   j:=Length(FAnimClips)-1;

   for i:=0 to j do
   begin
      x:=Length(FAnimClips[i].Keys)-1;
      for inx:=0 to x do
        TestKeyType(FAnimClips[i].Keys[inx].keytype);
   end;

   // ������������� �����.
   SortKeyTypes;

   VirtualBufer.Write(NumKeys, 4);     // NumKeys
  for x := 0 to NumKeys - 1 do
  begin   // Keys
    VirtualBufer.Write(KeyTypes[x].ktype, 4);  //AnimType
    // �������� ������ ����� ������
    Xom.WriteXName(VirtualBufer, KeyTypes[x].objname); //Object
  end;

   inx2:=Length(FAnimClips); // NumClips
   VirtualBufer.Write(inx2, 4);
  for j := 0 to inx2 - 1 do
  begin
    VirtualBufer.Write(FAnimClips[j].Time, 4); // time
    Xom.WriteXName(VirtualBufer, FAnimClips[j].Name); // name

    inx:=Length(FAnimClips[j].Keys); // NumAnimKeys
    VirtualBufer.Write(inx, 4);

    //    if inx = integer($0101) then inx := 1 else
    for x := 0 to inx - 1 do
    begin   // AnimKeys

      k2 := 257;
      VirtualBufer.Write(k2, 4); // 1 1 0 0
      // ����� ������ ���� ��� ������������� !!!
      k2:=GetID(FAnimClips[j].Keys[x].keytype);// KeyTypes[k2];
      VirtualBufer.Write(k2, 2);

      k3 := 0;
      If FAnimClips[j].Keys[x].keytype.NoAddOp then k3:= 2;
      VirtualBufer.Write(k3, 2);

      VirtualBufer.Write(Zero, 2);
      VirtualBufer.Write(Zero, 4);

      k := Length(FAnimClips[j].Keys[x].Data);
      VirtualBufer.Write(k, 4);
      for i := 0 to k - 1 do
        VirtualBufer.Write(FAnimClips[j].Keys[x].Data[i][0], 6*4);
    end;
  end;

    StrArray[CIndx].Update := true;

    xSize := VirtualBufer.Position;
    StrArray[CIndx].point := AllocMem(xSize);
    Move(VirtualBufer.Memory^, StrArray[CIndx].point^, xSize);
    StrArray[CIndx].size:= xSize;
    VirtualBufer.Free;
end;

Function TAnimClips.BuildAnim(CntrIndx: Integer):Integer;
var
//  AB: TComboBox;
  inx, inx1, i, j, inx2, x, k, k2, k3: Integer;
  p2: Pointer;
  s, s1: string;
  KeyFrame: TKeyFrame;
  AnimType: Cardinal;
  AnimClip: TAnimClip;
  ExpAnim: Boolean;
//  StrList:TStringList;
  KeyTypes: array of TTypeKey;

  function GetOneName(Str: string): string;
  var
    k: Integer;
  begin
    k := RPos('|', Str);
    if k > 0 then
      Result := Copy(Str, k + 1, Length(Str) - k)
    else
      Result := Str;
  end;
begin
  // clear buffer
  ClearClips;
  //------
//  AB := But.AnimBox;
//  AB.Clear;
  cIndx:=CntrIndx;
  BaseClip:=nil;
  p2 := StrArray[CntrIndx].point;
  Name := Xom.GetString(TestByte128(p2)); // Name
  inx1 := Integer(p2^);
  Inc(Longword(p2), 4);   // NumKeys
  //    s:=format('"%s" KeyTypes[%d]',[s,inx]);
//  FKeyList.Duplicates:=dupIgnore;
  SetLength(KeyTypes, inx1);
  for x := 0 to inx1 - 1 do
  begin   // Keys
    AnimType := Cardinal(p2^);
    Inc(Longword(p2), 4);            //AnimType
    s := Xom.GetString(TestByte128(p2)); //Object
 //   KeyTypes[x].fullname:=s;
    KeyTypes[x].objname := GetOneName(s);
//    FKeyList.Add(KeyTypes[x].objname);
    KeyTypes[x].ktype := AnimType;
  end;

  inx2 := Integer(p2^);
  Inc(Longword(p2), 4);  // NumClips

 // SetLength(AnimClips, inx2);

  for j := 0 to inx2 - 1 do
  begin

    AnimClip      := TAnimClip.Create;
    AnimClip.Time := Single(p2^);
    Inc(Longword(p2), 4);  // time
    s1 := Xom.GetString(TestByte128(p2));  // name
    if (s1='Base') then BaseClip:=AnimClip;
    AnimClip.Name := s1;
    AddClip(AnimClip);
  {  AnimClip.FNamesObj:=TStringList.Create;
    AnimClip.FNamesObj.Assign(StrList); 
    SetLength(AnimClip.FAnimInfo,StrList.Count);   }
 //   AB.Items.Add(s1);
    inx     := Word(p2^);  // NumAnimKeys
    ExpAnim := (inx = 256) or (inx=257);
    if ExpAnim then
      inx := inx1
    else
      Inc(Longword(p2), 4);
    SetLength(AnimClip.Keys, inx);
    //    if inx = integer($0101) then inx := 1 else
    for x := 0 to inx - 1 do
    begin   // AnimKeys
      k2 := Word(p2^);
      if k2 = 256 then
      begin
        Inc(Longword(p2), 16);
        Continue;
      end;
      Inc(Longword(p2), 4); // 1 1 0 0
      if not ExpAnim then
      begin
        k2 := Word(p2^);
        Inc(Longword(p2), 2);
      end
      else
        k2 := x;
      k3 := Word(p2^);
      Inc(Longword(p2), 2);

      Inc(Longword(p2), 6);
      k := Integer(p2^);
      Inc(Longword(p2), 4);
      AnimClip.Keys[x].keytype := KeyTypes[k2];
      AnimClip.Keys[x].keytype.NoAddOp := (k3 = 2);
      SetLength(AnimClip.Keys[x].Data, k);
      for i := 0 to k - 1 do
      begin
        Move(p2^, KeyFrame[0], 6 * 4);
        Inc(Longword(p2), 6 * 4);
        AnimClip.Keys[x].Data[i] := KeyFrame;
      end;
    end;
  end;
//  StrList.Free;
  KeyTypes := nil;
  Result:=inx2;
end;

function TAnimClips.GetItem(Index:String):TAnimClip;
begin
  Result := FAnimClips[FStrings.IndexOf(Index)];
end;

function TAnimClips.GetItemID(Index: Integer): TAnimClip;
begin
  Result := FAnimClips[Index];
end;

function TAnimClips.AddClip(Clip:TAnimClip):Integer;
begin
  SetLength(FAnimClips, Length(FAnimClips)+1);
  Result:=FStrings.Add(Clip.Name);
  FAnimClips[Result]:=Clip;
end;

procedure TAnimClips.SetItem(Index:String;Value:TAnimClip);
var i:integer;
begin
// ������� ���� ������, ���� �� ������� �� ��������.
  i:=FStrings.IndexOf(Index);
  if (i>-1) then FAnimClips[i]:=Value;
end;

function TAnimClips.GetNameAnimType(AType: Cardinal): string;
begin
  Result := '';
  case Word(AType) of
    ATypePosition: Result := 'pos';
    ATypeRotation: Result := 'rotation';
    ATypeScale:
    Result := 'scale';
    AType2DTex:    Result := 'texpos';
    ATypeReScale:  Result := 'rescale';
    ATypeTexture, ATypeChilds:
    begin
      Result := 'childs';
      Exit;
    end;
  end;
  case Byte(AType shr 24) of
    ATypeX: Result := Result + '.x';
    ATypeY: Result := Result + '.y';
    ATypeZ: Result := Result + '.z';
  end;
end;

{
function  TAnimClip.GetAnimFrame(Name: string):TAnimInfo;
var
  i,m:integer;
begin
  i:=FNamesObj.IndexOf(Name);
  if (i>-1) then
        Result:=FAnimInfo[FNamesObj.IndexOf(Name)];
end; }


function LineFacet(p1,p2:TXYZ;Axis:TAxis;var p:TXYZ;wd,wd0:TXYZ):boolean;
var
   d:real;
   denom,mu,length:real;
   n,pa,pb,pc:TXYZ;//,pa1,pa2,pa3
begin
   result:=FALSE;
    p.x:=0;p.y:=0;p.z:=0;
    pa.x:=0;pa.y:=0;pa.z:=0;
   case Axis of
   Axis_X: begin    pb.x:=1;pb.y:=0;pb.z:=0;    pc:=wd; end;
   Axis_XY: begin    pb.x:=1;pb.y:=0;pb.z:=0;    pc.x:=0;pc.y:=1;pc.z:=0; end;
   Axis_Y: begin    pb.x:=0;pb.y:=1;pb.z:=0;    pc:=wd;  end;
   Axis_YZ: begin    pb.x:=0;pb.y:=1;pb.z:=0;    pc.x:=0;pc.y:=0;pc.z:=1; end;
   Axis_Z: begin    pb.x:=0;pb.y:=0;pb.z:=1;    pc:=wd; end;
   Axis_XZ: begin    pb.x:=1;pb.y:=0;pb.z:=0;    pc.x:=0;pc.y:=0;pc.z:=1; end;
   Axis_XYZ: begin  pb:=wd;  pc:=wd0;end;
   end;
   //* Calculate the parameters for the plane */
   n.x := (pb.y - pa.y)*(pc.z - pa.z) - (pb.z - pa.z)*(pc.y - pa.y);
   n.y := (pb.z - pa.z)*(pc.x - pa.x) - (pb.x - pa.x)*(pc.z - pa.z);
   n.z := (pb.x - pa.x)*(pc.y - pa.y) - (pb.y - pa.y)*(pc.x - pa.x);
   //Normalize(n);
   length := Sqrt(n.x*n.x+n.y*n.y+n.z*n.z);
   n.x:= n.x/length;
   n.y:= n.y/length;
   n.z:= n.z/length;

   d := - n.x * pa.x - n.y * pa.y - n.z * pa.z;

   //* Calculate the position on the line that intersects the plane */
   denom := n.x * (p2.x - p1.x) + n.y * (p2.y - p1.y) + n.z * (p2.z - p1.z);
   if (ABS(denom) < 1.0E-8)        //* Line and plane don't intersect */
     then exit;
   mu := - (d + n.x * p1.x + n.y * p1.y + n.z * p1.z) / denom;
   if (mu < 0) or( mu > 1)  //* Intersection not along line segment */
      then exit;
  p.x := p1.x + mu * (p2.x - p1.x);
  p.y := p1.y + mu * (p2.y - p1.y);
  p.z := p1.z + mu * (p2.z - p1.z);
   case Axis of
   Axis_X:begin p.y:=0;p.z:=0;end;
   Axis_Y:begin p.x:=0;p.z:=0;end;
   Axis_Z:begin p.y:=0;p.x:=0;end;
   end;
   result:=TRUE;
end;

procedure GetProjectVert(X,Y:real;axis:TAxis;var p:TXYZ);
var
  p1,p2,wd,wd0,wd1:TXYZ;
  vp: TVector4i;
  PrMatrix, VmMatrix: TGLMatrixd4;
begin
    glGetIntegerv(GL_VIEWPORT, @vp);
    glGetDoublev(GL_MODELVIEW_MATRIX, @VmMatrix);
    glGetDoublev(GL_PROJECTION_MATRIX, @PrMatrix);
   case axis of
   Axis_Y,Axis_X,Axis_Z,Axis_XYZ:
        begin
        wd.x:=0;   wd.y:=0;   wd.z:=0;
        gluProject(wd.x, wd.y, wd.z, VmMatrix, PrMatrix, vp,
        @wd.x, @wd.y, @wd.z);
        gluUnProject(wd.x, wd.y+10, wd.z, VmMatrix, PrMatrix, vp,
        @wd0.x, @wd0.y, @wd0.z);
        gluUnProject(wd.x+10, wd.y, wd.z, VmMatrix, PrMatrix, vp,
        @wd1.x, @wd1.y, @wd1.z);
        if axis=Axis_XYZ then
        wd:=wd0 else begin
        wd.x:=(wd0.x+wd1.x)/2;
        wd.y:=(wd0.y+wd1.y)/2;
        wd.z:=(wd0.z+wd1.z)/2;
                        end;
        end;
   end;

    gluUnProject(X, Y, 0, VmMatrix, PrMatrix, vp,
    @p1.x, @p1.y, @p1.z);
    gluUnProject(X, Y, 1, VmMatrix, PrMatrix, vp,
    @p2.x, @p2.y, @p2.z);
//   case axis of
//   Axis_Y,Axis_X,Axis_Z: begin
//    test1:=LineFacet(p1,p2,axis, p,wd0);
//    test2:=LineFacet(p1,p2,axis, p0,wd1);
  //  if (p.x<>p0.x) and (p.y<>p0.y) and (p.z<>p0.z) then
  //  p:=p0;test1:=test2;
//    if not test1 then p:=p0;
 //   end;
 //  else
   LineFacet(p1,p2,axis, p,wd,wd1);
  // end;
 end;

 
function GetAngle(x,y,x1,y1:single):single;
var z,z1,dir:single;
begin
if (y<0) then dir:=pi else dir:=0;
z:=dir+ArcTan(x/y);
if (y1<0) then dir:=pi else dir:=0;
z1:=dir+ArcTan(x1/y1);
//if z>z1 then dir:=pi else dir:=0;
result:=radtodeg(z-z1);
end;

var
  ActiveMatrix, TempMatrix: TMatrix;
  NKey:Tver;
  MovePoxPos,  ScalePoxPos,RotatePoxPos:Tver;
  p:TXYZ;
  theta,alpha,beta,gamma:single;

function findBezier(x,a1,b1,c1,d1,a2,b2,c2,d2:Single):Single;

   function BezierF(t,a,b,c,d:Single):Single;
   var
   t1:Single;
   begin
   t1:=(1-t);
   result:=t1*t1*t1*a + 3*t*t1*t1*b + 3*t*t*t1*c + t*t*t*d;
   end;

   function find(t1,t2,t3:single):single;
   var xdiv:single;
   begin
   xdiv:=BezierF(t2,a1,b1,c1,d1);
   if abs(xdiv-x)<0.001 then begin result:=t2; exit; end;
   if xdiv>x then
        result:=find(t1,(t1+t2)/2,t2)
        else
        result:=find(t2,(t2+t3)/2,t3);
   end;
begin
   result:=BezierF(find(0,0.5,1.0),a2,b2,c2,d2);
end;


function TAnimClip.GenAnimFrame(Name: string; T: TTrans; Bone: Boolean; ActiveMove:Boolean):TAnimInfo;
var
  i, j, m, n,m1,i1, k1, k2, inx: Integer;
  Value:Single;
  Key: TKeyData;
  AType: Cardinal;
  RotTmp: ^TVer;
  ZeroV:TVer;
  function GetMidValue(Key1, Key2: TKeyFrame): Single;
  var
    divTime: Single;
  //  it:integer;
    a,b,c,d,t:single;
  //  a1,b1,c1,d1,t0:single;
  begin
  //  divTime := AnimTimer.Value - Key1[4];
    if (Key2[4] = Key1[4]) then
      Result := Key1[5]
    else
     begin
        a := Key1[4] + (Key2[4] - Key1[4]) * cos(Key1[3]) * Key1[2] / 3;
        b := Key1[5] + (Key2[5] - Key1[5]) * sin(Key1[3]) * Key1[2] / 3;
        c := Key2[4] - (Key2[4] - Key1[4]) * cos(Key2[1]) * Key2[0] / 3;
        d := Key2[5] - (Key2[5] - Key1[5]) * sin(Key2[1]) * Key2[0] / 3;
        result:=findBezier(AnimTimer.Value,Key1[4],a,c,Key2[4],Key1[5],b,d,Key2[5]);
        end;
     // Result := Key1[5] + (Key2[5] - Key1[5]) * divTime / (Key2[4] - Key1[4]);
  end;

  function GetMidValueInt(Key1, Key2: TKeyFrame): Single;
  var
    divTime: Single;
  begin
    divTime := AnimTimer.Value - Key1[4];
    if (Key2[4] = Key1[4]) then
      Result := Key1[5]
    else if divTime / (Key2[4] - Key1[4]) > 0.5 then
      Result := Key2[5]
    else
      Result := Key1[5];
  end;

begin
  if Name='' then exit;
  if But.TrackPanel.CanSlide then
  AnimTimer.Value:=But.TrackPanel.Slider;
{  if BaseClip<>nil then begin
   ZeroV[0]:=0;
   ZeroV[1]:=0;
   ZeroV[2]:=0;

  T.Pos:=ZeroV;
   T.Rot:=ZeroV;
   T.RotOrient:=ZeroV;
   T.JointOrient:=ZeroV;
   T.Size:=ZeroV;    
 end;                }

  m := Length(Keys) - 1;
  for i := 0 to m do
    if (Keys[i].keytype.objname = Name) then
    begin
      Key := Keys[i];
      Result.True:=True;
      n := Length(Key.Data) - 1;
      k1 := 0;
      k2 := 0;
      for j := 0 to n do
      begin
        if (j = 0) and (Key.Data[j][4] > AnimTimer.Value) then
        begin
          k1 := j; 
          k2 := j;
          Break;
        end;
        if (j = n) then
        begin
          k1 := n;
          k2 := n;
          Break;
        end
        else if (Key.Data[j][4] <= AnimTimer.Value) and
          (AnimTimer.Value < Key.Data[j + 1][4]) then
        begin
          k1 := j;
          k2 := j + 1;
          Break;
        end;
      end;

      AType := Key.keytype.ktype;
      Value :=  GetMidValue(Key.Data[k1], Key.Data[k2]);
      if (BaseClip<>nil)and (BaseClip<>self) then begin
                m1 := Length(BaseClip.Keys) - 1;
                for i1 := 0 to m1 do
                if (BaseClip.Keys[i1].keytype.objname = Name)
                and (BaseClip.Keys[i1].keytype.ktype = AType) then begin
                if Word(AType)=ATypeReScale then
                Value:=(Value+BaseClip.Keys[i1].Data[0][5]) /2 else
                Value:=(Value+BaseClip.Keys[i1].Data[0][5]);
                break;
                end;

      end;

      inx := Byte(AType shr 24);
      case Word(AType) of
        ATypePosition :
           // if Key.keytype.NoAddOp then
         //   T.Pos[inx]:=T.Pos[inx]+Value;// else
          T.Pos[inx] := Value;
        //?
        ATypeRotation:
          if Bone then
            T.JointOrient[inx] :=  Value
          else
            T.Rot[inx] := Value;
        ATypeScale:
          T.Size[inx] := Value;
        AType2DTex:
          Result.TCoord[inx]:=Value;//GetMidValueInt(Key.Data[k1], Key.Data[k2]);
        ATypeTexture,ATypeChilds:
          Result.Child := Trunc(GetMidValueInt(Key.Data[k1], Key.Data[k2]));
        ATypeReScale:
        T.Size[inx] := Value; // ???
      end;
     end;

  if ActiveMove then begin
  glPushMatrix;
        glGetFloatv(GL_MODELVIEW_MATRIX,@ActiveMatrix);// ���������� ������� �������
        // ������ ����������

        glTranslatef(T.Pos[0], T.Pos[1], T.Pos[2]); // ������� �� �� ��� �������� ��������

        if MoveReady then begin
                GetProjectVert(PTarget.X,PTarget.Y,MAxis,p);

                if MoveFirts then begin     // ���������� ��������� �����
                MoveFirts:=false; wd:=p;
                end;

                wdup.x:=-wd.x+p.x;   wdup.y:=-wd.y+p.y;  wdup.z:=-wd.z+p.z;

                glTranslatef(wdup.x, wdup.y, wdup.z);                              // ���������� � ��������

               T.Pos[0]:=T.Pos[0]+wdup.x;   // ���������� ������
               T.Pos[1]:=T.Pos[1]+wdup.y;
               T.Pos[2]:=T.Pos[2]+wdup.z;
               
        end;

        if But.Move^ then
                begin
                glGetFloatv(GL_MODELVIEW_MATRIX,@ObjMatrix);  // ��������� �������
                NKey:=T.Pos;
                end;

        glGetFloatv(GL_MODELVIEW_MATRIX, @TempMatrix);

        glRotatef(RadToDeg(T.Rot[2]), 0, 0, 1); //Z
        glRotatef(RadToDeg(T.Rot[1]), 0, 1, 0); //y
        glRotatef(RadToDeg(T.Rot[0]), 1, 0, 0); //X

        if Bone then begin
                glGetFloatv(GL_MODELVIEW_MATRIX,@TempMatrix);
                glRotatef(RadToDeg(T.JointOrient[2]), 0, 0, 1);//Z
                glRotatef(RadToDeg(T.JointOrient[1]), 0, 1, 0);//y
                glRotatef(RadToDeg(T.JointOrient[0]), 1, 0, 0);//X
                RotTmp:=@T.JointOrient;
        end else
                RotTmp:=@T.Rot;

        if RotateReady then begin
           case MAxis of
                Axis_X:begin
                        GetProjectVert(PTarget.X,PTarget.Y,Axis_YZ,p);
                        end;
                Axis_Y:begin
                        glLoadMatrixf(@TempMatrix);
                        glRotatef(RadToDeg(RotTmp^[2]), 0, 0, 1); //Z
                        glRotatef(RadToDeg(RotTmp^[1]), 0, 1, 0); //y
                        GetProjectVert(PTarget.X,PTarget.Y,Axis_XZ,p);
                        end;
                Axis_Z:begin
                         glLoadMatrixf(@TempMatrix);
                         glRotatef(RadToDeg(RotTmp^[2]), 0, 0, 1); //Z
                         GetProjectVert(PTarget.X,PTarget.Y,Axis_XY,p);
                        end;
                end;

                if RotateFirts then begin
                        RotateFirts:=false; wd:=p;
                end;

                glPushAttrib(GL_COLOR_MATERIAL or GL_ENABLE_BIT);
                glDisable(GL_TEXTURE_2D);
                glDisable(GL_LIGHTING);
                glColor3f(1,1,0);
                glBegin(GL_LINES);
                        glVertex3f(0, 0, 0);
                        glVertex3f(p.x, p.y, p.z);
                        glVertex3f(0, 0, 0);
                        glVertex3f(wd.x, wd.y, wd.z);
                glEnd;
                glPopAttrib;

                glLoadMatrixf(@TempMatrix);

                alpha:=0;  beta:=0;  gamma:=0;

                case MAxis of
                Axis_X:alpha:=GetAngle(wd.y,wd.z,p.y,p.z);
                Axis_Y:beta:=GetAngle(wd.z,wd.x,p.z,p.x);
                Axis_Z:gamma:=GetAngle(wd.x,wd.y,p.x,p.y);
                end;
                RotatePoxPos[0]:=DegToRad(alpha);
                RotatePoxPos[1]:=DegToRad(beta);
                RotatePoxPos[2]:=DegToRad(gamma);

                RotTmp^[0]:=RotTmp^[0]+DegToRad(alpha);
                RotTmp^[1]:=RotTmp^[1]+DegToRad(beta);
                RotTmp^[2]:=RotTmp^[2]+DegToRad(gamma);
                
                glRotatef(RadToDeg(RotTmp^[2]), 0, 0, 1);//Z                     //
                glRotatef(RadToDeg(RotTmp^[1]), 0, 1, 0);//y                      //
                glRotatef(RadToDeg(RotTmp^[0]), 1, 0, 0);//X                     //
                glGetFloatv(GL_MODELVIEW_MATRIX,@ObjMatrix);
        end;
        if But.Rotate^ then  begin
                glGetFloatv(GL_MODELVIEW_MATRIX,@ObjMatrix);  // ��������� �������
                NKey:=RotTmp^;
                end;
        if Bone then begin
                glRotatef(RadToDeg(T.RotOrient[2]), 0, 0, 1);//Z
                glRotatef(RadToDeg(T.RotOrient[1]), 0, 1, 0);//y
                glRotatef(RadToDeg(T.RotOrient[0]), 1, 0, 0);//X
        end;



        glGetFloatv(GL_MODELVIEW_MATRIX,@TempMatrix);

        glScalef(T.Size[0], T.Size[1], T.Size[2]);

        if ScaleReady then begin
           GetProjectVert(PTarget.X,PTarget.Y,MAxis,p);
                if ScaleFirts then begin
                ScalePoxPos:=T.Size;
                ScaleFirts:=false; wd:=p;
                end;
                wdup.x:=(-wd.x+p.x)+1;  wdup.y:=(-wd.y+p.y)+1; wdup.z:=(-wd.z+p.z)+1;

              if wdup.x<0 then  wdup.x:=0;
               if wdup.y<0 then  wdup.y:=0;
                if wdup.z<0 then  wdup.z:=0;

                   if ShiftOn then begin
                                 case MAxis of
                                 Axis_XY:begin wdup.x:=Max(wdup.x, wdup.y);wdup.y:=wdup.x;end;
                                 Axis_YZ:begin wdup.y:=Max(wdup.y, wdup.z);wdup.z:=wdup.y;end;
                                 Axis_XZ:begin wdup.x:=Max(wdup.x, wdup.z);wdup.z:=wdup.x;end;
                                 end;
                        end;
                case MAxis of
                Axis_X:glScalef(wdup.x, 1, 1);
                Axis_Y:glScalef(1, wdup.y, 1);
                Axis_Z:glScalef(1, 1, wdup.z);
                Axis_XY:glScalef(wdup.x, wdup.y, 1);
                Axis_YZ:glScalef(1, wdup.y, wdup.z);
                Axis_XZ:glScalef(wdup.x, 1, wdup.z);
                Axis_XYZ:begin wdup.x:=Max(wdup.x, wdup.z);
                        wdup.y:=wdup.x; wdup.z:=wdup.x;
                        glScalef(wdup.x,wdup.y,wdup.z); end;
                end;

                case MAxis of
                Axis_X,Axis_XY,Axis_XZ,Axis_XYZ:
                T.Size[0]:=T.Size[0]*wdup.x;
                end;

                case MAxis of
                Axis_Y,Axis_XY,Axis_YZ,Axis_XYZ:
                T.Size[1]:=T.Size[1]*wdup.y;
                end;

                case MAxis of
                Axis_Z,Axis_XZ,Axis_YZ,Axis_XYZ:
                T.Size[2]:=T.Size[2]*wdup.z;
                end;
                
        end;
        if But.Scale^ then begin
                glGetFloatv(GL_MODELVIEW_MATRIX,@ObjMatrix);  // ��������� �������
                NKey:=T.Size;
                end;
 // ������ ���������� ��������
  glPopMatrix;
   end;

if Result.True then begin
  glPushMatrix;
  if Bone then
      Result.Tm:=GetMatrix2(T.Pos, T.Rot, T.JointOrient, T.RotOrient, T.Size)
  else
      Result.Tm:=GetMatrix(T.Pos, T.Rot, T.Size);

  glPopMatrix;    end;

//  FAnimInfo[FNamesObj.IndexOf(Name)]:=AI;

end;

procedure  TAnimClip.SortKeys;
// ���������� ������ �� ������
var
i,j,n,test:integer;
X:TKeyData;
begin
n:=Length(Keys)-1;
for i:=1 to n do
  for j:=n downto i do
        begin
        test:=CompareStr(Keys[j-1].keytype.objname,Keys[j].keytype.objname);
        if (test>0)or ((test=0) and (
        (Word(Keys[j-1].keytype.ktype)>Word(Keys[j].keytype.ktype))
        or
        ((Word(Keys[j-1].keytype.ktype)=Word(Keys[j].keytype.ktype))
        and
        (Keys[j-1].keytype.ktype>Keys[j].keytype.ktype))))
                then begin
                X:=Keys[j-1];
                Keys[j-1]:=Keys[j];
                Keys[j]:=X;
                end;
        end;
end;

procedure  TAnimClip.SortKeyData(KData:PKeyData);
var
i,j,n:integer;
X:TKeyFrame;
begin
n:=Length(KData.Data)-1;
for i:=1 to n do
  for j:=n downto i do
        if KData.Data[j-1][4]>KData.Data[j][4] then begin
                X:=KData.Data[j-1];
                KData.Data[j-1]:=KData.Data[j];
                KData.Data[j]:=X;
        end;
end;

procedure TAnimClip.AddKData(KType:TTypeKey);
var
inx:integer;
Fval:single;
begin
ActiveMesh:=Xom.Mesh3D.GetMeshOfName(KType.objname);
inx:=byte(Ktype.ktype shr 24);
FVal:=0.0;
Case Word(Ktype.ktype) of
ATypePosition:FVal:=ActiveMesh.Transform.Pos[inx];
ATypeRotation:FVal:=ActiveMesh.Transform.Rot[inx];
ATypeScale:FVal:=ActiveMesh.Transform.Size[inx];
end;
AddKey(nil,0.0,FVal,FVal,KType.ktype);
end;

procedure TAnimClip.DeleteKData(KType:TTypeKey);
var i,n:integer;
DeleteFlag:Boolean;
begin
n:=Length(Keys)-1;
DeleteFlag:=false;
for i:=0 to n do   begin
if DeleteFlag then Keys[i-1]:=Keys[i] else
if (Keys[i].keytype.ktype=KType.ktype) and
(Keys[i].keytype.objname=KType.objname) then DeleteFlag:=true;
end;
SetLength(Keys[n].Data,0);
SetLength(Keys,n);
//KType.objname
end;

procedure TAnimClip.DeleteKey(Key:PKeyFrame);
var
i,ii,n,nn,inx:integer;
begin
n:=Length(Keys)-1;
for i:=0 to n do begin
        inx:=FindKey(@Keys[i],Key);
        if inx<>-1 then
         begin
         nn:=Length(Keys[i].Data);
         if nn=2 then exit;
         for ii:=inx to nn-2 do
                Keys[i].Data[ii]:=Keys[i].Data[ii+1];
         SetLength(Keys[i].Data,nn-1);
         exit;
         end;
        end;
end;

function TAnimClip.FindKey(KData:PKeyData;SelKey:PKeyFrame):integer;
var
  ii,num:integer;
begin
   Result:=-1;
   num:=Length(KData.Data);
   for ii:=0 to num-1 do
     if @KData.Data[ii]=SelKey then begin Result:=ii; Exit; end;
end;

function TAnimClip.FindTimeKey(KData:PKeyData;FTime:Single):integer;
var
  ii,num:integer;
begin
   Result:=-1;
   num:=Length(KData.Data);
   for ii:=0 to num-1 do
     if KData.Data[ii][4]=FTime then begin Result:=ii; Exit; end;
end;

function TAnimClip.FindKeyNameType(FName:String;AType:integer):PKeyData;
var
i,j:integer;
begin
Result:=nil;
      j:=Length(Keys);
      for i:=0 to j-1 do
        if  (Keys[i].keytype.ktype=AType)and(Keys[i].keytype.objname=FName) then
                begin Result:=@Keys[i]; exit; end;
end;

procedure  TAnimClip.AddKey(KData:PKeyData;CurTime:Single;FirstValue:Single;NewValue:Single;AType:Integer);
var
 n,i,m1,i1,t:integer;
 value:Single;
begin
  //    Value :=  GetMidValue(Key.Data[k1], Key.Data[k2]);
      if (BaseClip<>nil)and (BaseClip<>self) then begin
                m1 := Length(BaseClip.Keys) - 1;
                for i1 := 0 to m1 do
                if (BaseClip.Keys[i1].keytype.objname = ActiveMesh.Name)
                and (BaseClip.Keys[i1].keytype.ktype = AType) then begin
                if Word(AType)=ATypeReScale then
                  begin
                  NewValue:=2*NewValue-BaseClip.Keys[i1].Data[0][5];
                  FirstValue:=2.0;
                  end
                  else
                  begin
                  NewValue:=NewValue-BaseClip.Keys[i1].Data[0][5];
                  FirstValue:=0;
                  end;
                break;
                end;
      end;
    // ������� ����, �������� � ������ ��� ������� ������.
    // ������������� ������
    if KData=nil then begin
        n:=Length(Keys);
        SetLength(Keys,n+1);
        Keys[n].keytype.objname:=ActiveMesh.Name;
    //    Keys[n].keytype.fullname:=GetFullName(ActiveMesh.Name);
        Keys[n].keytype.ktype:=AType;
        SetLength(Keys[n].Data,2);
        Keys[n].Data[0][0]:=1.0;
        Keys[n].Data[0][1]:=0.0;
        Keys[n].Data[0][2]:=1.0;
        Keys[n].Data[0][3]:=0.0;
        Keys[n].Data[0][4]:=0.0;
        Keys[n].Data[0][5]:=FirstValue;

        Keys[n].Data[1]:=Keys[n].Data[0];
        Keys[n].Data[1][4]:=CurTime;
        Keys[n].Data[1][5]:=NewValue;
        SelectKey:=@Keys[n].Data[1];
    end else begin
        If (SelectKey<>nil) and
        (SelectKey[4]=But.TrackPanel.Slider) and
        (FindKey(KData,SelectKey)<>-1) then
        begin
        SelectKey[5]:=NewValue;
        exit;
        end;
        t:=FindTimeKey(KData,CurTime);
        if t<>-1 then begin
        KData.Data[t][5]:=NewValue;
        exit;
        end;
        n:=Length(KData.Data);
        SetLength(KData.Data,n+1);
        KData.Data[n][0]:=1.0;
        KData.Data[n][1]:=0.0;
        KData.Data[n][2]:=1.0;
        KData.Data[n][3]:=0.0;
        KData.Data[n][4]:=CurTime;
        KData.Data[n][5]:=NewValue;
        SortKeyData(KData);
        for i:=0 to n-1 do
        if KData.Data[i][4]=CurTime then
                SelectKey:=@KData.Data[i];
    end;
    // ���� ����� ����� ��� �����,
    // �������� ��� �������� ����
    // ������ ���� ���� ��� ����.
end;

procedure TAnimClip.UpdateAnimClip;
var
 FrameTime:Single;
 i,num,inx:integer;

 M,R,S,RS,T2d,Ch: array [0..2]of  PKeyData;
begin
 if not
 ((MoveMode and MoveReady )or
  (RotateMode and RotateReady)or
   (ScaleMode and ScaleReady)) then exit;
  FrameTime:=But.TrackPanel.Slider;
  for i:=0 to 2 do begin
        M[i]:=nil;
        R[i]:=nil;
        S[i]:=nil;
        T2d[i]:=nil;
        Ch[i]:=nil;
        RS[i]:=nil;
  end;

  num := Length(Keys) - 1;
  for i := 0 to num do
    if (Keys[i].keytype.objname = ActiveMesh.Name) then
    begin
     inx := Byte(Keys[i].keytype.ktype shr 24);
     case Word(Keys[i].keytype.ktype) of
       ATypePosition: M[inx]:=@Keys[i];
       ATypeRotation: R[inx]:=@Keys[i];
       ATypeScale: S[inx]:=@Keys[i];
       AType2DTex: T2d[inx]:=@Keys[i];
       ATypeTexture,ATypeChilds: Ch[inx]:=@Keys[i];
       ATypeReScale: RS[inx]:=@Keys[i];
     end;

    end;

    if MoveMode and MoveReady then begin
      MoveReady:=false;
      case MAxis of
                Axis_X,Axis_XY,Axis_XZ,Axis_XYZ:
                AddKey(M[ATypeX],FrameTime,
                ActiveMesh.Transform.Pos[ATypeX],NKey[ATypeX],
                ATypePosition+(ATypeX shl 24));
      end;
      case MAxis of
                Axis_Y,Axis_XY,Axis_YZ,Axis_XYZ:
                AddKey(M[ATypeY],FrameTime,
                ActiveMesh.Transform.Pos[ATypeY],NKey[ATypeY],
                ATypePosition+(ATypeY shl 24));
      end;
      case MAxis of
                Axis_Z,Axis_XZ,Axis_YZ,Axis_XYZ:
                AddKey(M[ATypeZ],FrameTime,
                ActiveMesh.Transform.Pos[ATypeZ],NKey[ATypeZ],
                ATypePosition+(ATypeZ shl 24));
      end;
   end;

if RotateMode and RotateReady then begin
      RotateReady:=false;
      case MAxis of
                Axis_X,Axis_XY,Axis_XZ,Axis_XYZ:
                AddKey(R[ATypeX],FrameTime,
                ActiveMesh.Transform.Rot[ATypeX],NKey[ATypeX],
                ATypeRotation+(ATypeX shl 24));
      end;
      case MAxis of
                Axis_Y,Axis_XY,Axis_YZ,Axis_XYZ:
                AddKey(R[ATypeY],FrameTime,
                ActiveMesh.Transform.Rot[ATypeY],NKey[ATypeY],
                ATypeRotation+(ATypeY shl 24));
      end;
      case MAxis of
                Axis_Z,Axis_XZ,Axis_YZ,Axis_XYZ:
                AddKey(R[ATypeZ],FrameTime,
                ActiveMesh.Transform.Rot[ATypeZ],NKey[ATypeZ],
                ATypeRotation+(ATypeZ shl 24));
      end;
end;

if ScaleMode and ScaleReady then begin
      ScaleReady:=false;
      case MAxis of
                Axis_X,Axis_XY,Axis_XZ,Axis_XYZ:
                AddKey(RS[ATypeX],FrameTime,
                ActiveMesh.Transform.Size[ATypeX],NKey[ATypeX],
                ATypeReScale+(ATypeX shl 24));
      end;
      case MAxis of
                Axis_Y,Axis_XY,Axis_YZ,Axis_XYZ:
                AddKey(RS[ATypeY],FrameTime,
                ActiveMesh.Transform.Size[ATypeY],NKey[ATypeY],
                ATypeReScale+(ATypeY shl 24));
      end;
      case MAxis of
                Axis_Z,Axis_XZ,Axis_YZ,Axis_XYZ:
                AddKey(RS[ATypeZ],FrameTime,
                ActiveMesh.Transform.Size[ATypeZ],NKey[ATypeZ],
                ATypeReScale+(ATypeZ shl 24));
      end;
end;
SortKeys;
end;

constructor TXom.Create;
begin
  inherited Create;
  Mesh3D := TMesh.Create;
end;

constructor TMesh.Create;
var
  n: Integer;
begin
  inherited Create;
  Name := 'dummy';
  Attribute.ZBufferFuncVal := 1;
  Attribute.ZBuffer := true;
  Attribute.CullFace := true;
  Transform.TransType := TTNone;
  for n := 0 to 2 do
    Transform.Size[n] := 1.0;
  for n := 1 to 4 do
    Transform.TrMatrix[n][n] := 1.0;
end;

type
  TVector4 = array[1..4] of Single;

function VertAddVert(v1, v2: TVer): TVer;
begin
  Result[0] := v1[0] + v2[0];
  Result[1] := v1[1] + v2[1];
  Result[2] := v1[2] + v2[2];
end;

function ValXVert(Val: Single; vect: TVer): TVer;
begin
  Result[0] := Val * vect[0];
  Result[1] := Val * vect[1];
  Result[2] := Val * vect[2];
end;

function MatrixInvert(const m: TMatrix): TMatrix;
var
  i: Integer;
  l: array [1..3] of Glfloat;
begin
  for i := 1 to 3 do
  begin
    l[i] := 1.0 / (Sqr(m[i, 1]) + Sqr(m[i, 2]) + Sqr(m[i, 3]));
    Result[1, i] := m[i, 1] * l[i];
    Result[2, i] := m[i, 2] * l[i];
    Result[3, i] := m[i, 3] * l[i];
    Result[i, 4] := 0.0;
  end;
  Result[4, 4] := 1.0;

  for i := 1 to 3 do
    Result[4, i] := -(m[4, 1] * m[i, 1] * l[i] + m[4,
      2] * m[i, 2] * l[i] + m[4, 3] * m[i, 3] * l[1]);
end;

function MatrXMatr(M1, M2: TMatrix): TMatrix;
var
  i, j, k: Integer;
begin
  for i := 1 to 4 do
    for j := 1 to 4 do
    begin
      Result[i, j] := 0;
      for k := 1 to 4 do
        Result[i, j] := Result[i, j] + m1[i, k] * m2[k, j];
    end;
end;

function MatrXVert(Matrix: TMatrix; V0, V1, V2: Single): Tver;
var
  i, j: Integer;
  b, c: TVector4;
begin
  b[1] := V0;
  b[2] := V1;
  b[3] := V2;
  b[4] := 1;
  //SetLEngth(c,Length(b));
  for i := 1 to 4 do 
  begin
    c[i] := 0;
    for j := 1 to 4 do 
      c[i] := c[i] + Matrix[j, i] * b[j];
  end;
  Result[0] := c[1];
  Result[1] := c[2];
  Result[2] := c[3];
end;

procedure TMesh.GetBox(var Box: TBox);
var
  i: Integer;
  //TempBox:Tbox;
  vector: Tver;
  Matrix: TMatrix;
  procedure TestVect(Vector: Tver);
  begin
    with Box do 
    begin
      if Vector[0] > Xmax then Xmax := Vector[0];
      if Vector[0] < Xmin then Xmin := Vector[0];
      if Vector[1] > Ymax then Ymax := Vector[1];
      if Vector[1] < Ymin then Ymin := Vector[1];
      if Vector[2] > Zmax then Zmax := Vector[2];
      if Vector[2] < Zmin then Zmin := Vector[2];
    end;
  end;
begin
  glPushMatrix();
  glMultMatrixf(@Transform.TrMatrix);
  if not ((SizeBox.Xmax = 0) and (SizeBox.Xmin = 0) and
    (SizeBox.Ymax = 0) and (SizeBox.Ymin = 0) and
    (SizeBox.Zmax = 0) and (SizeBox.Zmin = 0)) then
  begin
    glGetFloatv(GL_MODELVIEW_MATRIX, @Matrix);

    with SizeBox do 
    begin
      Vector := MatrXVert(Matrix, Xmin, Ymin, Zmin);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmax, Ymax, Zmax);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmin, Ymax, Zmin);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmax, Ymin, Zmax);
      TestVect(Vector);

      Vector := MatrXVert(Matrix, Xmin, Ymin, Zmax);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmax, Ymax, Zmin);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmax, Ymin, Zmin);
      TestVect(Vector);
      Vector := MatrXVert(Matrix, Xmin, Ymax, Zmax);
      TestVect(Vector);
    end;
  end;

  for i := 0 to Length(Childs) - 1 do
    if Childs[i] <> nil then
      Childs[i].GetBox(Box);
  glPopMatrix();
end;

function TMesh.GetMeshOfName(ObjName:String):TMesh;
var i:integer;
begin
Result:=nil;
If Name=ObjName then begin
 Result :=Self; exit;
 end;
  for i := 0 to Length(Childs) - 1 do
    if Childs[i] <> nil then
      begin
      Result:=Childs[i].GetMeshOfName(ObjName);
      if Result<>nil then exit;
      end;
end;

function TMesh.GetMeshOfID(Id:Integer):TMesh;
var i:integer;
begin
Result:=nil;
If Indx=Id then begin
 Result :=Self; exit;
 end;
  for i := 0 to Length(Childs) - 1 do
    if Childs[i] <> nil then
      begin
      Result:=Childs[i].GetMeshOfID(Id);
      if Result<>nil then exit;
      end;
end;


function GetMatrix(Pos, Rot, Size: Tver): TMatrix;
begin
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(Pos[0], Pos[1], Pos[2]);
  glRotatef(RadToDeg(Rot[2]), 0, 0, 1);//Z
  glRotatef(RadToDeg(Rot[1]), 0, 1, 0);//y
  glRotatef(RadToDeg(Rot[0]), 1, 0, 0);//X
  glScalef(Size[0], Size[1], Size[2]);
  glGetFloatv(GL_MODELVIEW_MATRIX, @Result);
end;

function GetMatrix2(Pos, Rot1, Rot2, Rot3, Size: Tver): TMatrix;
begin
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(Pos[0], Pos[1], Pos[2]);

  glRotatef(RadToDeg(Rot1[2]), 0, 0, 1); //Z
  glRotatef(RadToDeg(Rot1[1]), 0, 1, 0); //y
  glRotatef(RadToDeg(Rot1[0]), 1, 0, 0); //X

  glRotatef(RadToDeg(Rot2[2]), 0, 0, 1);//Z
  glRotatef(RadToDeg(Rot2[1]), 0, 1, 0);//y
  glRotatef(RadToDeg(Rot2[0]), 1, 0, 0);//X
  
  glRotatef(RadToDeg(Rot3[2]), 0, 0, 1);//Z
  glRotatef(RadToDeg(Rot3[1]), 0, 1, 0);//y
  glRotatef(RadToDeg(Rot3[0]), 1, 0, 0);//X
  glScalef(Size[0], Size[1], Size[2]);  //??????
  glGetFloatv(GL_MODELVIEW_MATRIX, @Result);

end;

procedure MatrixDecompose(XMatrix: TMatrix; var XPos: Tver;
  var XRot: Tver; var XSize: Tver);
  //var Matrix:Tmatrix;
var
  angle_x, angle_y, angle_z, D, C, trx, tr_y: Real;
  mat: array [0..15] of Real;
begin
  XPos[0] := XMatrix[4][1];
  XPos[1] := XMatrix[4][2];
  XPos[2] := XMatrix[4][3];
  XSize[0] := Sqrt(Sqr(XMatrix[1][1]) + Sqr(XMatrix[1][2]) + Sqr(XMatrix[1][3]));
  XSize[1] := Sqrt(Sqr(XMatrix[2][1]) + Sqr(XMatrix[2][2]) + Sqr(XMatrix[2][3]));
  XSize[2] := Sqrt(Sqr(XMatrix[3][1]) + Sqr(XMatrix[3][2]) + Sqr(XMatrix[3][3]));

  mat[0] := XMatrix[1][1] / XSize[0];
  mat[1] := XMatrix[1][2] / XSize[0];
  mat[2] := XMatrix[1][3] / XSize[0];

  mat[4] := XMatrix[2][1] / XSize[1];
  mat[5] := XMatrix[2][2] / XSize[1];

  mat[6] := XMatrix[2][3] / XSize[1];
  mat[10] := XMatrix[3][3] / XSize[2];
  D := -arcsin(mat[2]);
  angle_y := D;   //     /* Calculate Y-axis angle */
  C := Cos(angle_y);
  angle_y := angle_y * RADIANS;

  if (Abs(C) > 0.005) then         //   /* Gimball lock? */
  begin
    trx  := mat[10] / C;           //* No, so get X-axis angle */
    tr_y := -mat[6] / C;

    angle_x := ArcTan2(tr_y, trx) * RADIANS;

    trx  := mat[0] / C;            //* Get Z-axis angle */
    tr_y := -mat[1] / C;

    angle_z := ArcTan2(tr_y, trx) * RADIANS;
  end
  else                                 //* Gimball lock has occurred */
  begin
    angle_x := 0;                      //* Set X-axis angle to zero */

    trx  := mat[5];                 //* And calculate Z-axis angle */
    tr_y := mat[4];

    angle_z := ArcTan2(tr_y, trx) * RADIANS;
  end;
  //--------------
  XRot[0] := -angle_x;
  XRot[1] := angle_y;
  XRot[2] := -angle_z;
end;


Procedure glLoadMulti;
begin
glPushAttrib(GL_TEXTURE_BIT or GL_EVAL_BIT);
{$IFDEF NOMULTI}
   glActiveTextureARB(GL_TEXTURE1    );
   glEnable          (GL_TEXTURE_2D      );

   glTexEnvi         (GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE ,GL_COMBINE    );
   glTexEnvi         (GL_TEXTURE_ENV,GL_COMBINE_RGB  ,GL_REPLACE        );

   glActiveTextureARB(GL_TEXTURE0    );
   glEnable          (GL_TEXTURE_2D      );

   glTexEnvi         (GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE ,GL_COMBINE    );
   glTexEnvi         (GL_TEXTURE_ENV,GL_COMBINE_RGB  ,GL_INTERPOLATE);

    glTexEnvi ( GL_TEXTURE_ENV, GL_SOURCE0_RGB,  GL_TEXTURE1      );
    glTexEnvi ( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR    );

    glTexEnvi ( GL_TEXTURE_ENV, GL_SOURCE1_RGB,  GL_TEXTURE);
    glTexEnvi ( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );

    glTexEnvi ( GL_TEXTURE_ENV, GL_SOURCE2_RGB,  GL_PRIMARY_COLOR);
    glTexEnvi ( GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA    );

   glActiveTextureARB(GL_TEXTURE1   );
   glEnable          (GL_TEXTURE_2D      );

   glTexEnvi         (GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE ,GL_COMBINE    );
   glTexEnvi         (GL_TEXTURE_ENV,GL_COMBINE_RGB  ,GL_MODULATE        );

    glTexEnvi ( GL_TEXTURE_ENV, GL_SOURCE0_RGB,  GL_PREVIOUS      );
    glTexEnvi ( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR    );

    glTexEnvi ( GL_TEXTURE_ENV, GL_SOURCE1_RGB,   GL_PRIMARY_COLOR);
    glTexEnvi ( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );
{$ENDIF}
end;

Procedure glUnLoadMulti;
begin
glPopAttrib;
end;

procedure TMesh.Draw(DrawType: TDrawType);
label
  ToChilds;
var
  i, j, ActChild, NormalLen, FaceNum, VCount, BCount: Integer;
  Matrix: TMatrix;
  point31, point32, BVert: Tver;
  AnimInfo: TAnimInfo;
    PushMatrix: TMatrix;
  rotaxis:TAxis;
label
  normal2;
begin     // ������ � ���� ������� �������
   glGetFloatv(GL_MODELVIEW_MATRIX,@PushMatrix);
//  glPushMatrix();
  AnimInfo.True:=False;
  AnimInfo.Child:=0;
  AnimInfo.TCoord[0]:=0;
  AnimInfo.TCoord[1]:=0;
  AnimInfo.Tm:=Transform.TrMatrix;
  {

  if (DrawType = DrGenAnim) then
   begin
       CurAnimClip.GenAnimFrame(AnimInfo,Name, Transform, XType = 'BG');
       goto ToChilds;
   end;                   }

  if AnimReady and (But.AnimButton^ or But.EditAnimBut^) then
        AnimInfo:=CurAnimClip.GenAnimFrame(Name, Transform, XType = 'BG',SelectObj = Indx);

  if (DrawType = DrBone) then
  begin
    if (XType = 'BO') then
      glCallList(objAxes);
    if (XType = 'BG') then
    begin
      glGetFloatv(GL_MODELVIEW_MATRIX, @Matrix);
      point31 := MatrXVert(Matrix, 0, 0, 0);
      //  glDepthMask(GL_FALSE);
    end;
  end;

  if Transform.TransType <> TTNone then
        glMultMatrixf(@AnimInfo.Tm);   // ��� ��� ���?

  if (XType = 'BO') then
  begin
    glGetFloatv(GL_MODELVIEW_MATRIX, @BoneMatrix);
  end;

  if (DrawType = DrBone) then
  begin
    if (XType = 'BG') then
    begin
      //  glColor3f(0.1, 0.8, 0.1);
      glGetFloatv(GL_MODELVIEW_MATRIX, @Matrix);
      point32 := MatrXVert(Matrix, 0, 0, 0);
      glDisable(GL_LIGHTING);
      glColor3f(1, 1, 1);
      glPointSize(6);
      glBegin(GL_POINTS);
      glVertex3f(0, 0, 0);
      glEnd;
      glPushMatrix();
      glLoadIdentity();
      glBegin(GL_Lines);
      glVertex3f(point31[0], point31[1], point31[2]);
      glVertex3f(point32[0], point32[1], point32[2]);
      glEnd;
      glPopMatrix();
    end;
  end;

  //if (XType = 'BO') then goto
  if (SelectObj = Indx) and (Indx<>0) then
   begin
     ActiveMesh:= Self;
 end;

  if (DrawType = DrBoxes) and (Transform.TransType <> TTNone) then
  begin
      glDisable(GL_LIGHTING);
      glColor3f(0.0, 1.0, 0.0);
      if ActiveMesh=Self  then
      glColor3f(1.0, 0.0, 0.0);
      glPointSize(8);
      glBegin(GL_POINTS);
      glVertex3f(0, 0, 0);
      glEnd;
      if But.Dummy^ then begin
      glColor3f(1.0, 1.0, 0.0);
      glRasterPos3f(0, 0, 0);
      glListBase(FontGL);
      glCallLists(Length(Space), GL_UNSIGNED_BYTE, Pointer(Space));
      glCallLists(Length(Name), GL_UNSIGNED_BYTE, Pointer(Name));
      end;

  end;

  if (DrawType = DrSelect) and (Transform.TransType <> TTNone)  then
  begin
      glDisable(GL_LIGHTING);
      glColor4_256(Indx);
      glxDrawBitmapText(But.Canvas, 0, 0, 0, BbitPoint2, '', true);
  end;

  if ((DrawType = DrBlend) and not Attribute.ZBuffer) or
    ((DrawType = DrMesh) and (Attribute.ZBuffer))then 
  begin
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    if But.DrawBoxes^ and ((XType = 'SH') or (XType = 'SS')) then
      DrawSelectBox(SizeBox);
    glEnable(GL_LIGHTING);

    glDepthFunc(GL_NEVER + Attribute.ZBufferFuncVal);

    if Attribute.ZBuffer then
      glDepthMask(GL_TRUE)
    else
      glDepthMask(GL_FALSE);

    if Attribute.CullFace then
      glEnable(GL_CULL_FACE)
    else
      glDisable(GL_CULL_FACE);

 //   if not Attribute.Multi then
     glEnable(GL_BLEND);

    if Attribute.AlphaTest then
    begin
      glEnable(GL_ALPHA_TEST);
      glDisable(GL_BLEND);
      glAlphaFunc(GL_GEQUAL, Attribute.AlphaTestVal);
    end
    else  
      glDisable(GL_ALPHA_TEST);

    if Attribute.Blend then
    begin
      //     glEnable(GL_BLEND);
      glBlendFunc(BlendVal[Attribute.BlendVal1], BlendVal[Attribute.BlendVal2]);
    end
    else // glDisable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glColor3f(1.0, 1.0, 1.0);

    glPushAttrib(GL_COLOR_MATERIAL);
    if Attribute.Material.use then
      with Attribute.Material do
      begin
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, @Abi);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, @Dif);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, @Spe);
     //   glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, @Emi);
        //   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,Material.Shi);
      end;

    // Dummy poxel
    if But.Dummy^ and (DrawType = DrMesh)
    and (XType = 'GR') and (not But.EditAnimBut^) then  // ������ ������ � ������
      glxDrawBitmapText(But.Canvas, 0, 0, 0, BbitPoint2, Name, false);

    if Length(Face) <> 0 then
    begin
      NormalLen := Length(Normal);
      FaceNum := Length(Face);

      if NormalLen = 0 then
        glDisable(GL_LIGHTING);

    if Attribute.Texture <> 0 then
    begin
      glEnable(GL_TEXTURE_2D);
      if Attribute.Multi then begin
        glLoadMulti;
        glActiveTextureARB(GL_TEXTURE0);
        glBindTexture     (GL_TEXTURE_2D, Attribute.MultiT1);
        glActiveTextureARB(GL_TEXTURE1);
        glBindTexture     (GL_TEXTURE_2D, Attribute.MultiT2);

        glDisable( GL_BLEND );

        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glClientActiveTextureARB (GL_TEXTURE1);
        if TextCoord2<>nil then
        glTexCoordPointer(2, GL_FLOAT, 0, TextCoord2)
        else
        glTexCoordPointer(2, GL_FLOAT, 0, TextCoord);

        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glClientActiveTextureARB (GL_TEXTURE0); 
        glTexCoordPointer(2, GL_FLOAT, 0, TextCoord);
      end
      else  begin
        glBindTexture(GL_TEXTURE_2D, Attribute.Texture);

        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2, GL_FLOAT, 0, TextCoord);
      end;

       glPushMatrix();
       glMatrixMode(GL_TEXTURE);
 //      if Attribute.Texture2D then begin
     {   glLoadMatrixf(@Attribute.T2DMatrix); }
 //end;
      if Attribute.Texture<>LastTexture then
        glLoadIdentity();
        LastTexture:=Attribute.Texture;
     
        glTranslatef(AnimInfo.TCoord[0],AnimInfo.TCoord[1],0);
       glMatrixMode(GL_MODELVIEW);
       glPopMatrix();

    end;

      if (Length(Color) <> 0) and
        (Cardinal(Pointer(@Color[0])^) <> $FF000000) then
      begin
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4, GL_UNSIGNED_BYTE, 0, Color);
      end;

      if NormalLen <> 0 then
      begin
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, Normal);
      end;

      glEnableClientState(GL_VERTEX_ARRAY);

      if (XType = 'SS') and (Weight <> nil) and (Bones[0] <> nil) then 
      begin
        VCount := Length(Vert);
        for i := 0 to VCount - 1 do
        begin
          RVert[i] := ZereVector;
          BCount := Length(Bones);
          for j := 0 to BCount - 1 do
          begin
            BVert := MatrXVert(MatrXMatr(Bones[j].InvBoneMatrix,
              Bones[j].BoneMatrix), Vert[i][0], Vert[i][1], Vert[i][2]);
            //  BVert:= MatrXVert(Bones[j].BoneMatrix, Vert[i][0],Vert[i][1],Vert[i][2]);
            RVert[i] := VertAddVert(RVert[i],
              ValXVert(Weight[i][j], BVert));
          end;
        end;
                
        glVertexPointer(3, GL_FLOAT, 0, RVert);
      end       

      else
        glVertexPointer(3, GL_FLOAT, 0, Vert);

      for j := 0 to FaceNum - 1 do
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, @Face[j]);

      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      glDisableClientState(GL_COLOR_ARRAY);
      glDisableClientState(GL_NORMAL_ARRAY);
      glDisableClientState(GL_VERTEX_ARRAY);

      //   if NormalLen=0 then
      glEnable(GL_LIGHTING);
      glDisable(GL_BLEND);
      if Attribute.Multi then glUnLoadMulti;
      glDisable(GL_TEXTURE_2D);
      
      glDepthFunc(GL_LESS);
    end;
    glPopAttrib();
  end;
  ToChilds:
  if (XType = 'CS') then 
  begin
      ActChild := AnimInfo.Child;
    if Childs[ActChild] <> nil then
      Childs[ActChild].Draw(DrawType)
  end
  else
  begin
    for i := 0 to Length(Childs) - 1 do
    begin
      if Childs[i] <> nil then 
        Childs[i].Draw(DrawType);
    end;
  end;
  glLoadMatrixf(@PushMatrix);  // ���������� ������� �������
//  glPopMatrix();
end;

destructor TMesh.Destroy;
var
  i: Integer;
begin
  Vert := nil;
  RVert := nil;
  for i := 0 to Length(Weight) - 1 do
    Weight[i] := nil;
  Weight := nil;
  Face := nil;
  Normal := nil;
  TextCoord := nil;
  TextCoord2 := nil;
  for i := 0 to Length(Childs) - 1 do begin
    Childs[i].Free;
    Childs[i]:=nil;
    end;
  Childs := nil;
  Bones := nil;
  inherited Destroy;
end;

function Byte128(_byte: Integer): Integer;
begin
  asm
mov eax,_byte
mov ecx,128
cdq
div ecx
cmp eax,0
je @@no
add edx,128
@@no:
mov ecx,eax
imul ecx,256
add ecx,edx
mov result,ecx
end;
end;

function TestByte128(var _p: Pointer): Integer;
var
  Val, n: Integer;
begin
  n := 1;
  Val := Byte(_p^) and $7f;
  while ((Byte(_p^) shr 7) = 1) and (n < 4) do
  begin
    Inc(Longword(_p));
    Val := ((Byte(_p^) and $7F) shl (7 * n)) + Val;
    Inc(n);
  end;
  Inc(Longword(_p));
  Result := Val;
end;

procedure WriteXByte(var Memory: TMemoryStream; Val: Integer);
var 
  byte0: Byte;
  HByte: Boolean;
begin
  Hbyte := true;
  while Hbyte do
  begin
    byte0 := Byte(Val);
    //     Hbyte:=byte((val shr 7)and 1);
    Hbyte := (Val shr 7) > 0;
    if Hbyte then  
      byte0 := byte0 or $80;
    Memory.Write(byte0, 1);
    Val := Val shr 7;
  end;
end;

procedure TXom.WriteXName(var Memory:TMemoryStream; Name:String);
var
    Index,Len,k,i:integer;
    s:String;
begin        // GlValueList.Values[]
        Index:=-1;
    for i:=0 to But.GlValueList.RowCount-1 do
      if But.GlValueList.Cells[1, i]=Name then  begin Index:=i; break; end;

    if Index=-1 then begin
        Index:=But.GlValueList.RowCount;
        s := Format('%.2x', [byte128(Index + 1)]);
        Index:=But.GlValueList.InsertRow(s, Name, true);
    end;
 //   k:=Byte128(Index);
    WriteXByte(Memory,Index);
end;

procedure WriteXByteP(var p: Pointer; Val: Integer);
var 
  byte0: Byte;
  HByte: Boolean;
begin
  Hbyte := true;
  while Hbyte do
  begin
    byte0 := Byte(Val);
    Hbyte := (Val shr 7) > 0;
    if Hbyte then  
      byte0 := byte0 or $80;
    Byte(p^) := byte0; 
    Inc(Longword(p));
    Val := Val shr 7;
  end;
end;

procedure TMesh.DrawSelectBox(SelectBox: TBox);
var
  Xsize, Ysize, Zsize: Single;
  Border, BordOffset: Single;
  TempSelectBox: TBox;
  procedure DrawTetra(X, Y, Z, Xs, Ys, Zs: Single);
  begin
    glVertex3f(X, Y, Z);
    glVertex3f(X + Xs, Y, Z);
    glVertex3f(X, Y, Z);
    glVertex3f(X, Y + Ys, Z);
    glVertex3f(X, Y, Z);
    glVertex3f(X, Y, Z + Zs);
  end;
begin
  Border := 0.3;
  BordOffset := 0.1;

  Xsize := (SelectBox.Xmax - SelectBox.Xmin) * Border;
  Ysize := (SelectBox.Ymax - SelectBox.Ymin) * Border;
  Zsize := (SelectBox.Zmax - SelectBox.Zmin) * Border;
  //  end;
  TempSelectBox.Xmin := SelectBox.Xmin - Xsize * BordOffset;
  TempSelectBox.Ymin := SelectBox.Ymin - Ysize * BordOffset;
  TempSelectBox.Zmin := SelectBox.Zmin - Zsize * BordOffset;
  TempSelectBox.Xmax := SelectBox.Xmax + Xsize * BordOffset;
  TempSelectBox.Ymax := SelectBox.Ymax + Ysize * BordOffset;
  TempSelectBox.Zmax := SelectBox.Zmax + Zsize * BordOffset;
  //  glPushMatrix;
  //  glScalef(Size.X, Size.Y, Size.Z);
  glPushAttrib(GL_COLOR_MATERIAL);
  glDisable(GL_LIGHTING);
  with TempSelectBox do
  begin
    glColor3f(1, 1, 1);
    glBegin(GL_LINES);
    DrawTetra(Xmin, Ymin, Zmin, Xsize, Ysize, Zsize);
    DrawTetra(Xmax, Ymax, Zmax, - Xsize, - Ysize, - Zsize);

    DrawTetra(Xmin, Ymax, Zmin, Xsize, - Ysize, Zsize);
    DrawTetra(Xmax, Ymin, Zmax, - Xsize, Ysize, - Zsize);

    DrawTetra(Xmin, Ymin, Zmax, Xsize, Ysize, - Zsize);
    DrawTetra(Xmax, Ymax, Zmin, - Xsize, - Ysize, Zsize);

    DrawTetra(Xmax, Ymin, Zmin, - Xsize, Ysize, Zsize);
    DrawTetra(Xmin, Ymax, Zmax, Xsize, - Ysize, - Zsize);
    glEnd;
  end;
  glEnable(GL_LIGHTING);
  glPopAttrib;
  //  glPopMatrix;
end;

  //var
  //Material:TMaterial;
function TXom.GetString(indx: Integer): string;
begin
  with But.GlValueList do
    if (indx < RowCount) and (indx <> 0) then
      Result := Cells[1, indx]
  else
    Result := '';
end;



function TMesh.GetBone(CntrIndx: Integer; Mesh: TMesh): TMesh;
var 
  i, Len: Integer;
  //TempMesh:Tmesh;
begin
  if Mesh = nil then 
  begin 
    Result := nil;
    Exit;
  end;
  if Mesh.Indx = CntrIndx then
    Result := Mesh
  else  
    Result := nil;

  Len := Length(Mesh.Childs) - 1;
  for i := 0 to Len do
  begin
    Result := GetBone(CntrIndx, Mesh.Childs[i]);
    if Result <> nil then 
      Break;
  end;
end;

procedure TMesh.glLoad2DMipmaps(target: GLenum;
  Components, Width, Height: GLint; Format, atype: GLenum; Data: Pointer);
var
  i: Integer;
begin
  for i := 0 to 10 do 
  begin
    if (Width=0) then Width := 1;
    if (Height=0) then Height := 1;
    glTexImage2D(target, i, Components, Width, Height, 0, Format, atype, Data);
    Inc(Longword(Data), (Width * Height * Components));
    if ((Width = 1) and (Height = 1)) then
      Break;
    Width  := Width div 2;
    Height := Height div 2;
  end;
end;

function TMesh.GetTextureGL(Point: Pointer): Boolean;
var
  VBuf, VBuf2, palette: Pointer;
  w, h, CntrIndx: Integer;
  NumMaps, IName, ISize, IFormat, IComponents: Integer;
  Bsize: Byte;
  Is24Bit, IsXBOX: Boolean;
begin
  //////////////////////////////
  VBuf := Pointer(Longword(Point) + 3);
  IName := TestByte128(VBuf);
  w := Word(VBuf^);
  Inc(Longword(VBuf), 2);
  h := Word(VBuf^);
  Inc(Longword(VBuf), 2);
  NumMaps := Byte(VBuf^); 
  Inc(Longword(VBuf), 2);
  //  Is24Bit:=(byte(VBuf^)=0);  ????
  Inc(Longword(VBuf), 2);
  Bsize := Byte(VBuf^);  
  Inc(Longword(VBuf), 4 * Bsize + 1);
  Bsize := Byte(VBuf^);  
  Inc(Longword(VBuf), 4 * Bsize + 1);
  if Byte(VBuf^) = 7 then 
    IsXBOX := true;
  Is24Bit := (Byte(VBuf^) = 0); 
  Inc(Longword(VBuf), 4);
  ISize := TestByte128(VBuf);

  VBuf2 := VBuf;
  Inc(Longword(VBuf2), ISize);
  CntrIndx := TestByte128(VBuf2);

  IFormat := GL_RGBA;
  IComponents := 4;
  Result := false;
  if Is24Bit then
  begin
    IFormat     := GL_RGB;
    IComponents := 3;
    Result      := true;
  end;
 { if IsXbox then
  begin
    glEnable(GL_COLOR_TABLE);
    palette:= pointer(Longword(StrArray[CntrIndx].point)+11);
//    glColorTable(GL_COLOR_TABLE, GL_RGBA, 256, GL_RGBA, GL_UNSIGNED_BYTE, palette);
    IFormat:=GL_COLOR_INDEX;
    IComponents:=1;
  end;     }

  if NumMaps > 1 then
    glLoad2DMipmaps(GL_TEXTURE_2D, IComponents, w, h, IFormat,
      GL_UNSIGNED_BYTE, VBuf)
  else
    gluBuild2DMipmaps(GL_TEXTURE_2D, IComponents, w, h, IFormat,
      GL_UNSIGNED_BYTE, VBuf)
    //////////////////////////////
end;

procedure TMesh.glBuildTriangleGeo(CntrIndx: Integer);
var
  Point: Pointer;
  Strip: Boolean;
  p2, p3, MaxSingle, FaceArrStrip, FaceArr: Pointer;
  //FaceArr:AFace3;
  PointsArr2: AVer;
  j, k,k2, FaceNum, Count3dsPoints, Faces, Len: Integer;
  IDtest: Boolean;
  //SizeBox:TBox;
  //Colors:AUColor;
  //TempNote:TTreeNode;
  FaceCntr, VertexCntr, NormalCntr, ColorCntr, TextureCntr, WeightCntr: Integer;
begin
  Point := StrArray[CntrIndx].Point;
  Strip := StrArray[CntrIndx].XType = XIndexedTriangleStripSet;
{
01  00  00  2E  00  00  00  00
48  00  00  00  1A  1F  24  29
00  FF  FF  7F  7F  FF  FF  7F
7F  FF  FF  7F  7F  FF  FF  7F
FF  FF  FF  7F  FF  FF  FF  7F
FF  01  00  00  00   }
{ Strip
01  00  00  01  D5  01  05  00
00  00  00  01  00  00  00  02
03  00  04  00  88  55  8E  C1
F0  CE  48  C1  A6  BB  92  C1
E5  54  8E  41  C0  32  B4  41
88  18  8F  41  00  00  00  00
}
  if Strip then 
  begin
    p2    := Pointer(Longword(Point) + 6);
    Faces := Word(Pointer(Longword(Point) + 4)^);
    // get Faces
    FaceCntr := TestByte128(p2);   //5
    Inc(Longword(p2), 8);
    // get Vertex
    VertexCntr := TestByte128(p2);  //2
    // get Normal
    NormalCntr := TestByte128(p2);   //3
    // get Colors
    ColorCntr := TestByte128(p2);
    // get TextureCoord
    TextureCntr := TestByte128(p2);  //4
    WeightCntr  := TestByte128(p2);
  end 
  else 
  begin
    p2 := Pointer(Longword(Point) + 3);
    // get Faces
    FaceCntr := TestByte128(p2);   //5
    Inc(Longword(p2), 4);
    Faces := Word(p2^);
    Inc(Longword(p2), 4);
    // get Vertex
    VertexCntr := TestByte128(p2);  //2
    // get Normal
    NormalCntr := TestByte128(p2);   //3
    // get Colors
    ColorCntr := TestByte128(p2);
    // get TextureCoord
    TextureCntr := TestByte128(p2);  //4
    WeightCntr  := TestByte128(p2);
  end;
  Move(p2^, SizeBox, 2 * 12);
  // GetArrays
  // Vert
  if StrArray[VertexCntr].Xtype <> XCoord3fSet then 
    Exit;
  p2 := StrArray[VertexCntr].point;
  Inc(Longword(p2), 3);
  Count3dsPoints := TestByte128(p2);
  SetLength(Vert, Count3dsPoints);
  Move(p2^, Vert[0], Count3dsPoints * 12);
  // Weight
  if (WeightCntr <> 0) then 
  begin
    if (StrArray[WeightCntr].Xtype = XWeightSet) then
    begin
      p2 := StrArray[WeightCntr].point;
      Inc(Longword(p2), 3);
      k := Word(p2^);
      Inc(Longword(p2), 2);
      TestByte128(p2);
      SetLength(Weight, Count3dsPoints);
      for j := 0 to Count3dsPoints - 1 do
      begin
        SetLength(Weight[j], k);
        Move(p2^, Weight[j][0], k * 4);
        Inc(Longword(p2), k * 4);
      end;
    end;
    if (StrArray[WeightCntr].Xtype = XPaletteWeightSet) then
    begin
      p2 := StrArray[WeightCntr].point;
      Inc(Longword(p2), 3);
      k := TestByte128(p2);
      p3 := p2;
      Inc(Longword(p3), k);
      k := Word(p3^);
      Inc(Longword(p3), 2);
      k := TestByte128(p3);
      k := Length(Bones);
      if k = 0 then
        k := 100;
      SetLength(Weight, Count3dsPoints);
      for j := 0 to Count3dsPoints - 1 do
      begin
        SetLength(Weight[j], k);
        Weight[j][Byte(p2^)] := Single(p3^);
        Inc(Longword(p2));
        Inc(Longword(p3), 4);

        Weight[j][Byte(p2^)] := Single(p3^);
        Inc(Longword(p2));
        Inc(Longword(p3), 4);
     if WUM and (k>2) then begin
        Weight[j][Byte(p2^)] := Single(p3^);
        Inc(Longword(p2));
        Inc(Longword(p3), 4);
        end;
      end;

    end;
  end;
  // Normal
  if NormalCntr <> 0 then 
  begin
    if StrArray[NormalCntr].Xtype <> XNormal3fSet then 
      Exit;
    p2 := StrArray[NormalCntr].point;
    Inc(Longword(p2), 3);
    k := TestByte128(p2);
    SetLength(Normal, k);
    Move(p2^, Normal[0], k * 12);
  end;
  // Color
  if (ColorCntr <> 0) and (StrArray[ColorCntr].Xtype <> XConstColorSet) then 
  begin
    if StrArray[ColorCntr].Xtype <> XColor4ubSet then 
      Exit;
    p2 := StrArray[ColorCntr].point;
    Inc(Longword(p2), 3);
    k := TestByte128(p2);
    SetLength(Color, k);
    Move(p2^, Color[0], k * 4);
  end;
  // TextCoord
  if StrArray[TextureCntr].Xtype = XTexCoord2fSet then
  begin
  p2 := StrArray[TextureCntr].point;
  Inc(Longword(p2), 3);
  k := TestByte128(p2);
  SetLength(TextCoord, k);
  Move(p2^, TextCoord[0], k * 8);
  end else if  StrArray[TextureCntr].Xtype = XMultiTexCoordSet then
  begin
  p2 := StrArray[TextureCntr].point;
  Inc(Longword(p2), 3);
  TestByte128(p2);
  p3:=StrArray[TestByte128(p2)].point;
  p2:=StrArray[TestByte128(p2)].point;

  Inc(Longword(p2), 3);
  k := TestByte128(p2);
  SetLength(TextCoord2, k);
  Move(p2^, TextCoord2[0], k * 8);

  Inc(Longword(p3), 3);
  k := TestByte128(p3);
  SetLength(TextCoord, k);
  Move(p3^, TextCoord[0], k * 8);

  end else Exit;
  // Face
  if StrArray[FaceCntr].Xtype <> XIndexSet then 
    Exit;
  p2 := StrArray[FaceCntr].point;
  Inc(Longword(p2), 3);
  if Strip then 
  begin
    FaceNum      := TestByte128(p2) - 2;
    FaceArrStrip := p2;
    SetLength(Face, FaceNum);
  end 
  else 
  begin
    FaceNum := TestByte128(p2) div 3;
    FaceArr := p2;
    SetLength(Face, FaceNum);
    // SetLength(FaceArr,FaceNum);
    // Move(p2^, FaceArr[0], FaceNum*6);
  end;
  // Gen

  MaxSingle := Pointer($FFFF7F7F);
  if SizeBox.Xmin = Single(MaxSingle) then
    for j := 0 to Count3dsPoints - 1 do
      with SizeBox do 
      begin
        if Vert[j][0] > Xmax then 
          Xmax := Vert[j][0];
        if Vert[j][0] < Xmin then 
          Xmin := Vert[j][0];
        if Vert[j][1] > Ymax then 
          Ymax := Vert[j][1];
        if Vert[j][1] < Ymin then 
          Ymin := Vert[j][1];
        if Vert[j][2] > Zmax then 
          Zmax := Vert[j][2];
        if Vert[j][2] < Zmin then 
          Zmin := Vert[j][2];
      end;

  if Strip then
    for j := 0 to FaceNum - 1 do
    begin
      Move(FaceArrStrip^, Face[j], 6);
      if (j mod 2) = 1 then 
      begin
        Face[j][3] := Face[j][0];
        Face[j][0] := Face[j][2];
        //         Face[j][1]:=Face[j][2];
        Face[j][2] := Face[j][3];
      end;
      Inc(Longword(FaceArrStrip), 2);
    end
  else
    for j := 0 to FaceNum - 1 do
    begin
      Move(FaceArr^, Face[j], 6);
      Inc(Longword(FaceArr), 6);
    end;

{   Mesh.Texture:=NTexture;
   If Material.use then begin
   Mesh.Material:=Material;
   Material.use:=false;
   end;
//   Mesh.TrMatrix:=CurMatrix;   }


  // SetLength(Colors,0);

 // But.RotateButton.Down := true;
  // FormXom.SpeedButton1Click(nil);
end;

procedure TMesh.glBuildCollGeo(CntrIndx: Integer);
var
  Point: Pointer;
  p2, px, PointsArr, FaceArr: Pointer;
  FaceArr2: AFace;
  PointsArr2: AVer;
  Count3dsFace, Count3dsPoints, j, FaceNum: Integer;
  SizeBox: TBox;
  MaxZoom: Single;
  b,b1:byte;
begin
  Point := StrArray[CntrIndx].Point;
  p2 := Pointer(Longword(Point) + 7 + 8);
  FaceNum := TestByte128(p2);
  if FaceNum = 0 then 
    Exit;
  if Byte(p2^) = 255 then 
    Exit;
  // if byte(p2^)=1 then p2:=pointer(integer(p2)+1);
    b:=Byte(Pointer(Longword(p2) + 24)^);
    b1:=Byte(Pointer(Longword(p2) + 8)^);
  if b=b1  then

  begin
    FaceArr      := Pointer(Longword(p2) + 2);
    Count3dsFace := FaceNum div 4;
    SetLength(FaceArr2, Count3dsFace);
    Move(FaceArr^, FaceArr2[0], Count3dsFace * 8);
    FaceArr2[Count3dsFace - 1][3] := 0;
  end  
  else 
  begin
    FaceArr      := p2;
    Count3dsFace := FaceNum div 3;
    SetLength(FaceArr2, Count3dsFace);
    for j := 0 to Count3dsFace - 1 do 
    begin
      px := Pointer(Integer(FaceArr) + j * 6);
      Move(px^, FaceArr2[j], 6);
    end;
    // FaceArr2[Count3dsFace-1][3]:=0;
  end;
  p2 := Pointer(Integer(p2) + FaceNum * 2);
  Count3dsPoints := (TestByte128(p2) div 4);
  PointsArr := p2;
  SetLength(PointsArr2, Count3dsPoints + 1);
  // Move(PointsArr^, PointsArr2, Count3dsPoints);

  with   SizeBox do 
  begin
    Xmax := -MaxSingle;
    Xmin := MaxSingle;
    Ymax := -MaxSingle;
    Ymin := MaxSingle;
    Zmax := -MaxSingle;
    Zmin := MaxSingle;
  end;

  for j := 0 to Count3dsPoints - 1 do 
  begin
    px := Pointer(Longword(PointsArr) + j * 16);
    Move(px^, PointsArr2[j], 12);
    with SizeBox do 
    begin
      if PointsArr2[j][0] > Xmax then 
        Xmax := PointsArr2[j][0];
      if PointsArr2[j][0] < Xmin then 
        Xmin := PointsArr2[j][0];
      if PointsArr2[j][1] > Ymax then 
        Ymax := PointsArr2[j][1];
      if PointsArr2[j][1] < Ymin then 
        Ymin := PointsArr2[j][1];
      if PointsArr2[j][2] > Zmax then 
        Zmax := PointsArr2[j][2];
      if PointsArr2[j][2] < Zmin then 
        Zmin := PointsArr2[j][2];
    end;
    //         PointsArr2[j][0]:=Single(Pointer(integer(PointsArr)+j*16)^);
    //         PointsArr2[j][1]:=Single(Pointer(integer(PointsArr)+j*16+4)^);
    //         PointsArr2[j][2]:=Single(Pointer(integer(PointsArr)+j*16+8)^);
  end;
  with SizeBox do
  begin
    TransView.Xpos := -(XMax + Xmin) / 2;
    TransView.Ypos := -(YMax + Ymin) / 2;
    TransView.Zpos := -(ZMax + Zmin) / 2;
    MaxZoom        := Max((Xmax - Xmin), (Ymax - Ymin));
    MaxZoom        := Max(MaxZoom, (Zmax - Zmin));
    TransView.zoom := MaxZoom * 3;
  end;

//  glEnableClientState(GL_VERTEX_ARRAY);
 //  glInterleavedArrays(GL_V3F, 0, PointsArr2);
//  glVertexPointer(3, GL_FLOAT, 0, PointsArr2);
  glNewList(objCollGeo, GL_COMPILE);
  for j := 0 to Count3dsFace - 1 do
   //glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, @FaceArr2[j]);
    begin
    glBegin(GL_TRIANGLES);
    glVertex3fv(@PointsArr2[FaceArr2[j][0]]);
    glVertex3fv(@PointsArr2[FaceArr2[j][1]]);
    glVertex3fv(@PointsArr2[FaceArr2[j][2]]);
    glEnd;
    end;
  glEndList;
 // glDisableClientState(GL_VERTEX_ARRAY);
  SetLength(FaceArr2, 0);
  SetLength(PointsArr2, 0);
//  But.RotateButton.Down := true;
  // FormXom.SpeedButton1Click(nil);
end;

procedure TMesh.glClearCollGeo;
begin
  glDeleteLists(objCollGeo, 1);
end;

function ReadName(var Stream: TMemoryStream): string;
var
  p: Pointer;
begin
  p := Pointer(Longword(Stream.Memory) + Stream.Position);
  Result := PChar(p);
  Stream.Position := Stream.Position + Length(Result) + 1;
end;

function BuildTree(XomTree, Tree: TTreeView; InNode, Node: TTreeNode): XTypes;
var
  CntrIndx: Integer;
  i: Integer;
  // TempNode:TTreeNode;
  procedure AddNode(Name: string);
  begin
    Node := Tree.Items.AddChild(Node, Name);
    Node.Data := Pointer(CntrIndx);
  end;
begin
  CntrIndx := Integer(InNode.Data);
  Result := StrArray[CntrIndx].Xtype;
  case StrArray[CntrIndx].Xtype of
XGroup:
    begin
      AddNode('Group');
      for i := 0 to InNode.Count - 1 do 
      begin
        if BuildTree(XomTree, Tree, InNode[i], Node) = XShape then
          Node.Text := 'GroupShape';
      end;
    end;
XBinModifier:
    begin
      AddNode('BinModifier');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XSkin:
    begin
      AddNode('Skin');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XInteriorNode:
    begin
      AddNode('Node');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XTransform, XMatrix:
      AddNode('Matrix');
XOglTextureMap:
      Tree.Items.AddChild(Node, 'Texture').Data := InNode.GetNext.Data;
XMaterial:
      AddNode('Material');
XColor4ubSet:
      //       if ColorUse then
      AddNode('Color');
XIndexSet:
      AddNode('Face');
XIndexedTriangleSet:
    begin
      AddNode('Obj');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XShape:
    begin
      AddNode('Shape');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XBone:
      AddNode('Bone');
XWeightSet:
      AddNode('Weight');
XSkinShape:
    begin
      AddNode('SkinShape');
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XSimpleShader:
    begin
      for i := 0 to InNode.Count - 1 do
        BuildTree(XomTree, Tree, InNode[i], Node);
    end;
XCoord3fSet:
      AddNode('Vert');
XNormal3fSet:
      AddNode('Norm');
XTexCoord2fSet:
      AddNode('TextCoord');
{else
 if InNode.GetNext<>nil then
  BuildTree(XomTree,Tree,InNode.GetNext,Node);  }
  end;
end;


procedure TMesh.ReadXNode(VirtualBufer: TMemoryStream;
  XTextureStr: TStringList; Tree: TTreeView; Node: TTreeNode);
var
  num: Word;
  i: Integer;
  IsPar: Boolean;
  //   p:pointer;
  TempNode: TTreeNode;
  s: string;


  function ReadColor(var Stream: TMemoryStream): Color4d;
  var
    b: Byte;
    i: Integer;
  begin
    Result[3] := 1;
    for i := 0 to 2 do
    begin
      Stream.Read(b, 1);
      Result[i] := b / 255
    end;
  end;
begin
  // P:=VirtualBufer.Memory;

  Name := ReadName(VirtualBufer);
  //    VirtualBufer.Write(PChar(Mesh.Name)^, Length(Mesh.Name) + 1);
  XType := ReadName(VirtualBufer);
  if XType = 'SH' then
    Node := Tree.Items.AddChild(Node, 'Shape');

  with Transform do 
  begin
    VirtualBufer.Read(TransType, 1);

    if TransType = TTMatrix then
      for i := 1 to 4 do
        VirtualBufer.Read(TrMatrix[i], 3 * 4);

    if TransType = TTVector then
    begin
      VirtualBufer.Read(Pos[0], 3 * 4);
      VirtualBufer.Read(Rot[0], 3 * 4);
      VirtualBufer.Read(Size[0], 3 * 4);
      TrMatrix := GetMatrix(Pos, Rot, Size);
    end;
    if TransType = TTJoint then
    begin
      VirtualBufer.Read(Pos[0], 3 * 4);
      VirtualBufer.Read(Rot[0], 3 * 4);
      VirtualBufer.Read(JointOrient[0], 3 * 4);
      VirtualBufer.Read(RotOrient[0], 3 * 4);
      VirtualBufer.Read(Size[0], 3 * 4);
      TrMatrix := GetMatrix2(Pos, Rot, JointOrient, RotOrient, Size);
    end;
  end;

  VirtualBufer.Read(Num, 2);

  if Num > 0 then
  begin     // Mesh
    TempNode      := Tree.Items.AddChild(Node, 'Obj');
    TempNode.Data := Pointer(num);
    SetLength(Face, num);
    for i := 0 to Num - 1 do
      VirtualBufer.Read(Face[i], 2 * 3);
    Tree.Items.AddChild(TempNode, 'Face').Data := @Face;

    VirtualBufer.Read(Num, 2);
    SetLength(Vert, num);
    for i := 0 to Num - 1 do
      VirtualBufer.Read(Vert[i], 4 * 3);
    Tree.Items.AddChild(TempNode, 'Vert').Data := @Vert;

    VirtualBufer.Read(IsPar, 1);
    if IsPar then
    begin
      SetLength(Normal, Num);
      for i := 0 to Num - 1 do
        VirtualBufer.Read(Normal[i], 4 * 3);
      Tree.Items.AddChild(TempNode, 'Norm').Data := @Normal;
    end;

    VirtualBufer.Read(IsPar, 1);

    begin
      SetLength(Color, Num);
      for i := 0 to Num - 1 do
      begin
        if IsPar then  
          VirtualBufer.Read(Color[i], 3);
        Color[i][3] := 255;
      end;
      Tree.Items.AddChild(TempNode, 'Color').Data := @Color;
      //        ColorUse:=true;
    end;
      
    VirtualBufer.Read(IsPar, 1);
    if IsPar then
    begin
      SetLength(TextCoord, Num);
      for i := 0 to Num - 1 do
        VirtualBufer.Read(TextCoord[i], 2 * 4);
      Tree.Items.AddChild(TempNode, 'TextCoord').Data := @TextCoord;
    end;

    VirtualBufer.Read(IsPar, 1);
    if IsPar then
    begin
      Tree.Items.AddChildFirst(TempNode.Parent, 'Material').Data :=
        @Attribute.Material;
      Attribute.Material.use := true;
      with Attribute do
      begin
        Material.Name := ReadName(VirtualBufer);
        Material.Abi := ReadColor(VirtualBufer);
        Material.Dif := ReadColor(VirtualBufer);
        Material.Spe := ReadColor(VirtualBufer);
        Material.Emi := ReadColor(VirtualBufer);
      end;
    end;

    VirtualBufer.Read(IsPar, 1);
    if IsPar then
    begin
      s := ReadName(VirtualBufer);
      Attribute.TextureId := XtextureStr.Add(s);
      Tree.Items.AddChildFirst(TempNode.Parent, 'Texture').Data :=
        PChar(XtextureStr.Strings[Attribute.TextureId]);
    end;
  end 
  else
  begin
    if XType = 'IN' then
      TempNode := Tree.Items.AddChild(Node, 'Node');
    if XType = 'GR' then
      TempNode := Tree.Items.AddChild(Node, 'Dummy');
    if XType = 'GO' then
      TempNode := Tree.Items.AddChild(Node, 'Object');
    if XType = 'BM' then
      TempNode := Tree.Items.AddChild(Node, 'BinModifer');
    if XType = 'GS' then
      TempNode := Tree.Items.AddChild(Node, 'GroupShape');
    if Transform.TransType <> TTNone then
      Tree.Items.AddChild(TempNode, 'Matrix').Data := @Transform;

    VirtualBufer.Read(Num, 2);
    SetLength(Childs, Num);
    for i := 0 to Num - 1 do
    begin
      Childs[i] := TMesh.Create;
      Childs[i].ReadXNode(VirtualBufer, XTextureStr, Tree, TempNode);
    end;
  end;
  //  TempNode.Data:=@Mesh;
end;

procedure TMesh.SaveAsXom3D(FileName: string);
const
  Zero: Integer = 0;
var
  VirtualBufer: TMemoryStream;
  s: string;
  i: Integer;

  procedure WriteColor(Color: Color4d);
  var
    b: Byte;
    i: Integer;
  begin
    for i := 0 to 2 do
    begin
      b := Round(Color[i] * 255);
      VirtualBufer.Write(b, 1);
    end;
  end;

  procedure WriteXNode(Mesh: TMesh);
  var
    num: Word;
    i, k: Integer;
    IsPar: Boolean;
  begin
    if Mesh = nil then   // XNodeInterion
    begin
      VirtualBufer.Write(PChar('XN')^, 3);
      Exit;
    end;

    if Mesh.xtype = 'BO' then   // XBone
    begin
   //  for i := 1 to 4 do
   //       VirtualBufer.Write(InvBoneMatrix[i], 3 * 4);
      Exit;
    end;

    VirtualBufer.Write(PChar(Mesh.XType)^, 3);

    VirtualBufer.Write(PChar(Mesh.Name)^, Length(Mesh.Name) + 1);

    with Mesh.Transform do
    begin
      VirtualBufer.Write(TransType, 1);
      if TransType = TTMatrix then
        for i := 1 to 4 do
          VirtualBufer.Write(TrMatrix[i], 3 * 4);
      if TransType = TTVector then
      begin
        VirtualBufer.Write(Pos[0], 3 * 4);
        VirtualBufer.Write(Rot[0], 3 * 4);
        VirtualBufer.Write(Size[0], 3 * 4);
      end;
      if TransType = TTJoint then
      begin
        VirtualBufer.Write(Pos[0], 3 * 4);
        VirtualBufer.Write(Rot[0], 3 * 4);
        VirtualBufer.Write(JointOrient[0], 3 * 4);
        VirtualBufer.Write(RotOrient[0], 3 * 4);
        VirtualBufer.Write(Size[0], 3 * 4);
        for i := 1 to 4 do
          VirtualBufer.Write(Mesh.Childs[0].InvBoneMatrix[i], 3 * 4);
      end;
    end;


     // XSkinShape XShape
    if (Mesh.xtype = 'SS') or (Mesh.xtype = 'SH') then
    begin
      num := Length(Mesh.Face);
      VirtualBufer.Write(Num, 2);
      // Mesh
       //XVertexDataSet
      //XIndexSet
      for i := 0 to Num - 1 do
        VirtualBufer.Write(Mesh.Face[i], 2 * 3);
      //XCoordSet
      num := Length(Mesh.Vert);
      VirtualBufer.Write(Num, 2);
      for i := 0 to Num - 1 do
        VirtualBufer.Write(Mesh.Vert[i], 4 * 3);
      //XNormalSet
      IsPar := Length(Mesh.Normal) > 0;
      VirtualBufer.Write(IsPar, 1);
      if IsPar then
        for i := 0 to Num - 1 do
          VirtualBufer.Write(Mesh.Normal[i], 4 * 3);
      //XColorSet
      IsPar := Length(Mesh.Color) > 0;
      VirtualBufer.Write(IsPar, 1);
      if IsPar then
        for i := 0 to Num - 1 do
          VirtualBufer.Write(Mesh.Color[i], 3);
      //XTexCoordSet
      IsPar := Length(Mesh.TextCoord) > 0;
      VirtualBufer.Write(IsPar, 1);
      if IsPar then
        for i := 0 to Num - 1 do
          VirtualBufer.Write(Mesh.TextCoord[i], 2 * 4);
      //XMaterial
      IsPar := Mesh.Attribute.Material.use;
      VirtualBufer.Write(IsPar, 1);
      if IsPar then 
        with Mesh.Attribute do 
        begin
          VirtualBufer.Write(PChar(Material.Name)^, Length(Material.Name) + 1);
          WriteColor(Material.Abi);
          WriteColor(Material.Dif);
          WriteColor(Material.Spe);
          WriteColor(Material.Emi);
        end;
      //XImage
      IsPar := Mesh.Attribute.Texture > 0;
      VirtualBufer.Write(IsPar, 1);
      if IsPar then 
      begin
        s := Format('XTexture#%d.tga', [Mesh.Attribute.TextureId]);
        SaveTga(s, But.XImage, Mesh.Attribute.TextureId, But.XImageLab);
        VirtualBufer.Write(PChar(s)^, Length(s) + 1);
      end;
      //XSkinShape
      if Mesh.xtype = 'SS' then
      begin
        k := Length(Mesh.Bones);
        VirtualBufer.Write(k, 2);
        for i := 0 to k - 1 do
        begin
          VirtualBufer.Write(PChar(Mesh.Bones[i].Name)^,
            Length(Mesh.Bones[i].Name) - 5);
          VirtualBufer.Write(Zero, 1);
        end;
        //XWeightSet
        for i := 0 to Num - 1 do
          VirtualBufer.Write(Mesh.Weight[i][0], k * 4);
      end;
    end
    else
    begin
      Num := Length(Mesh.Childs);
      VirtualBufer.Write(Num, 2);
      for i := 0 to Num - 1 do
        WriteXNode(Mesh.Childs[i]);
    end;
  end;
begin
  VirtualBufer := TMemoryStream.Create;
  VirtualBufer.Write(PChar('X3D')^, 4);
  WriteXNode(Self);
  VirtualBufer.SaveToFile(FileName);
  VirtualBufer.Free;
end;

{       TCntrVal       }
constructor TCntrVal.Create;
begin
  inherited Create;
end;
{}

procedure oglGrid(GridMax: Integer);
var
  i, size: Integer;
begin
  size := 5;
  glBegin(GL_LINES);
  for i := -GridMax to GridMax do
  begin
    glVertex3f(-GridMax * size, 0, i * size);
    glVertex3f(GridMax * size, 0, i * size);
    glVertex3f(i * size, 0, - GridMax * size);
    glVertex3f(i * size, 0, GridMax * size);
  end;
  glEnd;
end;

procedure oglAxes;
var 
  S: string;
begin
  glDisable(GL_LIGHTING);
  glColor3f(1, 0, 0);
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glEnd;
  glRasterPos3f(1.2, 0, 0); 
  s := 'x';
  glListBase(2000);//FontGL
  glCallLists(1, GL_UNSIGNED_BYTE, Pointer(S));
  glColor3f(0, 1, 0);
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glEnd;
  glRasterPos3f(0, 1.2, 0);
  s := 'y';
  glCallLists(1, GL_UNSIGNED_BYTE, Pointer(S));
  glColor3f(0, 0, 1);
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd;
  glRasterPos3f(0, 0, 1.2);
  s := 'z';
  glCallLists(1, GL_UNSIGNED_BYTE, Pointer(S));
  glEnable(GL_LIGHTING);
end;

procedure Light(var Color: Color4D);
var
  sinX, cosX, sinY, cosY, Step: Extended;
begin
  Step := TransView.zoom;
  SinCos((TransView.yrot - 90) * Pi / 180, sinY, cosY);
  SinCos((TransView.xrot - 90) * Pi / 180, sinX, cosX);

  Color[0] := -(Transview.xpos + Step * cosY * sinX);
  Color[1] := -(Transview.ypos + Step * cosX);
  Color[2] := -(Transview.zpos + Step * sinY * sinX);
end;

procedure ViewTga(XIndex: Integer; ImageT32: TImage; XGrid: Boolean;
  XLabel: TLabel);
label 
  f24;
var
  VBuf, VBuf2: Pointer;
  w, h, i, j, IName, ISize, CntrIndx: Integer;
  b: TBitmap;
  p0, p1: PARGBA;
  p2, p4: PARGB;
  p3: PAB;
  Grid, Bsize: Byte;
  Bgrid, Tgrid, Is24Bit: Boolean;
  alfa, beta: Real;
  offsetbyte, Imap: Integer;
  isXBOX: Boolean;
begin
  VBuf := Pointer(Longword(StrArray[XIndex].Point) + 3);
  IName := TestByte128(VBuf);
  isXBOX:=false;
  // DeleteObject(ImageT32.Picture.Bitmap.Handle);
  //  ImageT32.Picture.Bitmap.Create;

  b := TBitmap.Create;//ImageT32.Picture.Bitmap;
  b.canvas.Brush.Style := bsSolid;
  b.canvas.Brush.Color := clBlack;
  w := Word(VBuf^);
  b.Width := w; 
  Inc(Longword(VBuf), 2);
  h := Word(VBuf^);
  Inc(Longword(VBuf), 2);
  b.Height := h;

  Imap := Byte(VBuf^); 
  Inc(Longword(VBuf), 2);
  //  Is24Bit:=(byte(VBuf^)=0);  ????
  Inc(Longword(VBuf), 2);
  Bsize := Byte(VBuf^);
  Inc(Longword(VBuf), 4 * Bsize + 1);
  Bsize := Byte(VBuf^);
  Inc(Longword(VBuf), 4 * Bsize + 1);
  if Byte(VBuf^) = 7 then 
    isXBOX := true;
  Is24Bit := (Byte(VBuf^) = 0);
  Inc(Longword(VBuf), 4);
  //  ImageReady:=ImageReady and(typeimage=1);

  ISize := TestByte128(VBuf);
  VBuf2 := VBuf;
  Inc(Longword(VBuf2), ISize);
  CntrIndx := TestByte128(VBuf2);
  if not isXBOX then 
  begin
    if not Is24Bit then
    begin
      b.PixelFormat := pf32bit;
      //  VBuf:=pointer(Longword(Buf)+offsetbyte);
      Bgrid := false;
      for j := 0 to h - 1 do 
      begin
        if (j mod 8 = 0) then 
          Bgrid := not Bgrid;
        Tgrid := Bgrid;
        p0 := b.scanline[h - 1 - j]; 
        p1 := Pointer(Longword(VBuf) + j * w * 4);
        for i := 0 to w - 1 do 
        begin
          p0[i] := p1[i];
          if XGrid then 
          begin
            alfa := (p1[i].a / 256);
            beta := 1 - alfa;
            if (i mod 8 = 0) then 
              Bgrid := not Bgrid;
            if Bgrid then 
              grid := 192 
            else 
              grid := 255;
            p0[i].b := Round(p1[i].r * alfa + Grid * beta);
            p0[i].g := Round(p1[i].g * alfa + Grid * beta);
            p0[i].r := Round(p1[i].b * alfa + Grid * beta);
          end
          else 
          begin
            p0[i].b := p1[i].r;
            p0[i].r := p1[i].b;
          end;
        end;
        Bgrid := Tgrid;
      end;
      Xbit := 32;
    end
    else

    begin
      b.PixelFormat := pf24bit;
      //   VBuf:=pointer(Longword(Buf)+offsetbyte);
      for j := 0 to h - 1 do
      begin
        Pointer(p2) := Pointer(b.scanline[h - 1 - j]);
        p4 := Pointer(Longword(VBuf) + j * w * 3);
        for i := 0 to w - 1 do 
        begin
          p2[i] := p4[i];
          p2[i].b := p4[i].r;
          p2[i].r := p4[i].b;
        end;
      end;
      Xbit := 24;
    end;
  end 
  else
  begin
    b.PixelFormat := pf32bit;
    p1    := Pointer(Longword(StrArray[CntrIndx].point) + 11);
    Bgrid := false;
    for j := 0 to h - 1 do 
    begin
      if (j mod 8 = 0) then 
        Bgrid := not Bgrid;
      Tgrid := Bgrid;
      p0 := b.scanline[h - 1 - j]; 
      p3 := Pointer(Longword(VBuf) + j * w * 1);
      for i := 0 to w - 1 do 
      begin
        p0[i] := p1[p3[i]];
        if XGrid then 
        begin
          alfa := (p1[p3[i]].a / 256);
          beta := 1 - alfa;
          if (i mod 8 = 0) then 
            Bgrid := not Bgrid;
          if Bgrid then 
            grid := 192 
          else 
            grid := 255;
          p0[i].b := Round(p1[p3[i]].b * alfa + Grid * beta);
          p0[i].g := Round(p1[p3[i]].g * alfa + Grid * beta);
          p0[i].r := Round(p1[p3[i]].r * alfa + Grid * beta);
        end
        else 
        begin
          //  p0[i].b:=p1[p3[i]].r;
          //   p0[i].r:=p1[p3[i]].b;
        end;
      end;
      Bgrid := Tgrid;
    end;

    Xbit := 8;
  end;
  ImageT32.Picture.Bitmap.Assign(b);
  B.Free;
  XLabel.Caption :=
    Format('XImage [%d] - %dx%dx%dbit%sMaps:%d Size:%d',
    [XIndex, w, h, Xbit, #13#10, Imap, ISize]);
end;

procedure SaveTGA(Name: string; Image32: TImage; Index: Integer;
  XLabel: TLabel);
var
  VirtualBufer: TMemoryStream;
  tga: TgaHead;
begin
  ViewTga(Index, Image32, false, XLabel);
  VirtualBufer := TMemoryStream.Create;
  tga.zero := 0;
  tga.zero2 := 0;
  tga.zero3 := 0;
  tga.typeTGA := 2;
  tga.Width := Image32.Width;
  tga.Height := Image32.Height;
  tga.ColorBit := Xbit;
  VirtualBufer.Write(tga, SizeOf(tga) - 2);
  VirtualBufer.Write(Image32.Picture.Bitmap.scanline[tga.Height - 1]^,
    tga.Height * tga.Width * (tga.ColorBit div 8));
  VirtualBufer.SaveToFile(Name);
  VirtualBufer.Free;
end;


procedure TXom.LoadXomFileName(FileName: string; var OutCaption: string);
var
  iFileHandle: Integer;
  NumType: array [XTypes] of Integer;
  s, s2, s1: string;
  //sTemp:string;
  i, j, types, MaxInx, LenSTR, k, k2, k3, L, sizecount, sizeoffset,
  ID, ij, x, x1, x2, px, inx, inx1, inx2 : Integer;
  P, p2: Pointer;
  XType,xi: XTypes;
  IDtest, Outpoint, IsCtnr, ExpAnim: Boolean;
  _byte, _byte2: Integer;
  HtimeHour, HtimeMin, HtimeSec: Byte;
  TreeNode: TTreeNode;
  iFileLength: Integer;
  //Tree: TTreeNode;
  TreeN: array of TTreeNode;
  Treesize: array of Integer;

  function GetXType(_j: Integer): XTypes;
  var 
    _i: Integer;
  begin
    Result := XNone;
    for _i := 0 to Integer(XMax) do
      if NumType[XTypes(_i)] = _j then 
        Result := XTypes(_i);
  end;
begin
  saidx := 0;
  for i := 0 to Integer(XMax) do
    NumType[XTypes(i)] := -1;

  iFileHandle := FileOpen(FileName, fmOpenRead);
  iFileLength := FileSeek(iFileHandle, 0, 2);
  FileSeek(iFileHandle, 0, 0);
  FreeMem(Buf);
  Buf := AllocMem(iFileLength + 1);
  FileRead(iFileHandle, Buf^, iFileLength);
  //VirtualBufer:=TMemoryStream.Create;
  //    VBuf := AllocMem(2);
  //VirtualBufer.Size:=iFileLength*2;
  FileClose(iFileHandle);
  s := ExtractFileName(FileName);
  OutCaption := Format('%s - [%s]', [APPVER,s]);
  //     Button3.Enabled:=false;
  But.GlValueList.Strings.Clear;
  if But.TreeViewM<>nil then begin
  But.TreeViewM.Items.Clear;
  But.TreeViewM.Visible := false;
  end;
  types := Word(Pointer(Longword(Buf) + 24)^);
  Conteiners := Word(Pointer(Longword(Buf) + 28)^);
  BaseConteiner := Word(Pointer(Longword(Buf) + 32)^);
  SetLength(Treesize, types);
  SetLength(TreeN, types);
  for i := 0 to Length(StrArray) - 1 do
    if StrArray[i].Update then 
      FreeMem(StrArray[i].point);
  SetLength(StrArray, 0);
  SetLength(StrArray, Conteiners + 1);


  for i := 0 to types - 1 do 
  begin
    p           := Pointer(64 + i * 64 + 32 + Longword(Buf));
    Treesize[i] := Word(Pointer(Longword(Buf) + 64 + i * 64 + 8)^);
    //    if (Pchar(p)='XResourceDetails') and (i=0) then Treesize[i]:=1;
    for Xi:=Low(XTypes) to High(XTypes) do
     if AnsiStrComp(Pchar(p),PCharXTypes[Xi])=0 then
                NumType[Xi]:=i;
   if But.TreeViewM<>nil then
      TreeN[i] := But.TreeViewM.Items.Add(nil,
         Format('%s(%d)', [PChar(p), Treesize[i]]));
  end;
  //p:=pointer(64+Types*64+16+4+integer(Buf));
  if Longword(Pointer(64 + Types * 64 + 16 + Longword(Buf))^) <> Ctnr2 then
    p := Pointer(64 + Types * 64 + 16 + 16 + 4 + Longword(Buf))
  else
    p := Pointer(64 + Types * 64 + 16 + 4 + Longword(Buf));
    STOffset:=p;
  MaxInx := Longword(p^);
  But.Status.Text := Format('Strings: (%d)', [MaxInx]);
  Inc(Longword(p), 4);
  LenSTR := Longword(p^);
  Inc(Longword(p), 4);
  //<table str>
  k := Longword(p) + MaxInx * 4;

  for i := 0 to MaxInx - 2 do 
  begin
    L := Longword(Pointer(i * 4 + Longword(p) + 4)^);
    s := Format('%.2x', [byte128(i + 1)]);
    But.GlValueList.InsertRow(s, Utf8ToAnsi(PChar(Pointer(k + L))), true)
  end;
  k := LenSTR + MaxInx * 4;
  P := Pointer(Longword(p) + k);

  //   while (Longword(pointer(Longword(p))^)<>Ctnr) do inc(Longword(p));
  But.Status.Text := Format('Strings: (%d) - (%d)', [MaxInx, LenSTR]);
  //Tree adding
  StrArray[0].point := p;
  Outpoint := false;
  for j := 0 to types - 1 do 
  begin
    XType := GetXType(j);
    if Treesize[j] > 0 then
      for i := 1 to Treesize[j] do
        if not Outpoint then
        begin
          IsCtnr := true;
          p2 := Pointer(Longword(p) + 7);
          k := TestByte128(p2);
          if k >= But.GlValueList.RowCount then
          begin 
            s := '[OUT]'; 
            s2 := '0'; 
          end
          else 
          begin 
            s := But.GlValueList.Cells[1, k];
            s2 := But.GlValueList.Keys[k];
          end;
          case XType of
            XGraphSet:
            begin
              p2 := p;
              s := '';
              k := TestByte128(p2);
              for x := 1 to k do
              begin
                Inc(Longword(p2), 16);
                k3 := TestByte128(p2);
                k2 := TestByte128(p2);
                s := s + GetString(k2) + '; ';
              end;
              s := Format('(%d) Graph [%s]', [k, s]);
              IsCtnr := false;
            end;
            XOglTextureMap:
            begin
              p2 := Pointer(Longword(p) + 7);
              Inc(Longword(p2), 4);
              Inc(Longword(p2), 4 * 4);
              k3 := TestByte128(p2);
              Inc(Longword(p2), 4);
              k2 := Word(p2^);
              Inc(Longword(p2), 2);
              Inc(Longword(p2), 4 * 5);
              k := TestByte128(p2);
              //  dec(Longword(p2),4);
              // funit, float4,index,float,unit,funit5
              s := Format('OglMap [%d; %d]', [k3, k]);
              if k2 <> 1 then
                Inc(Longword(p2), 68);
              IsCtnr := false;
            end;
            XBitmapDescriptor:
            begin
              p2 := p;
              k3 := TestByte128(p2);
              s := GetString(k3);
              if WUM then  Inc(Longword(p2));
              Inc(Longword(p2));
              k2 := TestByte128(p2);
              s := Format('"%s" [%d] [%dx%d]',
                [s, k2, Word(p2^), Word(Pointer(Longword(p2) + 2)^)]);
              Inc(Longword(p2), 4);
              IsCtnr := false;
            end;
            XXomInfoNode:
            begin
              for x := 1 to k do
                TestByte128(p2);
              s := format('InfoData[%d]',[k]);
            end;
            W3DTemplateSet:
            begin
              for x := 1 to k do
                TestByte128(p2);
              Inc(Longword(p2), 12);
              Inc(Longword(p2), 8);
              s := GetString(TestByte128(p2));
              IsCtnr := false;
            end;
            XCustomDescriptor:
            begin
              p2 := p;
              s := GetString(TestByte128(p2));
              if WUM then  Inc(Longword(p2));
              Inc(Longword(p2), 3);
              IsCtnr := false;
            end;
            XMeshDescriptor:
            begin
              p2 := p;
              k := TestByte128(p2);
              if WUM then Inc(Longword(p2));
              k3 := Byte(p2^);
              Inc(Longword(p2));
              x := TestByte128(p2);
              Inc(Longword(p2), 2);// 80 00
              IsCtnr := false;
              s := Format('"%s" #%d (%d)', [GetString(k), x, k3]);
            end;
            XEnvironmentMapShader:
            begin
              p2 := Pointer(Longword(p) + 7);
              k2 := TestByte128(p2);
              k3 := TestByte128(p2);
              Inc(Longword(p2), 1);
              x := TestByte128(p2);
              x := TestByte128(p2);
              Inc(Longword(p2), 7);
              k := TestByte128(p2);
              s := Format('"%s" #%d [%d;%d]', [GetString(k), x, k2,k3]);
            end;
            XMultiTexShader:
            begin
              p2 := Pointer(Longword(p) + 7);
                inc(Longword(p2));
                k:=TestByte128(p2);
                k:=TestByte128(p2);
                k:=TestByte128(p2);
             //   k:=TestByte128(p2);
                inc(Longword(p2),4);
                s:=GetString(TestByte128(p2));
            end;
            XTextDescriptor:
            begin
              p2 := p;
              s := GetString(TestByte128(p2));
              if WUM then  Inc(Longword(p2));
              k := TestByte128(p2); //1  number
              k := TestByte128(p2); //2  link
              k := Word(p2^);       // size
              Inc(Longword(p2), 4); // 1
              for x := 1 to k do
                Inc(Longword(p2), 6); // num, wchar, wchar
              //     k3:=TestByte128(p2);
              s := Format('"%s" #%d', [s, k]);
              IsCtnr := false;
            end;
            XNullDescriptor:
            begin
              p2 := p;
              s := GetString(TestByte128(p2));
              if WUM then  Inc(Longword(p2));
              k := TestByte128(p2);
              s := Format('"%s" (%d)', [s, k]);
              IsCtnr := false;
            end;
            XSpriteSetDescriptor:
            begin
              p2 := p;
              s := GetString(TestByte128(p2));
              if WUM then  Inc(Longword(p2));
              Inc(Longword(p2), 1);// 0
              k3 := TestByte128(p2);
              s := Format('"%s" #%d', [s, k3]);
              IsCtnr := false;
            end;
            XCollisionData:
            begin
              p2 := Pointer(Longword(p) + 9);
              k := TestByte128(p2); 
              k3 := 0;
              if k <> 0 then 
                k3 := TestByte128(p2);
              s := Format('[%d]', [k3]);
            end;
            XImage: 
            begin
              s := GetString(k);
              //                k2:=TestByte128(p2);
              s := Format('"%s" [%dx%d]',
                [s, Word(p2^), Word(Pointer(Longword(p2) + 2)^)]);
            end;
            XInteriorNode:
            begin
              for x := 1 to k do
                k3 := TestByte128(p2);
              px := Longword(p2);
              p2 := Pointer(Longword(p2) + 4 * 5);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s [%.2f; %.2f; %.2f; %.2f]',
                [s, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XGroup, XSkin:
            begin
              k := TestByte128(p2);
              for x := 1 to k do
                k3 := TestByte128(p2);
              px := Longword(p2);
              p2 := Pointer(Longword(p2) + 4 * 5);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s [%.2f; %.2f; %.2f; %.2f]',
                [s, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XChildSelector:
            begin
              s := 'Childs';
            end;
            XBinModifier:
            begin
              k := TestByte128(p2);
              //               for x:=1 to k do
              k3 := TestByte128(p2);
              k := TestByte128(p2);
              for x := 1 to k do
                k3 := TestByte128(p2);
              px := Longword(p2);
              p2 := Pointer(Longword(p2) + 4 * 5);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s [%.2f; %.2f; %.2f; %.2f]',
                [s, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XBone:
            begin
              p2 := Pointer(Longword(p) + 153 + 4);
              s := GetString(TestByte128(p2));
            end;
            XAnimClipLibrary:
            begin
              p2 := p;
              s1 := GetString(TestByte128(p2));  // Name
              inx1 := Integer(p2^); // NumKeys
              //    if inx>16000 then break;
              Inc(Longword(p2), 4);
              for x := 1 to inx1 do 
              begin   // Keys
                Inc(Longword(p2), 4);   //AnimType
                s1 := GetString(TestByte128(p2)); //Object
              end;
              inx2 := Integer(p2^);  // NumClips
              Inc(Longword(p2), 4);
              s := Format('Clips[%d]', [inx2]);
              for x := 1 to inx2 do 
              begin
                Inc(Longword(p2), 4); // time
                s1 := GetString(TestByte128(p2)); // name
                inx := Word(p2^);     // NumAnimKeys
                ExpAnim := (inx = 256) or (inx=257);
                // 00 01   - zero Frame
                // 01 01   - frame no Index
                // 24 00   - index frame
                //  ExpAnim:= inx <> integer($0101);
                if ExpAnim then 
                  inx := inx1 
                else
                  Inc(Longword(p2), 4);
                for x1 := 1 to inx do 
                begin // AnimKeys
                  x2 := Word(p2^);
                  if x2 = 256 then  
                  begin 
                    Inc(Longword(p2), 16); 
                    Continue; 
                  end;
                  Inc(Longword(p2), 4); // 1 1 0 0
                  if not ExpAnim then
                  begin
                    k2 := Word(p2^); 
                    Inc(Longword(p2), 2);
                  end;
                  k3 := Word(p2^); 
                  Inc(Longword(p2), 2);
                  Inc(Longword(p2), 6);
                  k := Integer(p2^); 
                  Inc(Longword(p2), 4);
                  for x2 := 1 to k do
                    Inc(Longword(p2), 6 * 4);
                end;
              end;
              IsCtnr := false;
            end;
            XSkinShape:
            begin
              for x := 1 to k do
                k3 := TestByte128(p2);
              Inc(Longword(p2), 4);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              Inc(Longword(p2), 5);
              px := Longword(p2);
              p2 := Pointer(Longword(p2) + 4 * 5);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s [%.2f; %.2f; %.2f; %.2f]',
                [s, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XShape:
            begin
              Inc(Longword(p2), 3);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              Inc(Longword(p2), 5);
              px := Longword(p2);
              p2 := Pointer(Longword(p2) + 4 * 5);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s [%.2f; %.2f; %.2f; %.2f]',
                [s, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XTransform:
            begin
              px := Longword(p) + 7;
              s := Format('Pos <%.2f; %.2f; %.2f>',
                [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]);
            end;
            XJointTransform:
            begin
              px := Longword(p) + 7;
              s := Format('<%.2f; %.2f; %.2f>',
                [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]);
            end;
            XMatrix:
            begin
              px := Longword(p) + 7 + 36;
              s := Format('Pos <%.2f; %.2f; %.2f>',
                [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]);
            end;
            XTexturePlacement2D:
            begin
              //px:=Longword(p)+7;
              s := 'Matrix';
            end;
            XSimpleShader:
            begin
              for x := 1 to k do
                k3 := TestByte128(p2);
              k := TestByte128(p2);
              for x := 1 to k do
                k3 := TestByte128(p2);
              Inc(Longword(p2), 4);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s', [s]);
            end;
            XTexFont:
            begin
              for x := 1 to k do 
                Inc(Longword(p2), 8);
              k := TestByte128(p2);
              for x := 1 to k do 
                Inc(Longword(p2), 8);
              k := TestByte128(p2);
              for x := 1 to k do
                k3 := TestByte128(p2);
              k := TestByte128(p2);
              for x := 1 to k do
                k3 := TestByte128(p2);
              Inc(Longword(p2), 4);
              s := GetString(TestByte128(p2));
              s := Format('%s', [s]);
            end;
            XContainerResourceDetails:
            begin
              k3 := TestByte128(p2);
              s := GetString(k3);
              k3 := TestByte128(p2);
              s := Format('%s [%d]', [s,k3]);
              //       sTemp:= sTemp+s+nVn;
            end;
            XFloatResourceDetails: 
            begin
              p2 := Pointer(Longword(p) + 7 + 4);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s = %f', [s, Single(Pointer(Longword(p) + 7)^)]);
              //         sTemp:= sTemp+s+nVn;
            end;
            XIntResourceDetails, XUintResourceDetails: 
            begin
              p2 := Pointer(Longword(p) + 7 + 4);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s := Format('%s = %d', [s, Word(Pointer(Longword(p) + 7)^)]);
              //        sTemp:= sTemp+s+nVn;
            end;
            XStringResourceDetails, GSProfile,XExportAttributeString:
            begin
              k3 := TestByte128(p2);
              if k = 0 then 
                s2 := '' 
              else
                s2 := But.GlValueList.Cells[1, k];
              s := But.GlValueList.Cells[1, k3];
              if (XType = GSProfile) then 
                s := Format('Nick:%s / E-mail:%s', [s2, s]) 
              else
                s := Format('%s = %s', [s, s2]);
              //         sTemp:= sTemp+s+nVn;
            end;
            LockedContainer:
            begin
              p2 := Pointer(Longword(p) + 7 + 1);
              k3 := TestByte128(p2);
              s := GetString(k3);
            end;
            BaseWeaponContainer:
            begin
             s:='BaseWeapon';
            end;
            MeleeWeaponPropertiesContainer:
            begin
             s:='MeleeWeapon';
            end;
            GunWeaponPropertiesContainer:
            begin
             s:='GunWeapon';
            end;
            SentryGunWeaponPropertiesContai:
            begin
            begin
             s:='SentryGunWeapon';
            end;
            end;
            FlyingPayloadWeaponPropertiesCo:
            begin
             s:='FlyingPayloadWeapon';
            end;
            JumpingPayloadWeaponPropertiesC:
            begin
             s:='JumpingPayloadWeapon';
            end;
            HomingPayloadWeaponPropertiesCo:
            begin
             s:='HommingPayloadWeapon';
            end;
            PayloadWeaponPropertiesContaine:
            begin
             s:='PayloadWeapon';
            end;
            MineFactoryContainer:
            begin
             s:='MineFactory';
            end;
            ParticleEmitterContainer:
            begin
             s:='EmitterData';
            end;
            EffectDetailsContainer:
            begin
            s:=format('EffectNames [%d]',[k]);
            end;
            HighScoreData:
            begin
              s1 := '';
              s1 := s1 + Format('%s - %.2f ', [s, ToTime(Longword(p2^))]);
              p2 := Pointer(Longword(p2) + 4);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s1 := s1 + Format('%s - %.2f ', [s, ToTime(Longword(p2^))]);
              p2 := Pointer(Longword(p2) + 4);
              k3 := TestByte128(p2);
              s := GetString(k3);
              s1 := s1 + Format('%s - %.2f ', [s, ToTime(Longword(p2^))]);
              s := s1;
              //         sTemp:= sTemp+s+nVn;
            end;
            StoredTeamData:
            begin
              s := Format('Team [%s]', [s]);
            end;
            CampaignData:
            begin
              s := Format('Compaign [%d]', [k]);
            end;
            XZBufferWriteEnable: 
            begin
              s := BoolToStr(Boolean(k), true);
            end;
            XAlphaTest:
            begin
              s := Format('Alpha [%d; %.2f]',
                [Longword(Pointer(p2)^), Single(Pointer(Longword(p2) + 4)^)]);
            end;
            XLightingEnable:
            begin
              px := Longword(p) + 14;
              s := Format('Light %s %d [%.2f; %.2f; %.2f; %.2f]',
                [BoolToStr(Boolean(k), true), Longword(Pointer(Longword(p) + 10)^),
                Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^),
                Single(Pointer(px + 12)^)]);
              p2 := Pointer(Longword(p) + 14 + 16);
              IsCtnr := false;
            end;
            XBlendModeGL:
            begin
              s := Format('Blend [%d; %d]',
                [Longword(Pointer(Longword(p) + 7)^), Longword(Pointer(Longword(p) + 7 + 4)^)]);
              p2 := Pointer(Longword(p) + 11);
              //    IsCtnr:=false;
            end;
            XDepthTest:
            begin
              s := Format('Depth [%d; %d; %f]',
                [Longword(Pointer(Longword(p) + 7)^), Longword(Pointer(Longword(p) + 7 + 4)^),
                Single(Pointer(Longword(p) + 7 + 9)^)]);
            end;
            XCullFace:
            begin
              s := BoolToStr(Boolean(k), true);
            end;
            XPointLight:
            begin
             p2 := Pointer(Longword(p) + 7 + 86);
             s2 := GetString(TestByte128(p2));
            s := Format('%s = <x:%f y:%f z:%f>',
                [s2, Single(Pointer(Longword(p) + 7)^), Single(Pointer(Longword(p) + 7 + 4)^),
                Single(Pointer(Longword(p) + 7 + 8)^)]);
            end;
            XMaterial:
            begin
              s := Format('Ambiend = [%.2f; %.2f; %.2f; %.2f]',
                [Single(Pointer(Longword(p) + 7)^), Single(Pointer(Longword(p) + 7 + 4)^),
                Single(Pointer(Longword(p) + 7 + 8)^), Single(Pointer(Longword(p) + 7 + 12)^)]);
            end;
            XVectorResourceDetails:
            begin
              p2 := Pointer(Longword(p) + 7 + 12);
              k := TestByte128(p2);
              s2 := GetString(k);
              s := Format('%s = <x:%f y:%f z:%f>',
                [s2, Single(Pointer(Longword(p) + 7)^), Single(Pointer(Longword(p) + 7 + 4)^),
                Single(Pointer(Longword(p) + 7 + 8)^)]);
              //         sTemp:= sTemp+s+nVn;
            end;
            XColorResourceDetails: 
            begin
              p2 := Pointer(Longword(p) + 7 + 4);
              k := TestByte128(p2);
              s2 := GetString(k);
              s := Format('%s = <r:%d g:%d b:%d a:%d>',
                [s2, Byte(Pointer(Longword(p) + 7)^), Byte(Pointer(Longword(p) + 7 + 1)^),
                Byte(Pointer(Longword(p) + 7 + 2)^), Byte(Pointer(Longword(p) + 7 + 3)^)]);
              //            sTemp:= sTemp+s+nVn;
            end;
            XCoord3fSet, XNormal3fSet: 
            begin
              px := Longword(p2);
              s := Format('[%d] ', [k]);
              for x := 0 to k - 1 do 
              begin
                s := s + Format('<%f; %f; %f> ',
                  [Single(Pointer(px + x * 12)^), Single(Pointer(px + x * 12 + 4)^),
                  Single(Pointer(px + x * 12 + 8)^)]);
                if Length(s) > 100 then 
                begin 
                  s := s + '...'; 
                  Break; 
                end;
              end;
            end;
            XTexCoord2fSet: 
            begin
              px := Longword(p2);
              s := Format('[%d] ', [k]);
              for x := 0 to k - 1 do 
              begin
                s := s + Format('<%f; %f> ',
                  [Single(Pointer(px + x * 12)^), Single(Pointer(px + x * 8 + 4)^)]);
                if Length(s) > 100 then 
                begin 
                  s := s + '...'; 
                  Break; 
                end;
              end;
            end;
            XMultiTexCoordSet:
            begin
              p2 := Pointer(Longword(p) + 7);
              k := TestByte128(p2);
              k2:= TestByte128(p2);
              k3:= TestByte128(p2);
              s := Format('%d:[%d][%d]', [k,k2,k3]);
            end;
            XPaletteWeightSet:
            begin
              p2 := Pointer(Longword(p) + 7);
              k := TestByte128(p2);
              s := Format('[%d bones index]', [k]);
            end;
            XWeightSet:
            begin
              p2 := Pointer(Longword(p) + 7);
              k2 := Word(p2^);
              Inc(Longword(p2), 2);
              k := TestByte128(p2);
              px := Longword(p2);
              s := Format('[%d bones] [%d vertex] <', [k2, k]);
              for x := 0 to k - 1 do 
              begin
                s := s + Format('%.2f; ', [Single(Pointer(px + x * 4)^)]);
                if Length(s) > 100 then 
                begin 
                  s := s + '...'; 
                  Break; 
                end;
              end;
            end;
            XColor4ubSet: 
            begin
              px := Longword(p2);
              s := Format('[%d] ', [k]);
              for x := 0 to k - 1 do 
              begin
                s := s + Format('<%d; %d; %d; %d> ',
                  [Byte(Pointer(px + x * 4)^), Byte(Pointer(px + x * 4 + 1)^),
                  Byte(Pointer(px + x * 4 + 2)^), Byte(Pointer(px + x * 4 + 3)^)]);
                if Length(s) > 100 then 
                begin 
                  s := s + '...'; 
                  Break; 
                end;
              end;
            end;
            XIndexedTriangleSet: 
            begin
              Inc(Longword(p2), 4);
              k2 := Word(p2^);
              Inc(Longword(p2), 4);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              Inc(Longword(p2));
              px := Longword(p2);
              s := Format('%d faces [%.2f; %.2f; %.2f][%.2f; %.2f; %.2f]',
                [k2, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^),
                Single(Pointer(px + 16)^), Single(Pointer(px + 20)^)]);
            end;
            XIndexedTriangleStripSet: 
            begin
              p2 := Pointer(Longword(p) + 8);
              k2 := Word(p2^);
              Inc(Longword(p2), 2);
              k3 := TestByte128(p2);
              Inc(Longword(p2), 8);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              k3 := TestByte128(p2);
              Inc(Longword(p2));
              px := Longword(p2);
              s := Format('%d faces [%.2f; %.2f; %.2f][%.2f; %.2f; %.2f]',
                [k2, Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^),
                Single(Pointer(px + 16)^), Single(Pointer(px + 20)^)]);
            end;
            XConstColorSet: 
            begin
              px := Longword(p) + 7;
              s := Format('[%.2f; %.2f; %.2f; %.2f]',
                [Single(Pointer(px)^), Single(Pointer(px + 4)^),
                Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
            end;
            XIndexSet: 
            begin
              s := Format('[%d] ', [k]);
            end;
            XCollisionGeometry: 
            begin
              p2 := Pointer(Longword(p) + 4 + 7 + 8);
              s := Format('Faces = %d', [TestByte128(p2)]);
            end;
            XDataBank: 
            begin
              s := Format('Bank #%d', [k]);
            end;
            XExpandedAnimInfo:
            begin
              s := 'Data';
              if (Longword(p^) = Ctnr) then
                p2 := Pointer(Longword(p) + 7 + 4)
              else
                p2 := Pointer(Longword(p) + 7);
              IsCtnr := false;
            end;
            XPalette: 
            begin
              s := 'Palette';
            end;
            XAnimInfo: 
            begin
              s := 'Data';
              if (Longword(p^) = Ctnr) then
                p2 := Pointer(Longword(p) + 5 + 4)
              else
                p2 := Pointer(Longword(p) + 5);
              IsCtnr := false;
            end;
            XFortsExportedData: 
            begin
              s := 'Data';
            {     p2:=pointer(Longword(p)+7);
                k:=TestByte128(p2);
                 for x:=1 to k do
                    k3:=TestByte128(p2);
                inc(Longword(p2),6);

                k:=TestByte128(p2);
                 for x:=1 to k do
                    k3:=TestByte128(p2);
                inc(Longword(p2),14);

                k:=TestByte128(p2);
                 for x:=1 to k do
                    k3:=TestByte128(p2);

                k:=TestByte128(p2);
                 for x:=1 to k do
                     inc(Longword(p2),4*3);
                k:=TestByte128(p2);
                 for x:=1 to k do
                     inc(Longword(p2),4*3);

                k:=TestByte128(p2);
                for x:=1 to k do
                    k3:=TestByte128(p2);
               // IsCtnr:=false;     }
            end;
            PC_LandChunk: 
            begin
              p2 := Pointer(Longword(p) + 7 + 4);
              Inc(Longword(p2), 4 * 4);
              Inc(Longword(p2), 5);
              IsCtnr := false;
              s := 'Data';
            end;
            PC_LandFrame:
            begin
              p2 := Pointer(Longword(p) + 7);
              k3 := TestByte128(p2);
              Inc(Longword(p2), k3);
              k3 := TestByte128(p2);
              Inc(Longword(p2), k3);
              Inc(Longword(p2), 2);    //00 00
              Inc(Longword(p2), 4);    // 3d 00 6c 00
              Inc(Longword(p2), 368);  // pos?
              k3 := TestByte128(p2);   // layers 1
              Inc(Longword(p2), k3 * 2 * 4);
              k3 := TestByte128(p2); // layers 2
              Inc(Longword(p2), k3 * 2 * 4);
              k3 := TestByte128(p2); // ?
              Inc(Longword(p2), k3 * 4);
              Inc(Longword(p2), 4);
              k3 := TestByte128(p2); // ?
              Inc(Longword(p2), k3 * 4);
              k3 := TestByte128(p2); // ?
              Inc(Longword(p2), k3 * 4);
              // ?????
              k3 := TestByte128(p2); // ?
              Inc(Longword(p2), (k3 - 1) * 4);
              Inc(Longword(p2), 16); //???
              Inc(Longword(p2), 4);  // ff ff ff ff
              k3 := TestByte128(p2); // childs
              for x := 1 to k3 do 
                TestByte128(p2);
              Inc(Longword(p2), 4 * 4); // coord4
              Inc(Longword(p2), 4);     // zero
              s := GetString(TestByte128(p2));
              IsCtnr := false;
            end;
            GSProfileList:
            begin
              s:= Format('List[%d]', [k]);
            end;
            SchemeData:
            begin
              s:= Format('(%s)', [s]);
            end;
            WeaponSettingsData:
            begin
              s:= 'Weapon';
            end;
            SchemeColective:
            begin
              s:= Format('Scheme[%d]', [k]);
            end;
            TeamDataColective:
            begin
              s:= Format('Team[%d]', [k]);
            end;
            LandFrameStore:
            begin
              s := Format('(%s)', [s]);
            end;
            DetailEntityStore:
            begin
              s := Format('(%s)', [s]);
            end;
            XPositionData: 
            begin
              p2 := Pointer(Longword(p) + 12);
              s := 'Data';
              IsCtnr := false;
            end;
            XDetailObjectsData: 
            begin
              p2 := Pointer(Longword(p) + 7);
              k := TestByte128(p2);
              for x := 1 to k do 
                k3 := TestByte128(p2);
              k := TestByte128(p2);
              for x := 1 to k do 
                k3 := TestByte128(p2);

              k := TestByte128(p2);
              for x := 1 to k do 
                Inc(Longword(p2), 3 * 4);
              k := TestByte128(p2);
              for x := 1 to k do 
                Inc(Longword(p2), 3 * 4);
              k := TestByte128(p2);
              for x := 1 to k do 
                Inc(Longword(p2), 3 * 4);

              s := 'Data';
              IsCtnr := false;
            end;
            XNone:
              s := Format('0x%x (%s)', [k, s]);
          end;
          Inc(saidx);
          if But.TreeViewM<>nil then begin
          TreeNode := But.TreeViewM.Items.AddChild(TreeN[j],
            Format('%d. %s', [saidx, s]));
          TreeNode.Data := Pointer(saidx);
          if saidx = BaseConteiner then
            BaseNode := TreeNode;
          end;
          if (Longword(p^) = Ctnr) then
            sizeoffset := 4
          else  
            sizeoffset := 0;
          sizecount := sizeoffset;
          StrArray[saidx].point := Pointer(Longword(p) + sizeoffset);
          StrArray[saidx].XType := XType;
          StrArray[saidx].CTNR := (sizeoffset = 4);

          if not IsCtnr then  
            sizecount := Longword(p2) - Longword(p);

          But.XLabel.Caption := Format('%.1f %%', [saidx / Conteiners * 100]);
          But.XLabel.Repaint;
          {    if j <> types-1 then //XUintResourceDetails         }
          if (IsCtnr) then

            while (Longword(Pointer(Longword(p) + sizecount)^) <> Ctnr) do 
            begin
              if ((Longword(p) + sizecount - Longword(Buf)) > iFileLength) then
              begin
                Outpoint := true;  
                StrArray[saidx].size := sizecount - sizeoffset - 1; 
                Exit;
              end;
              Inc(sizecount);
            end;

          StrArray[saidx].size := sizecount - sizeoffset;
          //    end
          //   else StrArray[saidx].size:=sizecount;

          But.StatusM.Text := Format('%d', [saidx]);

          // FormXom.TreeView1.Refresh;

          p := Pointer(Longword(p) + sizecount);
        end;
    //  inc(saidx);
  end
end;


procedure TXom.XomImportObj(InNode, OutNode: TTreeNode);
var 
  i, j: Integer;
  XIndex, size: Integer;
  NewPoint, p: Pointer;
  Vert: AVer;
  Normal: AVer;
  Face: AFace;
  Color: AUColor;
  TextCoord: ATCoord;
  TrMatrix: TMatrix;
  Attribute: TAttribute;
  Transform: TTrans;
  Num: Integer;
  function GetXMem(Index, size: Integer): Pointer;
  begin
    if StrArray[Index].Update then   
      FreeMem(StrArray[Index].point);
    StrArray[Index].Update := true;
    Result := AllocMem(size);
  end;

  procedure WriteXArray(Index: Integer; Arr: Pointer;
    Len, Size: Integer);
  var
    NewPoint, p: Pointer;
  begin
    if StrArray[Index].Update then   
      FreeMem(StrArray[Index].point);
    StrArray[Index].Update := true;
    NewPoint := AllocMem(Len * Size + 8);
    p := NewPoint;
    Inc(Longword(p), 3);
    WriteXByteP(p, Len);
    Move(Arr^, p^, Len * Size);
    Inc(Longword(p), Len * Size);
    StrArray[Index].point := NewPoint;
    StrArray[Index].size := Longword(p) - Longword(NewPoint);
  end;
begin
  XIndex := Integer(OutNode.Data);
  case StrArray[XIndex].Xtype of
    // XGroup:
    // XInteriorNode:
    XTransform:
    begin
      NewPoint := GetXMem(XIndex, StrArray[XIndex].size);
      Move(StrArray[XIndex].point^, NewPoint^, StrArray[XIndex].size);
      Transform := TTrans(InNode.Data^);
      //       MatrixDecompose(XMatrix,XPos,XRot,XSize);
      p := Pointer(Longword(NewPoint) + 3);
      Move(Transform.Pos[0], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      Move(Transform.Rot[0], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      Move(Transform.Size[0], p^, 4 * 3);
      Inc(Longword(p), 4 * 3);
      Inc(Longword(p), 4);
      Move(Transform.TrMatrix[1][1], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      Move(Transform.TrMatrix[2][1], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      Move(Transform.TrMatrix[3][1], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      Move(Transform.TrMatrix[4][1], p^, 4 * 3); 
      Inc(Longword(p), 4 * 3);
      StrArray[XIndex].point := NewPoint;
    end;
    XImage:
    begin
      ImportXImage(XIndex, string(InNode.Data));
    end;
    XMaterial:
    begin
    end;
    XIndexedTriangleSet:
    begin
      NewPoint := GetXMem(XIndex, StrArray[XIndex].size);
      Move(StrArray[XIndex].point^, NewPoint^, StrArray[XIndex].size);
      p := Pointer(Longword(NewPoint) + 3);
      TestByte128(p);
      Inc(Longword(p), 4);
      Move(InNode.Data, p^, 4);
      StrArray[XIndex].point := NewPoint;
    end;
    XIndexSet:
    begin
      Face := AFace(InNode.Data^);
      Num := Length(Face);
      NewPoint := GetXMem(XIndex, Num * 6 + 8);
      p := NewPoint;
      Inc(Longword(p), 3);
      WriteXByteP(p, Num * 3);
      for j := 0 to Num - 1 do
      begin
        Move(Face[j], p^, 6);
        Inc(Longword(p), 6);
      end;
      StrArray[XIndex].point := NewPoint;
      StrArray[XIndex].size := Longword(p) - Longword(NewPoint);
    end;
    // XShape:
    XCoord3fSet:
    begin
      Vert := AVer(InNode.Data^);
      WriteXArray(XIndex, @Vert[0], Length(Vert), 12);
    end;
    XNormal3fSet:
    begin
      Normal := AVer(InNode.Data^);
      WriteXArray(XIndex, @Normal[0], Length(Normal), 12);
    end;
    XColor4ubSet:
    begin
      Color := AUColor(InNode.Data^);
      WriteXArray(XIndex, @Color[0], Length(Color), 4);
    end;
    XTexCoord2fSet:
    begin
      TextCoord := ATCoord(InNode.Data^);
      WriteXArray(XIndex, @TextCoord[0], Length(TextCoord), 8);
    end;
  end;

  for i := 0 to InNode.Count - 1 do
    XomImportObj(InNode[i], OutNode[i]);
end;



procedure TXom.SaveXom(SaveDialog: TSaveDialog; Conteiners: XConteiners);
var
  VirtualBufer: TMemoryStream;
  p,p1,p2,p3,p4: Pointer;
  OldXom: Pointer;
  s: string;
  i,j,len, n, LastUpdate: Integer;
  CntrBegin: Longword;
  VBufBegin,VBuf: Pointer;
  LStrings:TStringList;

  procedure WriteConteiner(var Conteiner: TConteiner);
  begin
    if Conteiner.CTNR then
    begin
    //  p := PChar('CTNR');
      p := CtnrS;
      VirtualBufer.Write(p^, 4);
    end;

    VirtualBufer.Write(Conteiner.point^, Conteiner.size);
  end;
begin
  OldXom := Buf;
  if SaveDialog.Execute and (SaveDialog.FileName <> '') then
  begin
    VirtualBufer := TMemoryStream.Create;
    n := Length(Conteiners);
{for i:=1 to n do
 if Conteiners[i].update then
        begin
        FirstChange:=LongWord(Conteiners[i-1].point);
        inc(FirstChange,Conteiners[i-1].size);
        LastUpdate:=i;
        end;    }
  //  CntrBegin := Longword(Conteiners[0].point);
  //  Dec(CntrBegin, Longword(OldXom));
  //  VirtualBufer.Write(OldXom^, CntrBegin);
    VirtualBufer.Write(OldXom^, Longword(STOffset)-Longword(OldXom));

  VBufBegin := AllocMem(1024*1024);
  VBuf := VBufBegin;

  // Begin String Table
 { p := Schm;
  Move(p^, VBuf^, 4); 
  Inc(Longword(VBuf), 16);
  p := StrS;
  Move(p^, VBuf^, 4); }
  Inc(Longword(VBuf), 12);
  LStrings := TStringList.Create;
  j:=But.GlValueList.RowCount;
 // LStrings.Duplicates:=dupAccept;
  for i:=1 to j-1 do
       LStrings.Add(But.GlValueList.Cells[1,i]);

  j := LStrings.Count;
  p2 := VBuf;
  p4 := Pointer(Longword(p2) + j * 4);
  Len := 1;
  //Str Table
  for i := 0 to j - 1 do
  begin
    s  := LStrings.Strings[i];
  //  Smallint(Pointer(LStrings.IndexOf(s) * 4 + Longword(p2))^) := Smallint(Len);
    Smallint(Pointer(i * 4 + Longword(p2))^) := Smallint(Len);
    p3 := Pointer(Longword(p4) + Len);
    p  := PChar(s);
    Move(p^, p3^, Length(s));
    Len := Len + Length(s) + 1;
  end;
  Smallint(Pointer(Longword(p2) - 12)^) := Smallint(j + 1);
  Smallint(Pointer(Longword(p2) - 8)^) := Smallint(Len);
  VBuf := Pointer(Longword(p4) + Len);
  // End Table String
    VirtualBufer.Write(VBufBegin^, Longword(VBuf)-Longword(VBufBegin));
    FreeMem(VBufBegin);
    for i := 1 to n - 1 do
      WriteConteiner(Conteiners[i]);

    VirtualBufer.SaveToFile(SaveDialog.FileName);
    VirtualBufer.Free;
  end;
end;



function ImportXImage(XIndex: Integer; Name: string): Boolean;
label
  ExitP;
var
  TGABuf, VBuf: Pointer;
  Iname, W, h, Size, NumMaps: Integer;
  TgaH: TgaHead;
  alpha: Integer;

  function LoadTgaToMemeory(FileName: string): Pointer;
  var
    iFileHandle, iFileLength: Integer;
  begin
    if FileExists(FileName) then
    begin
      iFileHandle := FileOpen(FileName, fmOpenRead);
      if iFileHandle > 0 then
      begin
        iFileLength := FileSeek(iFileHandle, 0, 2);
        FileSeek(iFileHandle, 0, 0);
        Result := AllocMem(iFileLength + 1);
        FileRead(iFileHandle, Result^, iFileLength);
        FileClose(iFileHandle);
      end;
    end
  end;

  procedure GetMipmap(var MipXImage: Pointer; RGBImage: Pointer;
    const TgaH: TgaHead; Width, Height, Compon: Integer);
  var 
    i, IFormat: Integer;
  begin
    IFormat := GL_RGBA;
    if Compon = 3 then
      IFormat := GL_RGB;
    if (TgaH.Width <> Width) and (TgaH.Height <> Height) then
      gluScaleImage(IFormat, TgaH.Width, TgaH.Height, GL_UNSIGNED_BYTE,
        RGBImage,
        Width, Height, GL_UNSIGNED_BYTE, MipXImage)
    else
      Move(RGBImage^, MipXImage^, TgaH.Width * TgaH.Height * Compon);
  end;

  function MakeXImage(Tga: Pointer; const TgaH: TgaHead;
    Iname, NumMaps: Integer; var xSize: Integer): Pointer;
  var
    p0: PARGB;
    p1: PARGBA;
    VirtualBufer: TMemoryStream;
    nbyte: Byte;
    MipXImage, RGBImage: Pointer;
    Maps, NMaps, Itype, i, j, wSize, Alpha, Compon, FullSize,
    Width, Height, offset: Integer;
  const
    zero: Integer = 0;
  begin
    VirtualBufer := TMemoryStream.Create;
    VirtualBufer.Write(zero, 3);
    WriteXByte(VirtualBufer, iName);//?
    VirtualBufer.Write(TgaH.Width, 2);
    VirtualBufer.Write(TgaH.Height, 2);
    if NumMaps = 1 then 
      NMaps := 1
    else 
    begin
      Maps  := Max(TgaH.Width, TgaH.Height);
      NMaps := Round(Log2(Maps)) + 1;
    end;
    VirtualBufer.Write(NMaps, 2);
    Itype := 0; 
    Compon := 3; 
    Alpha := 0;
    if TgaH.ColorBit = 32 then
    begin
      Alpha  := 1;
      Itype  := 2;
      Compon := 4;
    end;

    //    ConvertRGBtoBGR
    RGBImage := Pointer(Longword(Tga) + 18);

    if Alpha = 0 then
      for j := 0 to TgaH.Height - 1 do
      begin
        p0 := Pointer(Longword(RGBImage) + j * TgaH.Width * Compon);
        for i := 0 to TgaH.Width - 1 do
        begin
          nbyte := p0[i].b;
          p0[i].b := p0[i].r;
          p0[i].r := nbyte;
        end;
      end
    else
      for j := 0 to TgaH.Height - 1 do 
      begin
        p1 := Pointer(Longword(RGBImage) + j * TgaH.Width * Compon);
        for i := 0 to TgaH.Width - 1 do 
        begin
          nbyte := p1[i].b;
          p1[i].b := p1[i].r;
          p1[i].r := nbyte;
        end;
      end;

    VirtualBufer.Write(Itype, 2);
    VirtualBufer.Write(NMaps, 1);
    Width := TgaH.Width;
    for i := 0 to NMaps - 1 do
    begin
      wSize := Compon * Width;
      VirtualBufer.Write(wSize, 4);
      Width := Width div 2;
      // if Width = 0 then Break;
    end;
    VirtualBufer.Write(NMaps, 1);
    offset := 0;
    Width := TgaH.Width;
    Height := TgaH.Width;
    for i := 0 to NMaps - 1 do
    begin
      VirtualBufer.Write(Offset, 4);
      Offset := offset + Compon * Width * Height;
      Width  := Width div 2;
      Height := Height div 2;
      if Height = 0 then 
        Height := 1;
      //   If Width=0 then break;
    end;

    VirtualBufer.Write(Alpha, 4);
    FullSize := Offset;
    WriteXByte(VirtualBufer, FullSize);
    Width := TgaH.Width;
    Height := TgaH.Width;
    MipXImage := AllocMem(Width * Height * Compon);
    for i := 0 to NMaps - 1 do 
    begin
      GetMipmap(MipXImage, RGBImage, TgaH, Width, Height, Compon);
      VirtualBufer.Write(MipXImage^, Width * Height * Compon);
      if ((Width = 1) and (Height = 1)) then
        Break;
      Width  := Width div 2;
      Height := Height div 2;
    end;
    VirtualBufer.Write(zero, 1);
    FreeMem(MipXImage);
    xSize := VirtualBufer.Position;
    Result := AllocMem(xSize);
    Move(VirtualBufer.Memory^, Result^, xSize);
    VirtualBufer.Free;
  end;
{00 00 00 01 80 00 80 00
01 00 00 00 01 80 01 00
00 01 00 00 00 00 00 00
00 00 80 80 03     }
  {}
begin
  TGABuf := nil;
  Result := false;
  TGABuf := LoadTgaToMemeory(Name);
  if TGABuf = nil then 
  begin
    MessageBox(But.Handle,
      PChar(Format('Can not open %s!!!', [Name])),
      PChar('Error'), MB_OK);
    goto ExitP;
  end;
  Move(TGABuf^, TgaH, SizeOf(tgaH) - 2);
  VBuf := Pointer(Longword(StrArray[XIndex].point) + 3);
  IName := TestByte128(VBuf);
  w := Word(VBuf^);
  Inc(Longword(VBuf), 2);
  h := Word(VBuf^);
  Inc(Longword(VBuf), 2);
  NumMaps := Byte(VBuf^);
  Inc(Longword(VBuf), 2);
  if Word(VBuf^) > 0 then  
    alpha := 32
  else 
    alpha := 24;

  if (TgaH.Width <> w) or (TgaH.Height <> h) or (tgaH.ColorBit <> alpha) then
    if MessageBox(But.Handle,
      PChar(Format('You sure replace Image %dx%dx%d to %dx%dx%d?',
      [w, h, alpha, TgaH.Width, TgaH.Height, tgaH.ColorBit])),
      PChar('Warning!!!'), MB_YESNO) = idNo then
      goto ExitP;

  if (Frac(log2(TgaH.Width)) <> 0) or (Frac(log2(TgaH.Height)) <> 0) then
  begin
    MessageBox(But.Handle,
      PChar(Format('Bad size %dx%d!!!', [TgaH.Width, TgaH.Height])),
      PChar('Error'), MB_OK);
    goto ExitP;
  end;

  if StrArray[XIndex].Update then
    FreeMem(StrArray[XIndex].point);
  StrArray[XIndex].Update := true;
  //StrArray[XIndex].point:=AllocMem(Size);
  StrArray[XIndex].point := MakeXImage(TGABuf, TgaH, Iname, NumMaps, Size);
  StrArray[XIndex].size := Size;
  Result := true;
  ExitP:
  FreeMem(TGABuf);
end;

function ToTime(milisec: Longword): Single;
begin
  Result := (milisec div 60000) + ((milisec mod 60000) div 1000) / 100;
end;


procedure TXom.SelectClassTree(CntrIndx: Integer; var Mesh: TMesh);
var
  x, k, k2, k3, px, i, j, inx: Integer;
  p2: Pointer;
  s, s1: string;
  Matrix: TMatrix;
  IsDummy: Boolean;
  f: Single;
  Pos, Size, RotOrient, Rot, JointOrient: Tver;
  TempMesh: TMesh;
begin
  case StrArray[CntrIndx].Xtype of
    XGraphSet:
    begin
      p2 := StrArray[CntrIndx].point;
      k := TestByte128(p2);
      //                  for x:=1 to k do begin
      Inc(Longword(p2), 16);
      k3 := TestByte128(p2);
      TestByte128(p2);
      SelectClassTree(k3, Mesh);
      if k > 1 then
      begin
        Inc(Longword(p2), 16);
        k3 := TestByte128(p2);
        TestByte128(p2);
        if StrArray[k3].Xtype = XAnimClipLibrary then
        begin
        AnimClips:=nil;
        CurAnimClip:=nil;
        AnimClips:=TAnimClips.Create;
        But.AnimBox.Clear;
        AnimReady := (AnimClips.BuildAnim(k3) > 0);

        But.AnimBox.Items.Assign(AnimClips.FStrings);
        If AnimReady then
        CurAnimClip:=AnimClips.GetItemID(0);
        But.AnimBox.ItemIndex := 0;
        end;
      end;
      //                    end;
    end;
    XMeshDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      if WUM then Inc(Longword(p2));
      k := Byte(p2^);
      Inc(Longword(p2));
      k3 := TestByte128(p2);
      SelectClassTree(k3, Mesh);
      Inc(Longword(p2), 2);// 80 00
    end;
    XSpriteSetDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      Inc(Longword(p2), 1);
      k3 := TestByte128(p2);
      SelectClassTree(k3, Mesh);
    end;
    XDataBank:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      px := TestByte128(p2); // ����� ������
      s := Format(' #%d', [px]);
      //               TreeNode.Text:=TreeNode.Text+s;
      j := 1;
      while Longword(p2) - Longword(StrArray[CntrIndx].point) <
        StrArray[CntrIndx].size do
      begin
        k := TestByte128(p2);
        if k = 0 then 
          Continue;
        Inc(j);
        for x := 1 to k do 
          SelectClassTree(TestByte128(p2), Mesh);
      end;
    end;
    XContainerResourceDetails:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k3 := TestByte128(p2);
      TestByte128(p2);
      SelectClassTree(k3, Mesh);
    end;
    XBitmapDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      if WUM then  Inc(Longword(p2));
      Inc(Longword(p2));
      k3 := TestByte128(p2);
      SelectClassTree(k3, Mesh);
    end;
    XTexFont:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do 
        Inc(Longword(p2), 8);
      k := TestByte128(p2);
      for x := 1 to k do 
        Inc(Longword(p2), 8);
      k := TestByte128(p2);
      for x := 1 to k do 
        SelectClassTree(TestByte128(p2), Mesh);
    end;
    XImage:
    begin
      if But.ClassTree<>nil then begin
      if Integer(But.ClassTree.Selected.Data) = CntrIndx then
      begin
        ImageReady := true;
        //     FormXom.Button3.Enabled:=true;
        LastXImageIndex := CntrIndx;
      end;
      ViewTga(CntrIndx, But.XImage, true, But.XImageLab);
      end;
    end;
    XBlendModeGL:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then
      begin
        Mesh.Attribute.Blend := true;
        Mesh.Attribute.ZBuffer:= false;
        Mesh.Attribute.BlendVal1 := Longword(p2^);
        Mesh.Attribute.BlendVal2 := Longword(Pointer(Longword(p2) + 4)^);
      end;
    end;
    XAlphaTest:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then
      begin
        Mesh.Attribute.AlphaTest := Boolean(Byte(p2^));
        Inc(Longword(p2), 5);
        Mesh.Attribute.AlphaTestVal := Single(Pointer(p2)^);
        if Mesh.Attribute.AlphaTestVal=0 then Mesh.Attribute.AlphaTestVal:=1
      end;
    end;
    XCullFace:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then 
        Mesh.Attribute.CullFace := Boolean(Longword(p2^));
    end;
    XDepthTest:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then 
        Mesh.Attribute.ZBufferFuncVal := Longword(p2^);
    end;
    XZBufferWriteEnable:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then 
        Mesh.Attribute.ZBuffer := Boolean(Byte(p2^));
    end;
    XMaterial:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      if Mesh <> nil then
        with Mesh.Attribute do 
        begin
          Material.use := true;
          Move(p2^, Material.Abi[0], 4 * 4); 
          Inc(Longword(p2), 4 * 4);
          Move(p2^, Material.Dif[0], 4 * 4); 
          Inc(Longword(p2), 4 * 4);
          Move(p2^, Material.Spe[0], 4 * 4);
          Inc(Longword(p2), 4 * 4);
          Move(p2^, Material.Emi[0], 4 * 4);
          Inc(Longword(p2), 4 * 4);
          Move(p2^, Material.Shi, 4);
          //      Material.use:=false;     
        end;
    end;
    XMatrix:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;
      if Mesh <> nil then
      begin
        Mesh.Transform.TransType := TTMatrix;
        Mesh.Transform.TrMatrix := Matrix;
      end;
    end;
    XTexturePlacement2D:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Matrix[1][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[2][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[3][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[4][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      if Mesh <> nil then
      begin
        Mesh.Attribute.Texture2D:=true;
        Mesh.Attribute.T2DMatrix:= Matrix;
      end;
    end;
    XJointTransform:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, RotOrient[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      Move(p2^, Rot[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      Move(p2^, Pos[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      Move(p2^, JointOrient[0], 4 * 3);    
      Inc(Longword(p2), 4 * 3);
      Move(p2^, Size[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      Inc(Longword(p2), 4);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;

      if Mesh <> nil then
      begin
        Mesh.xtype := 'BG';
        Mesh.Transform.Pos := Pos;
        Mesh.Transform.Rot := Rot;
        Mesh.Transform.JointOrient := JointOrient;
        Mesh.Transform.RotOrient := RotOrient;
     {   if System.Pos('root',Mesh.Name)<>-1
        then begin
        Size[0]:=1.0;
        Size[1]:=1.0;
        Size[2]:=1.0;
        end; }
        Mesh.Transform.Size := Size;

        //   MatrixDecompose(Matrix,Mesh.Transform.Pos,Mesh.Transform.Rot,Mesh.Transform.Size);
        Mesh.Transform.TransType := TTJoint;
        Mesh.BoneMatrix := Matrix;
        Mesh.Transform.TrMatrix := GetMatrix2(Pos, Rot,
          JointOrient, RotOrient, Size);
       //      Mesh.Transform.TrMatrix:=Matrix;
        f := 10;
        Mesh.SizeBox.Xmin := -Pos[0] / 2;
        Mesh.SizeBox.Ymin := -Pos[1] / 2;
        Mesh.SizeBox.Zmin := -Pos[2] / 2;
        Mesh.SizeBox.Xmax := Pos[0] / 2;
        Mesh.SizeBox.Ymax := Pos[1] / 2;
        Mesh.SizeBox.Xmax := Pos[2] / 2;
      end;
    end;
    XBone:
    begin
      //    Matrix:= Mesh.Transform.TrMatrix;

      Mesh := TMesh.Create;
      Mesh.xtype := 'BO';
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      //   Mesh.InvBoneMatrix:=MatrixInvert(LastMatrix);
      Move(p2^, Matrix[1][1], 16 * 4); 
      Inc(Longword(p2), 16 * 4);
      //       Mesh.Transform.TransType:=TTMatrix;
      //       Mesh.Transform.TrMatrix:=Matrix;
      Mesh.InvBoneMatrix := Matrix;
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 153);
      Mesh.Name := GetString(TestByte128(p2));
      Mesh.Indx := CntrIndx;
    end;
    XTransform:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Pos[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      Move(p2^, Rot[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      Move(p2^, Size[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      Inc(Longword(p2), 4);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;
      //  CurMatrix:=Matrix;
      if Mesh <> nil then 
      begin
        //       if (Pos[0]<>Matrix[4][1])or (Pos[1]<>Matrix[4][2])or(Pos[2]<>Matrix[4][3])then
        //       begin
        Mesh.Transform.Pos := Pos;
        Mesh.Transform.Rot := Rot;
        Mesh.Transform.Size := Size;
        //        end;
        Mesh.Transform.TrMatrix := GetMatrix(Pos, Rot, Size);
        Mesh.Transform.TransType := TTVector;
      end;
    end;
    XEnvironmentMapShader:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      SelectClassTree(TestByte128(p2), Mesh); // XOglMap
      SelectClassTree(TestByte128(p2), Mesh); // XOglMap
      Inc(Longword(p2), 1);
      SelectClassTree(TestByte128(p2), Mesh);
    end;
    XOglTextureMap:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Inc(Longword(p2), 4);
      Inc(Longword(p2), 4 * 4);
      k3 := TestByte128(p2);
      SelectClassTree(k3, Mesh); // XImage

      //Inc(NTexture);
      if Mesh <> nil then
      begin
        glBindTexture(GL_TEXTURE_2D, CntrIndx);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
          GL_LINEAR_MIPMAP_LINEAR);
        Mesh.Attribute.ZBuffer := Mesh3D.GetTextureGL(StrArray[k3].point);
        Mesh.Attribute.TextureId := k3;
        Mesh.Attribute.Texture := CntrIndx;
      end;

      Inc(Longword(p2), 4);
      Inc(Longword(p2), 2); Inx:=Word(p2^);
      If Mesh<>nil then   begin Mesh.Attribute.TextureClamp:=(Inx=3);
       if  Mesh.Attribute.TextureClamp then begin
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);   // wf
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);  // wf
       end
       else
       begin
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
       end;
      end;
      Inc(Longword(p2), 4 * 5);
      k := TestByte128(p2);
      SelectClassTree(k, Mesh);   // XTexturePlacement2D


    end;
    XInteriorNode:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      Mesh := TMesh.Create;
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'IN';
      if But.ClassTree<>nil then
      if Integer(But.ClassTree.Selected.Data) = CntrIndx then
      begin
        Xom3DReady := true;
      end;
      SetLength(Mesh.Childs, k);
      for x := 0 to k - 1 do 
      begin
        SelectClassTree(TestByte128(p2), Mesh.Childs[x]);
      end;
      p2 := Pointer(Longword(p2) + 4 * 5);
      Mesh.Name := GetString(TestByte128(p2));
    end;
    XMultiTexShader:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Inc(Longword(p2), 1);
      if (Mesh <> nil) then Mesh.Attribute.Multi:=true;
      // color
      SelectClassTree(TestByte128(p2), Mesh);
      // 1 texture
      SelectClassTree(TestByte128(p2), Mesh);
      if (Mesh <> nil) then Mesh.Attribute.MultiT1:=Mesh.Attribute.Texture;
      // 2 texture
      SelectClassTree(TestByte128(p2), Mesh);
      if (Mesh <> nil) then Mesh.Attribute.MultiT2:=Mesh.Attribute.Texture;
      Inc(Longword(p2), 4);
      s := getString(TestByte128(p2));
      if (Mesh <> nil) and Mesh.Attribute.Material.use then
        Mesh.Attribute.Material.Name := s;
    end;
    XSimpleShader:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do 
        SelectClassTree(TestByte128(p2), Mesh);
      k := TestByte128(p2);
      for x := 1 to k do 
        SelectClassTree(TestByte128(p2), Mesh);
      Inc(Longword(p2), 4);
      s := getString(TestByte128(p2));
      if (Mesh <> nil) and Mesh.Attribute.Material.use then
        Mesh.Attribute.Material.Name := s;
    end;
    XAnimClipLibrary:
        begin

      //  SelectClassTree(k3, Mesh);
    //    AnimReady:=true;
        end;
    //XBone:
    XShape:
    begin
      Mesh := TMesh.Create;
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'SH';
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      x := Byte(p2^);
      Inc(Longword(p2), 4);
      SelectClassTree(TestByte128(p2), Mesh);
      SelectClassTree(TestByte128(p2), Mesh);
      Inc(Longword(p2), 5);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := Format('%s', [GetString(TestByte128(p2))]);
      Mesh.Name := s;
    end;
    XSkinShape:
    begin
      Mesh := TMesh.Create;
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'SS';
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      SetLength(Mesh.Bones, k);
      for x := 0 to k - 1 do
        Mesh.Bones[x] := Mesh3D.GetMeshOfID(TestByte128(p2));  //Bones;
      Inc(Longword(p2), 4);
      SelectClassTree(TestByte128(p2), Mesh); //XSimpleShader
      SelectClassTree(TestByte128(p2), Mesh); //XIndexedTriangleSet
      Inc(Longword(p2), 5);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := Format('%s', [GetString(TestByte128(p2))]);
      Mesh.Name := s;
    end;
    XGroup:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
    //
      Mesh := TMesh.Create;
      TempMesh := Mesh3D.GetMeshOfID(CntrIndx);
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'GR';
      if TempMesh<>nil then begin
      Mesh.Name:=TempMesh.Name;
      Mesh.IsLink:=true;
      end else begin
      inx := TestByte128(p2);
      if (inx <> 0) then
        SelectClassTree(inx, Mesh) 
      else // Matrix
        Mesh.xtype := 'GS';
      k := TestByte128(p2);
      SetLength(Mesh.Childs, k);
      //  IsDummy:=true;
      for x := 0 to k - 1 do 
      begin
        inx := TestByte128(p2);
        SelectClassTree(inx, Mesh.Childs[x]);
        if (x = 0) and (Mesh.Childs[x] <> nil) and (Mesh.Childs[x].XType = 'GS') then
          Mesh.xtype := 'GO';

        if (x>0) and (Mesh.Childs[x]<>nil) and (Mesh.Childs[x].XType='BM')  then
                Mesh.Childs[x-1].Attribute.ZBuffer:=false;

        //    if Mesh.Childs[x].XType='SH' then
        //           IsDummy:=false;
      end;
      //   if IsDummy then Mesh.xtype:='DM';
      px := Longword(p2);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      Mesh.Name := s;
      end;
    end;
    XChildSelector:
    begin
      if Mesh <> nil then 
        Mesh.XType := 'CS';
    end;
    XBinModifier:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 4);
      Mesh := TMesh.Create;
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'BM';
      k := TestByte128(p2);
      SelectClassTree(TestByte128(p2), Mesh);  //matrix
      k := TestByte128(p2);

      SetLength(Mesh.Childs, k);
      for x := 0 to k - 1 do
      begin
        inx := TestByte128(p2);
        SelectClassTree(inx, Mesh.Childs[x]);
       //Mesh.Childs[x].Attribute.ZBuffer:=false;
      end;
      px := Longword(p2);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      Mesh.Name := s;
    end;
    XSkin:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Mesh := TMesh.Create;
      Mesh.Indx := CntrIndx;
      Mesh.xtype := 'SK';
      SetLength(Mesh.Childs, 1);
      SelectClassTree(TestByte128(p2), Mesh.Childs[0]);
      k := TestByte128(p2);
      SetLength(Mesh.Childs, k + 1);
      for x := 1 to k do 
      begin
        inx := TestByte128(p2);
        SelectClassTree(inx, Mesh.Childs[x]);
      end;
      px := Longword(p2);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      Mesh.Name := s;
    end;
    XCollisionData:
    begin
      p2 := Pointer(Longword(StrArray[CntrIndx].Point) + 5);
      k := TestByte128(p2); 
      k3 := 0;
      if k <> 0 then 
        k3 := TestByte128(p2);
      SelectClassTree(k3, Mesh);
    end;
    XCollisionGeometry:
    begin
      Mesh3D.glBuildCollGeo(CntrIndx);
    end;
    XIndexedTriangleSet:
    begin
      if Mesh = nil then
      begin
        Mesh := TMesh.Create;
        Mesh.Indx := CntrIndx;
        Mesh.XType := 'OB';
        Mesh.Name := Format('XomMesh#%d', [CntrIndx]);
        //     FormXom.Button4.Enabled:=true;
      end;
      Mesh.glBuildTriangleGeo(CntrIndx);
      if Mesh.XType = 'SS' then
        Mesh.RVert := Copy(Mesh.Vert);
    end;
    XIndexedTriangleStripSet:
    begin
      if Mesh = nil then 
      begin
        Mesh := TMesh.Create;
        Mesh.Indx := CntrIndx;
        Mesh.XType := 'OB';
        Mesh.Name := Format('XomMesh#%d', [CntrIndx]);
        //      FormXom.Button4.Enabled:=true;
      end;
      Mesh.glBuildTriangleGeo(CntrIndx);
      if Mesh.XType = 'SS' then
        Mesh.RVert := Copy(Mesh.Vert);
    end;
  end;
end;

function TXom.AddClassTree(CntrIndx: Integer; TreeView: TTreeView;
  TreeNode: TTreeNode; HideChild: Boolean = false): TTreeNode;
var
  x, k, k4, k3, k2, px, i, j, inx, inx1, inx2, w, h, Isize, Imap, Bsize: Integer;
  val:single;
  p2: Pointer;
  s, s1: string;
  Matrix: TMatrix;
  KeyFrame: TKeyFrame;
  Pos, Size, Rot: Tver;
  AnimV: Tpnt;
  AnimType: Cardinal;
  ftime: Single;
  ExpAnim: Boolean;
  StrList: TStringList;
  TempNode, TempNode0, TempNode2, TempNode3: TTreeNode;
  //ArrNode:array of TTreeNode;
  procedure AddNode(Name: string;icon:integer=0);
  begin
    TreeNode := TreeView.Items.AddChild(TreeNode, Name);
    TreeNode.Data := Pointer(CntrIndx);
    TreeNode.ImageIndex := icon;
    TreeNode.SelectedIndex := icon;
  end;

      function AddChildF(TN:TTreeNode;var Pnt:pointer;StrVal:String;XT:XValTypes):TTreeNode;
      var
      vi:Integer;
      vu:Smallint;
      vf:Single;
      vb:boolean;
      vs:string;
      vc:Cardinal;
      vby:byte;
      CntrVal:TCntrVal;
      TmpNode:TTreeNode;
      begin
      CntrVal:=TCntrVal.Create;
      CntrVal.IdName:=StrVal;
      CntrVal.Point:=Pnt;
      CntrVal.XType:=XT;
      CntrVal.Cntr:=CntrIndx;
      case XT of
      XBool:    begin
                vb:=Boolean(byte(Pnt^));  Inc(Longword(Pnt));
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %s', [StrVal,BoolToStr(vb, true)]));
                end;
      XByte:    begin
                vby:=byte(Pnt^);  Inc(Longword(Pnt));
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %d', [StrVal,vby]));
                end;
      XString:  begin
                vs:=GetString(TestByte128(Pnt));
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %s', [StrVal,vs]));
                end;
      XFloat:   begin
                vf:=single(Pnt^);  Inc(Longword(Pnt), 4);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %f', [StrVal,vf]));
                end;
      XFFloat:   begin
                vf:=single(Pnt^);  Inc(Longword(Pnt), 4);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %.6f', [StrVal,vf]));
                end;
      XInt:    begin
                vi:=integer(Pnt^);  Inc(Longword(Pnt), 4);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %d', [StrVal,vi]));
                end;
      XCode:    begin
                vc:=integer(Pnt^);  Inc(Longword(Pnt), 4);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = 0x%x', [StrVal,vc]));
                end;
      XVector:  Begin
                vc:=Longword(Pnt);  Inc(Longword(Pnt), 4*3);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = <x:%f y:%f z:%f>',
                [StrVal, Single(Pointer(vc)^), Single(Pointer(vc + 4)^), Single(Pointer(vc + 8)^)]));
                end;
      XUInt:    begin
                vu:=SmallInt(Pnt^);  Inc(Longword(Pnt), 2);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = %d', [StrVal,vu]));
                end;
      XPoint:   begin
                vc:=Longword(Pnt);  Inc(Longword(p2), 4*2);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = <x:%f y:%f>',
                [StrVal, Single(Pointer(vc)^), Single(Pointer(vc + 4)^)]));
                end;
      XFColor:   begin
                vc:=Longword(Pnt);  Inc(Longword(p2), 4*3);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = <r:%f g:%f b:%f>',
                [StrVal, Single(Pointer(vc)^), Single(Pointer(vc + 4)^), Single(Pointer(vc + 8)^)]));
                end;
      XColor:   begin
                vc:=Longword(Pnt);  Inc(Longword(p2), 1*3);
                TmpNode:=TreeView.Items.AddChild(TN,Format('%s = <r:%d g:%d b:%d a:%d>',
                [StrVal, byte(Pointer(vc)^), byte(Pointer(vc + 1)^), byte(Pointer(vc + 2)^), byte(Pointer(vc + 3)^)]));
                end;
      end;
      TmpNode.Data:= CntrVal;
      result:=TmpNode;
      end;

    procedure BaseWeapon;
    begin
      AddChildF(TreeNode,p2,'DisplayName',XString);//
      if not WUM then begin
      AddChildF(TreeNode,p2,'AnimDraw',Xstring);//
      AddChildF(TreeNode,p2,'AnimAim',Xstring);//
      AddChildF(TreeNode,p2,'AnimFire',Xstring);//
      AddChildF(TreeNode,p2,'AnimHolding',Xstring);//
      AddChildF(TreeNode,p2,'AnimEndFire',Xstring );//
      end;
      AddChildF(TreeNode,p2,'WeaponGraphicsResourceID',Xstring);//
      AddChildF(TreeNode,p2,'WeaponType',XInt);//
      AddChildF(TreeNode,p2,'DefaultPreference',XInt);//
      AddChildF(TreeNode,p2,'CurrentPreference',XInt);//
      AddChildF(TreeNode,p2,'LaunchDelay',XInt);//?
      AddChildF(TreeNode,p2,'PostLaunchDelay',XInt);//?
      AddChildF(TreeNode,p2,'FirstPersonOffset',XVector);//
      AddChildF(TreeNode,p2,'FirstPersonScale',XVector);//
      if not WUM then begin
      AddChildF(TreeNode,p2,'FirstPersonFiringParticleEffect',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonDrawAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonWindUpAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonFireAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonWindDownAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonReloadAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHideAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonIdleAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandDrawAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandWindUpAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandFireAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandWindDownAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandReloadAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandHideAnim',Xstring);//
      AddChildF(TreeNode,p2,'FirstPersonHandIdleAnim',Xstring);//
      AddChildF(TreeNode,p2,'DisplayInFirstPerson',Xbool);//
      AddChildF(TreeNode,p2,'CanBeFiredWhenWormMoving',Xbool);//
      AddChildF(TreeNode,p2,'RumbleLight',XByte);//?
      AddChildF(TreeNode,p2,'RumbleHeavy',XByte);//?
      end;
      if WUM then begin
      AddChildF(TreeNode,p2,'FirstPersonFiringParticleEffect',Xstring);//
      AddChildF(TreeNode,p2,'HoldParticleFX',Xstring);//
      AddChildF(TreeNode,p2,'DisplayInFirstPerson',Xbool);//
      AddChildF(TreeNode,p2,'CanBeFiredWhenWormMoving',Xbool);//
      AddChildF(TreeNode,p2,'RumbleLight',XByte);//
      AddChildF(TreeNode,p2,'RumbleHeavy',XByte);//
      AddChildF(TreeNode,p2,'CanBeUsedWhenTailNailed',Xbool);//

      AddChildF(TreeNode,p2,'RetreatTimeOverride',Xint);//

      AddChildF(TreeNode,p2,'WXAnimDraw',Xstring);//
      AddChildF(TreeNode,p2,'WXAnimAim',Xstring);//
      AddChildF(TreeNode,p2,'WXAnimFire',Xstring);//
      AddChildF(TreeNode,p2,'WXAnimHolding',Xstring);//
      AddChildF(TreeNode,p2,'WXAnimEndFire',Xstring );//
      AddChildF(TreeNode,p2,'WXAnimTaunt',Xstring );//
      AddChildF(TreeNode,p2,'WXAnimTargetSelected',Xstring );//
      AddChildF(TreeNode,p2,'HoldLoopSfx',Xstring );//
      AddChildF(TreeNode,p2,'EquipSfx',Xstring );//
      end;
    end;

    procedure PayloadWeapon;
    begin
      AddChildF(TreeNode,p2,'IsAimedWeapon',XBool);
      AddChildF(TreeNode,p2,'IsPoweredWeapon',XBool);
      AddChildF(TreeNode,p2,'IsTargetingWeapon',XBool);
      if WUM then begin
      AddChildF(TreeNode,p2,'IsControlledBomber',XBool);
      end;
      AddChildF(TreeNode,p2,'IsBomberWeapon',XBool);
      AddChildF(TreeNode,p2,'IsDirectionalWeapon',XBool);
      AddChildF(TreeNode,p2,'IsHoming',XBool);
      AddChildF(TreeNode,p2,'IsLowGravity',XBool);
      AddChildF(TreeNode,p2,'IsLaunchedFromWorm',XBool);
      AddChildF(TreeNode,p2,'HasAdjustableFuse',XBool);
      AddChildF(TreeNode,p2,'HasAdjustableBounce',XBool);
      AddChildF(TreeNode,p2,'HasAdjustableHerd',XBool);
      AddChildF(TreeNode,p2,'IsAffectedByGravity',XBool);
      AddChildF(TreeNode,p2,'IsAffectedByWind',XBool);
      if WUM then begin
               AddChildF(TreeNode,p2,'EndTurnImmediate',XBool);
               AddChildF(TreeNode,p2,'UseParabolicRetical',XBool);
      end;
      AddChildF(TreeNode,p2,'ColliderFlags',XInt);
      x:=byte(p2^);  Inc(Longword(p2)); // ����
      if x=1 then
        AddChildF(TreeNode,p2,'CameraID',XString);
      AddChildF(TreeNode,p2,'PayloadGraphicsResourceID',XString);
      if WUM then begin
      AddChildF(TreeNode,p2,'Payload2ndGraphicsResourceID',XString);
      end;
      AddChildF(TreeNode,p2,'Scale',XFloat);
      AddChildF(TreeNode,p2,'Radius',XFloat);

      AddChildF(TreeNode,p2,'AnimTravel',XString);
      AddChildF(TreeNode,p2,'AnimSmallJump',XString);
      AddChildF(TreeNode,p2,'AnimBigJump',XString);
      AddChildF(TreeNode,p2,'AnimArm',XString);
      AddChildF(TreeNode,p2,'AnimSplashdown',XString);
      AddChildF(TreeNode,p2,'AnimSink',XString);
      if WUM then begin
               AddChildF(TreeNode,p2,'AnimIntermediate',XString);
               AddChildF(TreeNode,p2,'AnimImpact',XString);
      end;
      AddChildF(TreeNode,p2,'DirectionBlend',XFloat);
      if WUM then begin
            AddChildF(TreeNode,p2,'FuseTimerGraphicOffset',XFloat);
            AddChildF(TreeNode,p2,'FuseTimerScale',XFloat);
      end;
      AddChildF(TreeNode,p2,'BasePower',XFloat);
      AddChildF(TreeNode,p2,'MaxPower',XFloat);
      AddChildF(TreeNode,p2,'MinTerminalVelocity',XFloat);
      AddChildF(TreeNode,p2,'MaxTerminalVelocity',XFloat);
      AddChildF(TreeNode,p2,'LogicalLaunchZOffset',XFloat);
      if WUM then begin
            AddChildF(TreeNode,p2,'LogicalLaunchYOffset',XFloat);
      end;
      AddChildF(TreeNode,p2,'OrientationOption',XInt);
      AddChildF(TreeNode,p2,'SpinSpeed',XFloat);
      AddChildF(TreeNode,p2,'InterPayloadDelay',XFloat);
      if WUM then begin
            AddChildF(TreeNode,p2,'MinAimAngle',XFloat);
            AddChildF(TreeNode,p2,'MaxAimAngle',XFloat);
      end;
      AddChildF(TreeNode,p2,'DetonatesOnLandImpact',XBool);
      AddChildF(TreeNode,p2,'DetonatesOnExpiry',XBool);
      AddChildF(TreeNode,p2,'DetonatesOnObjectImpact',XBool);
      AddChildF(TreeNode,p2,'DetonatesOnWormImpact',XBool);
      AddChildF(TreeNode,p2,'DetonatesAtRest',XBool);
      AddChildF(TreeNode,p2,'DetonatesOnFirePress',XBool);
      AddChildF(TreeNode,p2,'DetonatesWhenCantJump',XBool);
      if WUM then begin
            AddChildF(TreeNode,p2,'DetonateMultiEffect',XInt);
            AddChildF(TreeNode,p2,'WormCollideResponse',XFloat);
      end;
      AddChildF(TreeNode,p2,'WormDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'WormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'LandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'ImpulseRadius',XFloat);
      AddChildF(TreeNode,p2,'ImpulseOffset',XFloat);
      AddChildF(TreeNode,p2,'Mass',XFloat);
      if WUM then begin
            AddChildF(TreeNode,p2,'WormImpactDamage',XFloat);
      end;
      AddChildF(TreeNode,p2,'MaxPowerUp',XFloat);
      AddChildF(TreeNode,p2,'TangentialMinBounceDamping',XFloat);
      AddChildF(TreeNode,p2,'ParallelMinBounceDamping',XFloat);
      AddChildF(TreeNode,p2,'TangentialMaxBounceDamping',XFloat);
      AddChildF(TreeNode,p2,'ParallelMaxBounceDamping',XFloat);
      AddChildF(TreeNode,p2,'SkimsOnWater',XBool);
      AddChildF(TreeNode,p2,'MinSpeedForSkim',XFloat);
      AddChildF(TreeNode,p2,'MaxAngleForSkim',XFloat);
      AddChildF(TreeNode,p2,'SkimDamping',XVector);
      AddChildF(TreeNode,p2,'SinkDepth',XFloat); ///////
      if WUM then begin
        AddChildF(TreeNode,p2,'NumStrikeBombs',XInt);
      end;
      AddChildF(TreeNode,p2,'NumBomblets',XInt);
      AddChildF(TreeNode,p2,'BombletMaxConeAngle',XFloat);
      AddChildF(TreeNode,p2,'BombletMaxSpeed',XFloat);
      AddChildF(TreeNode,p2,'BombletMinSpeed',XFloat);
      AddChildF(TreeNode,p2,'BombletWeaponName',XString);
      AddChildF(TreeNode,p2,'FxLocator',XString);
      AddChildF(TreeNode,p2,'ArielFx',XString);
      AddChildF(TreeNode,p2,'DetonationFx',XString);
      AddChildF(TreeNode,p2,'DetonationSfx',XString);
      AddChildF(TreeNode,p2,'ExpiryFx',XString);
      AddChildF(TreeNode,p2,'SplashFx',XString);
      AddChildF(TreeNode,p2,'SplishFx',XString);
      AddChildF(TreeNode,p2,'SinkingFx',XString);
      AddChildF(TreeNode,p2,'BounceFx',XString);
      AddChildF(TreeNode,p2,'StopFxAtRest',XBool);  
      AddChildF(TreeNode,p2,'BounceSfx',XString );
      AddChildF(TreeNode,p2,'PreDetonationSfx',XString);
      AddChildF(TreeNode,p2,'ArmSfx1Shot',XString);
      AddChildF(TreeNode,p2,'ArmSfxLoop',XString);
      AddChildF(TreeNode,p2,'LaunchSfx',XString );
      AddChildF(TreeNode,p2,'LoopSfx',XString);
      AddChildF(TreeNode,p2,'BigJumpSfx',XString);
      AddChildF(TreeNode,p2,'WalkSfx',XString);
      AddChildF(TreeNode,p2,'TrailBitmap',XString);
      AddChildF(TreeNode,p2,'TrailLocator1',XString);
      AddChildF(TreeNode,p2,'TrailLocator2',XString);
      AddChildF(TreeNode,p2,'TrailLength',XInt);
      AddChildF(TreeNode,p2,'AttachedMesh',XString);
      AddChildF(TreeNode,p2,'AttachedMeshScale',XFloat);
      AddChildF(TreeNode,p2,'StartsArmed',Xbool);
      if WUM then begin
        AddChildF(TreeNode,p2,'ArmOnImpact',Xbool);
      end;
      if not WUM then begin
        AddChildF(TreeNode,p2,'RetreatTimeOverride',XInt);//?
      end;
      AddChildF(TreeNode,p2,'ArmingCourtesyTime',XInt);//?
      AddChildF(TreeNode,p2,'PreDetonationTime',XInt);//?
      AddChildF(TreeNode,p2,'ArmingRadius',XFloat);//
      AddChildF(TreeNode,p2,'LifeTime',XInt);//?

      AddChildF(TreeNode,p2,'IsFuseDisplayed',Xbool);//
      BaseWeapon;
    end;

begin
  if (CntrIndx > 0) and (not TreeArray[CntrIndx]) then
  begin
    Inc(TreeCount);
    TreeArray[CntrIndx]   := true;
    But.TreeLabel.Caption := Format('%.1f %%', [(TreeCount) / Conteiners * 100]);
    But.TreeLabel.Repaint;
  end;

  case StrArray[CntrIndx].Xtype of
    XGraphSet:
    begin
      AddNode(Format('XGraphSet [%d]', [CntrIndx]),4);

      p2 := StrArray[CntrIndx].point;
      k := TestByte128(p2);
      for x := 1 to k do 
      begin
        Inc(Longword(p2), 16);
        k3 := TestByte128(p2);
        TempNode := TreeView.Items.AddChild(TreeNode,
          Format('<%s>', [GetString(TestByte128(p2))]));
        AddClassTree(k3, TreeView, TempNode);
      end;
    end;
    XXomInfoNode:
    begin
      AddNode(Format('XXomInfoNode [%d] ', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
    end;
    W3DTemplateSet:
    begin
      AddNode(Format('W3DTemplateSet [%d] ', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 12);
      Inc(Longword(p2), 8);
      s := GetString(TestByte128(p2));
      TreeNode.Text := TreeNode.Text + s;
    end;
    XSpriteSetDescriptor:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 1;
        TreeNode.SelectedIndex := 1;
      end;
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      Inc(Longword(p2), 1);
      AddNode(Format('XBaseResourceDescriptor::XSpriteSetDescriptor [%d] "%s"',
        [CntrIndx, s]),20);
      k3 := TestByte128(p2);
      AddClassTree(k3, TreeView, TreeNode);
    end;
    XNullDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      if WUM then  Inc(Longword(p2));
      k := TestByte128(p2);
      AddNode(Format('XBaseResourceDescriptor::XNullDescriptor [%d] "%s"',
        [CntrIndx, s]),20);

    end;
    XTextDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      AddNode(Format('XBaseResourceDescriptor::XTextDescriptor [%d] "%s"',
        [CntrIndx, s]),20);
      if WUM then  Inc(Longword(p2));  
      k := TestByte128(p2); //1  number
      // AddClassTree(k3,TreeView,TempNode);
      k3 := TestByte128(p2); //2  link
      AddClassTree(k3, TreeView, TreeNode);
      k := Word(p2^);       // size
      Inc(Longword(p2), 4); // 1

      TempNode := TreeView.Items.AddChild(TreeNode,
        Format(' #%d', [k]));
      for x := 1 to k do
      begin
        s1 := Format('[%d %d %d] ',
          [Word(p2^), Word(Pointer(Longword(p2) + 2)^), Word(Pointer(Longword(p2) + 4)^)]);
        Inc(Longword(p2), 6); // num, wchar, wchar
        TreeView.Items.AddChild(TempNode, s1);
      end;
    end;
    XMeshDescriptor:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 2;
        TreeNode.SelectedIndex := 2;
      end;
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      AddNode(Format('XBaseResourceDescriptor::XMeshDescriptor [%d] "%s"',
        [CntrIndx, s]),20);
      if WUM then Inc(Longword(p2));
      k := Byte(p2^);
      Inc(Longword(p2));
      k3 := TestByte128(p2);
      AddClassTree(k3, TreeView, TreeNode);
      Inc(Longword(p2), 2);// 80 00
    end;
    XCustomDescriptor:
    begin
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      AddNode(Format('XBaseResourceDescriptor::XCustomDescriptor [%d] "%s"',
        [CntrIndx, s]),20);
      if WUM then  Inc(Longword(p2));
      Inc(Longword(p2), 3);
    end;
    XBitmapDescriptor:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 3;
        TreeNode.SelectedIndex := 3;
      end;
      AddNode(Format('XBaseResourceDescriptor::XBitmapDescriptor [%d]',
        [CntrIndx]),20);
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));
      if WUM then  Inc(Longword(p2));
      Inc(Longword(p2));
      k3 := TestByte128(p2);
      s := Format(' "%s" [%dx%d]',
        [s, Word(p2^), Word(Pointer(Longword(p2) + 2)^)]);
      TreeNode.Text := TreeNode.Text + s;
      AddClassTree(k3, TreeView, TreeNode);
    end;
    TeamDataColective:
    begin
    AddNode(Format('TeamDataColective [%d]', [CntrIndx]));
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
        k := TestByte128(p2);
      TempNode:=TreeView.Items.AddChild(TreeNode,
        Format('Teams[%d]', [k]));
        for x := 1 to k do
          AddClassTree(TestByte128(p2), TreeView, TempNode);

        k := TestByte128(p2);
      TempNode:=TreeView.Items.AddChild(TreeNode,
        Format('HighScore[%d]', [k]));
        for x := 1 to k do
          AddClassTree(TestByte128(p2), TreeView, TempNode);
    end;
    SchemeData:
    begin
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
      s := GetString(TestByte128(p2));
      AddNode(Format('SchemeData [%d] "%s"', [CntrIndx, s]));
      Inc(Longword(p2), 2);
      for x := 1 to 54 do begin
          TempNode:=AddClassTree(TestByte128(p2), TreeView, TreeNode);
          TempNode.Text:=format('%d.%s',[x,TempNode.Text]);
          end;
    end;
    LockedContainer:
    begin
      AddNode(Format('LockedContainer [%d]', [CntrIndx]));
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
      AddChildF(TreeNode,p2,'Lock',XBool);
      AddChildF(TreeNode,p2,'LockKey',XString);
      AddChildF(TreeNode,p2,'Type',XInt);
    end;
    WeaponSettingsData:
    begin
    AddNode(Format('WeaponSettingsData [%d]', [CntrIndx]));
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
      AddChildF(TreeNode,p2,'Weapon',XInt);
      AddChildF(TreeNode,p2,'Crates',XInt);
      AddChildF(TreeNode,p2,'Delay',XInt);
      AddChildF(TreeNode,p2,'?',XInt);
    end;
    SchemeColective:
    begin
    AddNode(Format('SchemeColective [%d]', [CntrIndx]));
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
        k := TestByte128(p2);
        for x := 1 to k do
          AddClassTree(TestByte128(p2), TreeView, TreeNode);
    end;
    GSProfileList:
    begin
    AddNode(Format('GSProfileList [%d]', [CntrIndx]));
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
        k := TestByte128(p2);
        for x := 1 to k do
          AddClassTree(TestByte128(p2), TreeView, TreeNode);
    end;
    LandFrameStore: 
    begin
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
      s := GetString(TestByte128(p2));
      AddNode(Format('LandFrameStore [%d] "%s"', [CntrIndx, s]));
      Inc(Longword(p2), 3 * 3 * 4);

      if (Byte(Pointer(Longword(p2) + $4c)^) <> 0) then
      begin
        Inc(Longword(p2), 16 * 4 + 12);
        Inc(Longword(p2), TestByte128(p2) * 8);
        Inc(Longword(p2), TestByte128(p2) * 8);
        Inc(Longword(p2), 3);
        Inc(Longword(p2), TestByte128(p2) * 4);

        Inc(Longword(p2), 12);
        Inc(Longword(p2), TestByte128(p2) * 4);

        k := TestByte128(p2);
        for x := 1 to k do
          AddClassTree(TestByte128(p2), TreeView, TreeNode);
      end
      else
        Inc(Longword(p2), $60);

      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
    end;
    DetailEntityStore:
    begin
      p2 := StrArray[CntrIndx].point;
      Inc(Longword(p2), 3);
      s := GetString(TestByte128(p2));
      AddNode(Format('DetailEntityStore [%d] "%s"', [CntrIndx, s]));
      s := Format('Name = %s', [s]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Lib = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    PC_LandChunk:
      AddNode(Format('LandChunk::PC_LandChunk [%d]', [CntrIndx]));
    PC_LandFrame:
    begin
      AddNode(Format('LandFrame::PC_LandFrame [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k3 := TestByte128(p2);
      Inc(Longword(p2), k3);
      k3 := TestByte128(p2);
      Inc(Longword(p2), k3);
      Inc(Longword(p2), 2);    //00 00
      Inc(Longword(p2), 4);    // 3d 00 6c 00
      Inc(Longword(p2), 368);  // pos?
      k3 := TestByte128(p2);   // layers 1
      Inc(Longword(p2), k3 * 2 * 4);
      k3 := TestByte128(p2); // layers 2
      Inc(Longword(p2), k3 * 2 * 4);
      k3 := TestByte128(p2); // ?
      Inc(Longword(p2), k3 * 4);
      Inc(Longword(p2), 4);
      k3 := TestByte128(p2); // ?
      Inc(Longword(p2), k3 * 4);
      k3 := TestByte128(p2); // ?
      Inc(Longword(p2), k3 * 4);
      // ?????
      k3 := TestByte128(p2); // ?
      Inc(Longword(p2), (k3 - 1) * 4);
      Inc(Longword(p2), 16); //???
      Inc(Longword(p2), 4);  // ff ff ff ff
      k3 := TestByte128(p2); // childs
      for x := 1 to k3 do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 4 * 4); // coord4
      Inc(Longword(p2), 4);     // zero
      s := Format(' "%s"', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XDataBank:
    begin
      AddNode(Format('XDataBank [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      px := TestByte128(p2); // ����� ������
      s := Format(' #%d', [px]);
      TreeNode.Text := TreeNode.Text + s;
      j := 1;
      while Longword(p2) - Longword(StrArray[CntrIndx].point) <
        StrArray[CntrIndx].size do
      begin
        k := TestByte128(p2);
        if k = 0 then 
          Continue;
        TempNode := TreeView.Items.AddChild(TreeNode,
          Format(' #%d', [j]));
        Inc(j);
        for x := 1 to k do 
          AddClassTree(TestByte128(p2), TreeView, TempNode);
      end;
    end;   
    XContainerResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XContainerResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k3 := TestByte128(p2);
      s := GetString(TestByte128(p2));
      s := Format(' "%s"', [s]);
      TreeNode.Text := TreeNode.Text + s;
      AddClassTree(k3, TreeView, TreeNode);
      k3 := TestByte128(p2);
      TreeView.Items.AddChild(TreeNode, Format('%d', [k3]));
    end;
    LevelDetails:
    begin
      AddNode(Format('LevelDetails [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      s := Format('Frontend_Name = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Level_ScriptName = %s',
        [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Theme = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('MaterialFile = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Level_FileName = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('TimeOfDay = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Anim = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Level_Type = %d', [TestByte128(p2)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(Longword(p2), 3);
      s := Format('Frontend_Briefing = %s',
        [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Frontend_Image = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('LevelNumber = %d', [TestByte128(p2)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(Longword(p2), 3);
      s := Format('Lock = %s', [GetString(TestByte128(p2))]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Type = %d', [TestByte128(p2)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XVectorResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XVectorResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3 + 12);
      s := GetString(TestByte128(p2));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,XVector);
    end;
    XColorResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XColorResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3 + 4);
      s := GetString(TestByte128(p2));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,XColor);
    end;
    XFloatResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XFloatResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 7);
      s := GetString(TestByte128(p2));
      p2:=Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,Xfloat);
    end;
    XIntResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XIntResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 7);
      s := GetString(TestByte128(p2));
      p2:=Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,XInt);
    end;
    XUintResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XUintResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 7);
      s := GetString(TestByte128(p2));
      p2:=Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,XUInt);
    end;
    XExportAttributeString:
    begin
      AddNode(Format('XExportAttribute::XExportAttributeString [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s1 := GetString(TestByte128(p2));
      s := GetString(TestByte128(p2));
      s := Format('"%s" = "%s"', [s, s1]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XStringResourceDetails:
    begin
      AddNode(Format('XResourceDetails::XStringResourceDetails [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s1 := GetString(TestByte128(p2));
      s := GetString(TestByte128(p2));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,s,XString);
    end;
    GSProfile:
    begin
      AddNode(Format('GSProfile [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,'Nick',XString);
      AddChildF(TreeNode,p2,'E-mail',XString);
    end;
    HighScoreData:
    begin
      AddNode(Format('HighScoreData [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := GetString(TestByte128(p2));
      s1 := Format('gold = %s | %.2f ', [s, ToTime(Longword(p2^))]);
      TreeView.Items.AddChild(TreeNode, s1);
      p2 := Pointer(Longword(p2) + 4);
      s := GetString(TestByte128(p2));
      s1 := Format('silver = %s | %.2f ', [s, ToTime(Longword(p2^))]);
      TreeView.Items.AddChild(TreeNode, s1);
      p2 := Pointer(Longword(p2) + 4);
      s := GetString(TestByte128(p2));
      s1 := Format('bronze = %s | %.2f ', [s, ToTime(Longword(p2^))]);
      TreeView.Items.AddChild(TreeNode, s1);
    end;
    FlyingPayloadWeaponPropertiesCo:
    begin
      AddNode(Format('XContainer::FlyingPayloadWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,'MaxPitchSpeed',XFFloat);
      AddChildF(TreeNode,p2,'PitchAcceleration',XFFloat);
      AddChildF(TreeNode,p2,'Inertia',XFloat);
      AddChildF(TreeNode,p2,'MaxYawSpeed',XFFloat);
      AddChildF(TreeNode,p2,'YawAcceleration',XFFloat);
      AddChildF(TreeNode,p2,'MaxRollSpeed',XFFloat);
      AddChildF(TreeNode,p2,'RollAcceleration',XFFloat);
      AddChildF(TreeNode,p2,'FlyingSpeed',XFloat);
      AddChildF(TreeNode,p2,'MaxWorldYawSpeed',XFFloat);
      AddChildF(TreeNode,p2,'BlendTowardsHorizontal',XFloat);
      AddChildF(TreeNode,p2,'BlendTowardsVertical',XFFloat);
      AddChildF(TreeNode,p2,'MaxAutoRollSpeed',XFFloat);
      AddChildF(TreeNode,p2,'AutoRollAcceleration',XFFloat);
      AddChildF(TreeNode,p2,'AutoRollDelay',XInt);
      AddChildF(TreeNode,p2,'YawAnimSpeed',XFFloat);
      AddChildF(TreeNode,p2,'RollAnimSpeed',XFFloat);
      AddChildF(TreeNode,p2,'AnimYaw',XString);
      AddChildF(TreeNode,p2,'AnimRoll',XString);
      AddChildF(TreeNode,p2,'FlyingGraphicsResourceID',XString);
      AddChildF(TreeNode,p2,'FlyingLaunchSfx',XString);
      AddChildF(TreeNode,p2,'FlyingLoopSfx',XString);
      AddChildF(TreeNode,p2,'AnimFly',XString);
      AddChildF(TreeNode,p2,'AnimFall',XString);

      AddChildF(TreeNode,p2,'SmallJumpHorizontalSpeed',XFloat);
      AddChildF(TreeNode,p2,'SmallJumpMinVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'SmallJumpMaxVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpHorizontalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpMinVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpMaxVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'MaxDrop',XFloat);
      AddChildF(TreeNode,p2,'ReturnProbability',XFloat);
      AddChildF(TreeNode,p2,'MinTimeForSafeJump',XInt);
      PayloadWeapon;
    end;
    JumpingPayloadWeaponPropertiesC:
    begin
      AddNode(Format('XContainer::JumpingPayloadWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'SmallJumpHorizontalSpeed',XFloat);
      AddChildF(TreeNode,p2,'SmallJumpMinVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'SmallJumpMaxVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpHorizontalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpMinVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'BigJumpMaxVerticalSpeed',XFloat);
      AddChildF(TreeNode,p2,'MaxDrop',XFloat);
      AddChildF(TreeNode,p2,'ReturnProbability',XFloat);
      AddChildF(TreeNode,p2,'MinTimeForSafeJump',XInt);
      PayloadWeapon;
    end;
    HomingPayloadWeaponPropertiesCo:
    begin
      AddNode(Format('XContainer::HomingPayloadWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'OrientationProportion',XFloat);
      AddChildF(TreeNode,p2,'Stage1Duration',XInt);
      AddChildF(TreeNode,p2,'Stage2Duration',XInt);
      AddChildF(TreeNode,p2,'Stage3Duration',XInt);
      AddChildF(TreeNode,p2,'MaxHomingSpeed',XFloat);
      AddChildF(TreeNode,p2,'HomingAcceleration',XFFloat);
      AddChildF(TreeNode,p2,'AvoidsLand',XBool);
      AddChildF(TreeNode,p2,'VerticalLandAvoidanceDistance',XFloat);
      AddChildF(TreeNode,p2,'ForwardLandAvoidanceDistance',XFloat);
      AddChildF(TreeNode,p2,'VerticalLandAvoidanceForce',XFloat);
      AddChildF(TreeNode,p2,'ForwardLandAvoidanceForce',XFloat);
      PayloadWeapon;
    end;
    PayloadWeaponPropertiesContaine:
    begin
      AddNode(Format('XContainer::PayloadWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      PayloadWeapon;
    end;
    BaseWeaponContainer:
    begin
      AddNode(Format('XContainer::BaseWeaponContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      BaseWeapon;
    end;
    MineFactoryContainer:
    begin
      AddNode(Format('XContainer::MineFactoryContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'NumMineActivation',XByte);
      AddChildF(TreeNode,p2,'NumTurnsInactive',XByte);

      AddChildF(TreeNode,p2,'SafeRadiusPadding?',XFloat);

      AddChildF(TreeNode,p2,'DamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'WormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'LandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'ImpulseRadius',XFloat);
    end;
    MeleeWeaponPropertiesContainer:
    begin
      AddNode(Format('XContainer::MeleeWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'IsAimedWeapon',XBool);
      AddChildF(TreeNode,p2,'DamageIsPercentage',XBool);
      AddChildF(TreeNode,p2,'WormIsWeapon',XBool);
      if WUM then begin
      AddChildF(TreeNode,p2,'InstantKill',XBool);
      AddChildF(TreeNode,p2,'AccuracyMeter',XBool);
      AddChildF(TreeNode,p2,'MeleeType',XInt);
      end;
      if not WUM then begin
      AddChildF(TreeNode,p2,'RetreatTimeOverride',XInt);
      end;
      AddChildF(TreeNode,p2,'Radius',XFloat);
      AddChildF(TreeNode,p2,'MinAimAngle',XFloat);
      AddChildF(TreeNode,p2,'MaxAimAngle',XFloat);

      AddChildF(TreeNode,p2,'DischargeFX',XString);
      AddChildF(TreeNode,p2,'DischargeEndFX',XString);

      AddChildF(TreeNode,p2,'WormCollisionFX',XString);
      AddChildF(TreeNode,p2,'LandCollisionFX',XString);
      if WUM then begin
      AddChildF(TreeNode,p2,'WXAnimWindup',XString);//
      end;
      AddChildF(TreeNode,p2,'LogicalPositionOffset',XVector);
      AddChildF(TreeNode,p2,'ImpulseDirection',XVector);
      AddChildF(TreeNode,p2,'LogicalLaunchYOffset',XFloat);
      AddChildF(TreeNode,p2,'WormDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'LandDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'WormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'LandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'ImpulseRadius',XFloat);
      BaseWeapon;
    end;

    GunWeaponPropertiesContainer:
    begin
      AddNode(Format('XContainer::GunWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'IsAimedWeapon',XBool);
      AddChildF(TreeNode,p2,'IsAffectedByGravity',XBool);
      AddChildF(TreeNode,p2,'IsAffectedByWind',XBool);
      AddChildF(TreeNode,p2,'bCanDamageLand',XBool);
      AddChildF(TreeNode,p2,'bCanMoveBetweenShots',XBool);
      AddChildF(TreeNode,p2,'ImpulseIsNormal',XBool);
      AddChildF(TreeNode,p2,'DamageIsPercentage',XBool);
      AddChildF(TreeNode,p2,'LaserEffect',XBool);
      if WUM then begin
      AddChildF(TreeNode,p2,'Sniper',XBool);
      end;
      if not WUM then begin
        AddChildF(TreeNode,p2,'RetreatTimeOverride',XInt);//?
      end;
      AddChildF(TreeNode,p2,'NumberOfBullets',XInt);
      AddChildF(TreeNode,p2,'DischargeTime',XInt);
      AddChildF(TreeNode,p2,'Range',XInt);
      AddChildF(TreeNode,p2,'WaitForSoundDelay',XInt);
      AddChildF(TreeNode,p2,'Accuracy',XFloat);
      AddChildF(TreeNode,p2,'WormDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'WormPoisonMagnitude',XFloat);
      AddChildF(TreeNode,p2,'LandDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'BulletRadius',XFloat);
      AddChildF(TreeNode,p2,'MinAimAngle',XFloat);
      AddChildF(TreeNode,p2,'MaxAimAngle',XFloat);
      AddChildF(TreeNode,p2,'WormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'LandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'ImpulseRadius',XFloat);
      AddChildF(TreeNode,p2,'LogicalLaunchYOffset',XFloat);
      AddChildF(TreeNode,p2,'DischargeFX',XString);
      AddChildF(TreeNode,p2,'DischargeEndFX',XString);
      AddChildF(TreeNode,p2,'SecondaryDischargeFX',XString);
      AddChildF(TreeNode,p2,'SecondaryDischargeFXLocator',XString);
      AddChildF(TreeNode,p2,'DischargeSoundFX',XString);
      AddChildF(TreeNode,p2,'DischargeEndSoundFX',XString);
      AddChildF(TreeNode,p2,'WormCollisionFX',XString);
      AddChildF(TreeNode,p2,'LandCollisionFX',XString);
      AddChildF(TreeNode,p2,'WaterCollisionFX',XString);
      AddChildF(TreeNode,p2,'LogicalPositionOffset',XVector);
      AddChildF(TreeNode,p2,'ImpulseDirection',XVector);
      AddChildF(TreeNode,p2,'DischargeFXZOffset',XFloat);
      AddChildF(TreeNode,p2,'KickSize',XFloat);
      AddChildF(TreeNode,p2,'KickFrequency',XFloat);
      BaseWeapon;
    end;
    SentryGunWeaponPropertiesContai:
    begin
      AddNode(Format('XContainer::SentryGunWeaponPropertiesContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'ActivatedFx',XString);
      AddChildF(TreeNode,p2,'ReloadFx',XString);
      AddChildF(TreeNode,p2,'PreExplosionFx',XString);
      AddChildF(TreeNode,p2,'DamageFx',XString);
      AddChildF(TreeNode,p2,'ExplosionFx',XString);
      AddChildF(TreeNode,p2,'FireFx',XString);
      AddChildF(TreeNode,p2,'SplishFx',XString);
      AddChildF(TreeNode,p2,'SplashFx',XString);
      AddChildF(TreeNode,p2,'SinkingFx',XString);
      AddChildF(TreeNode,p2,'ReloadSfx',XString);
      AddChildF(TreeNode,p2,'ExplosionSfx',XString);
      AddChildF(TreeNode,p2,'FireSfx',XString);
      AddChildF(TreeNode,p2,'SplashSfx',XString);
      AddChildF(TreeNode,p2,'ShotImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ShotImpulseRadius',XFloat);
      AddChildF(TreeNode,p2,'ShotLandDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ShotLandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'ShotWormDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'ShotWormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'WeaponDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'WeaponDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'DeathWormDamageMagnitude',XFloat);
      AddChildF(TreeNode,p2,'DeathWormDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'DeathLandDamageRadius',XFloat);
      AddChildF(TreeNode,p2,'DeathImpulseMagnitude',XFloat);
      AddChildF(TreeNode,p2,'DeathImpulseRadius',XFloat);
      AddChildF(TreeNode,p2,'MaxWeaponTemp',XFloat);
      AddChildF(TreeNode,p2,'TempDelta',XFloat);
      AddChildF(TreeNode,p2,'WeaponReloadTime',XInt);
      AddChildF(TreeNode,p2,'MinWeaponRange',XFloat);
      AddChildF(TreeNode,p2,'MaxWeaponRange',XFloat);
      AddChildF(TreeNode,p2,'LogicalLaunchZOffset',XFloat);
      AddChildF(TreeNode,p2,'CollisionRadius',XFloat);
      AddChildF(TreeNode,p2,'TurretRotationalVelocity',XFFloat);
      AddChildF(TreeNode,p2,'WeaponHealth',XFloat);
      BaseWeapon;
    end;
    ParticleEmitterContainer:
    begin
      AddNode(Format('XContainer::ParticleEmitterContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      AddChildF(TreeNode,p2,'Comment',XString);
      AddChildF(TreeNode,p2,'EmitterType',XInt);
      AddChildF(TreeNode,p2,'SpriteSet',XString);
       x:=byte(p2^);  Inc(Longword(p2)); // ����
       if x=1 then  begin   // ���� ��� ���� �� �������� �����
             AddChildF(TreeNode,p2,'MeshSet',XString); // ���� ���� �� ��� �����
        end;
      AddChildF(TreeNode,p2,'MeshAnimNodeName',XString);
      AddChildF(TreeNode,p2,'EmitterAcceleration',XVector);
      AddChildF(TreeNode,p2,'EmitterAccelerationRandomise',XVector);
      AddChildF(TreeNode,p2,'EmitterIsAttachedToLand',XBool); 
      AddChildF(TreeNode,p2,'EmitterIsOfInterest',XBool);
      AddChildF(TreeNode,p2,'EmitterIsLaunchedFromWeapon',XBool);
      AddChildF(TreeNode,p2,'EmitterLifeTime',XUInt);
      AddChildF(TreeNode,p2,'EmitterLifeTimeRandomise',XUint);
      AddChildF(TreeNode,p2,'EmitterMaxParticles',XUint);
      AddChildF(TreeNode,p2,'EmitterNumCollide',XUint);
      AddChildF(TreeNode,p2,'EmitterNumSpawn',XUint);
      AddChildF(TreeNode,p2,'EmitterNumSpawnRadnomise',XUint);
      AddChildF(TreeNode,p2,'EmitterOriginOffset',XVector);
      AddChildF(TreeNode,p2,'EmitterOriginRandomise',XVector);
      AddChildF(TreeNode,p2,'EmitterParticleExpireFX',XString);
      AddChildF(TreeNode,p2,'EmitterParticleFX',XString);
      AddChildF(TreeNode,p2,'EmitterSameDirectionAsWorm',XBool);
      AddChildF(TreeNode,p2,'EmitterSoundFX',XString);
      if WUM then begin
      AddChildF(TreeNode,p2,'EmitterSoundFXVolume',XFloat);
      end;
      AddChildF(TreeNode,p2,'EmitterSpawnFreq',XUint);
      AddChildF(TreeNode,p2,'EmitterSpawnFreqRansomise',XUint);
      AddChildF(TreeNode,p2,'EmitterSpawnSizeVelocity',XPoint);
      AddChildF(TreeNode,p2,'EmitterStartDelay',XUint);
      AddChildF(TreeNode,p2,'EmitterVelocity',XVector);


      AddChildF(TreeNode,p2,'EmitterVelocityRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleAcceleration',XVector);
      AddChildF(TreeNode,p2,'ParticleAccelerationRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleAlpha',XFloat);
      AddChildF(TreeNode,p2,'ParticleAlphaFadeIn',XUint);
      AddChildF(TreeNode,p2,'ParticleAlphaVelocity',XFloat);
      AddChildF(TreeNode,p2,'ParticleAlphaVelocityDelay',XUint);
      AddChildF(TreeNode,p2,'ParticleAlternateAccelerationN',XFloat);
      AddChildF(TreeNode,p2,'ParticleAlternateAccelerationS',XFFloat);
      AddChildF(TreeNode,p2,'ParticleAnimationFrame',XByte);
      AddChildF(TreeNode,p2,'ParticleAnimationFrameRandomise',XByte);
      AddChildF(TreeNode,p2,'ParticleAnimationSpeed',XUint);
      AddChildF(TreeNode,p2,'ParticleAnimationSpeedRandomize',XUint);
      AddChildF(TreeNode,p2,'ParticleAttractor',XVector);
      AddChildF(TreeNode,p2,'ParticleAttractorRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleAttractorIsActive',XBool);
      AddChildF(TreeNode,p2,'ParticleCanEnterWater',XBool);
      AddChildF(TreeNode,p2,'ParticleCollisionFreq',XFloat);
      AddChildF(TreeNode,p2,'ParticleCollisionImmuneTime',XUint);
      AddChildF(TreeNode,p2,'ParticleCollisionMinAlpha',XFloat);
      AddChildF(TreeNode,p2,'ParticleCollisonRadius',XFloat);
      AddChildF(TreeNode,p2,'ParticleCollisonRadiusOffset',XVector);
      AddChildF(TreeNode,p2,'ParticleCollisonRadiusVelocity',XFFloat);
      AddChildF(TreeNode,p2,'ParticleCollisionShowDebug',XBool);
      AddChildF(TreeNode,p2,'ParticleCollisionWormDamageMagnitude',XUint);
      AddChildF(TreeNode,p2,'ParticleCollisionWormImpulseMagnitude',XUint);
      AddChildF(TreeNode,p2,'ParticleCollisionWormImpulseYMagnitude',XUint);
      AddChildF(TreeNode,p2,'ParticleCollisionWormPoisonMagnitude',XUint);
      AddChildF(TreeNode,p2,'ParticleCollisionWormType',XInt);
       x:=byte(p2^);  Inc(Longword(p2)); // ����
      for i:=1 to x do
        AddChildF(TreeNode,p2,'ParticleColor',XFColor);
       x:=byte(p2^);  Inc(Longword(p2)); // ����
      for i:=1 to x do
        AddChildF(TreeNode,p2,'ParticleColorBand',XFloat);
      AddChildF(TreeNode,p2,'ParticleExpireShake',XBool);
      AddChildF(TreeNode,p2,'ParticleExpireShakeLength',XFloat);
      AddChildF(TreeNode,p2,'ParticleExpireShakeMagnitude',XUint);
      AddChildF(TreeNode,p2,'ParticleIsAlternateAcceleration',XBool);
      AddChildF(TreeNode,p2,'ParticleIsEffectedByWind',XBool);
      AddChildF(TreeNode,p2,'ParticleIsSpiral',XBool);
      AddChildF(TreeNode,p2,'ParticleIsUnderWaterEffect',XBool);
      AddChildF(TreeNode,p2,'ParticleLandCollideType',XInt);
      AddChildF(TreeNode,p2,'ParticleLife',XUint);
      AddChildF(TreeNode,p2,'ParticleLifeRandomise',XUint);
      AddChildF(TreeNode,p2,'ParticleMass',XFloat);
      AddChildF(TreeNode,p2,'ParticleNumColors',XByte);
      AddChildF(TreeNode,p2,'ParticleNumFrames',XByte);
      AddChildF(TreeNode,p2,'ParticleOrientation',XVector);
      AddChildF(TreeNode,p2,'ParticleOrientationRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleOrientationVelocity',XVector);
      AddChildF(TreeNode,p2,'ParticleOrientationVelocityRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleSize',XPoint);
      AddChildF(TreeNode,p2,'ParticleSizeOriginIsCenterPoint',XBool);
      AddChildF(TreeNode,p2,'ParticleSizeRandomise',XFloat);
      AddChildF(TreeNode,p2,'ParticleSizeVelocity',XPoint);
      AddChildF(TreeNode,p2,'ParticleSizeVelocityRandomise',XPoint);
      AddChildF(TreeNode,p2,'ParticleSizeVelocityDelay',XUint);
      if WUM then begin
      AddChildF(TreeNode,p2,'ParticleSizeVelocityDelayRandomise',XUint);
      end;
      AddChildF(TreeNode,p2,'ParticleFinalSizeScale',XPoint);
      AddChildF(TreeNode,p2,'ParticleFinalSizeScaleRandomise',XFloat);
      if WUM then begin
      AddChildF(TreeNode,p2,'ParticleSizeFadeIn',XUint);
      AddChildF(TreeNode,p2,'ParticleSizeFadeInRandomize',XUint);
      AddChildF(TreeNode,p2,'ParticleSizeFadeInDelay',XUint);
      AddChildF(TreeNode,p2,'ParticleSizeFadeInDelayRandomize',XUint);
      end;
      AddChildF(TreeNode,p2,'ParticleRenderScene',XInt);
      AddChildF(TreeNode,p2,'ParticleSpiralRadius',XFloat);
      AddChildF(TreeNode,p2,'ParticleSpiralRadiusRandomise',XFloat);
      AddChildF(TreeNode,p2,'ParticleSpiralRadiusVelocity',XFloat);
      AddChildF(TreeNode,p2,'ParticleSpiralRadiusVelocityRandomise',XFloat);
      AddChildF(TreeNode,p2,'ParticleSpiralRadiusSizeVelocity',XFloat);
      AddChildF(TreeNode,p2,'ParticleVelocity',XVector);
      AddChildF(TreeNode,p2,'ParticleVelocityRandomise',XVector);
      AddChildF(TreeNode,p2,'ParticleVelocityIsNormalised',XBool);

    end;
    EffectDetailsContainer:
    begin
      AddNode(Format('XContainer::EffectDetailsContainer [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k:= TestByte128(p2);
      for x:=1 to k do
      TreeView.Items.AddChild(TreeNode,
        Format('EffectNames = "%s"', [GetString(TestByte128(p2))]));
    end;
    CampaignData:
    begin
      AddNode(Format('CampaignData [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddChildF(TreeNode,p2,'Type',XInt);
    end;
    StoredTeamData:
    begin
      AddNode(Format('StoredTeamData [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      TempNode:=AddChildF(TreeNode,p2,'Team',XString);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      TreeNode.Text:=TreeNode.Text+format(' "%s"',[GetString(TestByte128(p2))]);
       x:=TestByte128(p2);// ����
      for i := 1 to x do
      begin
        AddChildF(TempNode,p2,Format('worm %d', [i]),XString);
      end;

       x:=TestByte128(p2);  // ����
      TempNode:=TreeView.Items.AddChild(TreeNode,
        Format('Campaign[%d]', [x]));
      for i := 1 to x do
      begin
        TempNode2:=AddClassTree(TestByte128(p2), TreeView, TempNode);
        TempNode2.Text:=format('%d. %s',[i,TempNode2.Text])
      end;
      for i:= 1 to 5 do
      begin
       AddChildF(TreeNode,p2,Format('val %d', [i]),XInt);
      end;
        AddChildF(TreeNode,p2,Format('Flag', [i]),XString);
        AddChildF(TreeNode,p2,Format('SoundBank', [i]),XString);
        AddChildF(TreeNode,p2,Format('?', [i]),Xbyte);
        AddChildF(TreeNode,p2,Format('Login', [i]),XString);
    end;
    XImage:
    begin
      AddNode(Format('XImage [%d]', [CntrIndx]),3);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := GetString(TestByte128(p2));

      w := Word(p2^);
      Inc(Longword(p2), 2);
      h := Word(p2^);
      Inc(Longword(p2), 2);

      Imap := Byte(p2^); 
      Inc(Longword(p2), 2);
      Inc(Longword(p2), 2);
      Bsize := Byte(p2^);  
      Inc(Longword(p2), 4 * Bsize + 1);
      Bsize := Byte(p2^);  
      Inc(Longword(p2), 4 * Bsize + 1);
      Inc(Longword(p2), 4);
      ISize := TestByte128(p2);
      Inc(Longword(p2), ISize);
      k3 := TestByte128(p2);
      s := Format(' "%s" [%dx%d maps:%d size:%d]', [s, w, h, Imap, ISize]);
      TreeNode.Text := TreeNode.Text + s;
      if k3 > 0 then  
        AddClassTree(k3, TreeView, TreeNode);
    end;
    XAnimClipLibrary:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 6;
        TreeNode.SelectedIndex := 6;
      end;
      AddNode(Format('XAnimClipLibrary [%d]', [CntrIndx]));
      ////////////
      StrList := TStringList.Create;
      p2 := StrArray[CntrIndx].point;
      s := GetString(TestByte128(p2));   // Name
      inx1 := Integer(p2^);    
      Inc(Longword(p2), 4); // NumKeys
      s := Format('"%s" KeyTypes[%d]', [s, inx1]);
      TempNode := TreeView.Items.AddChild(TreeNode, s);
      StrList.Clear;
      //   SetLength(StrList,inx);
      for x := 0 to inx1 - 1 do 
      begin     // Keys
        AnimType := Cardinal(p2^);   
        Inc(Longword(p2), 4);   //AnimType
        s := GetString(TestByte128(p2));   //Object
        s1 := AnimClips.GetNameAnimType(AnimType);
        s := Format('%s.%s', [s, s1]);
        StrList.Add(s);
        s1 := Format('#%d = %s', [x, s]);
        TempNode2 := TreeView.Items.AddChild(TempNode, s1);
          TempNode2.ImageIndex := 8;
          TempNode2.SelectedIndex := 8;
      end;
      inx2 := Integer(p2^);
      Inc(Longword(p2), 4);  // NumClips
      s1 := Format('Clips[%d]', [inx2]);
      TempNode := TreeView.Items.AddChild(TreeNode, s1);
      for j := 1 to inx2 do
      begin
        ftime := Single(p2^);
        Inc(Longword(p2), 4);  // time
        s1 := Format('"%s" (%.2fs)', [GetString(TestByte128(p2)), ftime]);
        // name
        TempNode0 := TreeView.Items.AddChild(TempNode, s1);
          TempNode0.ImageIndex := 9;
          TempNode0.SelectedIndex := 9;
        inx := Word(p2^); // NumAnimKeys
        ExpAnim := (inx = 256) or (inx=257);
        if ExpAnim then 
          inx := inx1 
        else
          Inc(Longword(p2), 4);

        for x := 0 to inx - 1 do 
        begin   // AnimKeys
          k2 := Word(p2^);
          if k2 = 256 then  
          begin 
            Inc(Longword(p2), 16); 
            Continue; 
          end;
          Inc(Longword(p2), 4); // 1 1 0 0
          if not ExpAnim then
          begin
            k2 := Word(p2^); 
            Inc(Longword(p2), 2);
          end 
          else 
            k2 := x;

          k3 := Longword(p2^);
          Inc(Longword(p2), 4);
          k4 := Longword(p2^);
          Inc(Longword(p2), 4);
          k := Integer(p2^); 
          Inc(Longword(p2), 4);
          s1 := Format('%s.keys[%d] (%d;%d)', [StrList.Strings[k2], k, k3,k4]);
          TempNode2 := TreeView.Items.AddChild(TempNode0, s1);
         // TempNode2.Data:=XAnimKey;
          TempNode2.ImageIndex := 7;
          TempNode2.SelectedIndex := 7;
          for i := 1 to k do
          begin
              {  Move(p2^, KeyFrame[0], 6*4);
                s:=format('%d. [%.2f; %.2f; %.2f; %.2f; %.2f; %.2f]',[i-1,KeyFrame[0],KeyFrame[1],
                KeyFrame[2],KeyFrame[3],KeyFrame[4],KeyFrame[5]]);
                TreeView.Items.AddChild(TempNode2,s);//  }
            Inc(Longword(p2), 6 * 4);
          end;
        end;
      end;
      StrList.Free;
      //////////
    end;
    XBone:
    begin
      AddNode(Format('XBone [%d]', [CntrIndx]),10);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 153);
      s := Format(' "%s"', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s;

      if HideChild then
        Exit;
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      TempNode := TreeView.Items.AddChild(TreeNode, 'Data');
      Move(p2^, Matrix[1][1], 16 * 4); 
      Inc(Longword(p2), 16 * 4);
      for i := 1 to 4 do 
      begin
        s := Format('[%.2f; %.2f; %.2f; %.2f]',
          [Matrix[i][1], Matrix[i][2], Matrix[i][3], Matrix[i][4]]);
        TreeView.Items.AddChild(TempNode, s);
      end;

      Inc(Longword(p2), 16 * 4);
      //  s:=format('[%d]',[word(p2^)] );
      //  TreeView.Items.AddChild(TempNode,s);
      Inc(Longword(p2), 2);

      Move(p2^, Matrix[1][1], 4 * 4); 
      Inc(Longword(p2), 4 * 4);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Matrix[1][1], Matrix[1][2], Matrix[1][3], Matrix[1][4]]);
      TreeView.Items.AddChild(TempNode, s);
    end;
    XMatrix:
    begin
      AddNode(Format('XMatrix [%d]', [CntrIndx]),17);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;
      TempNode := TreeView.Items.AddChild(TreeNode, 'Matrix');
      for i := 1 to 4 do 
      begin
        s := Format('[%.2f; %.2f; %.2f; %.2f]',
          [Matrix[i][1], Matrix[i][2], Matrix[i][3], Matrix[i][4]]);
        TreeView.Items.AddChild(TempNode, s);
      end;
    end;
    XTexturePlacement2D:
    begin
      AddNode(Format('XTextureStage::XTextureGen::XTextureMatrix::XTexturePlacement2D [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Matrix[1][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[2][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[3][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      Move(p2^, Matrix[4][1], 4 * 4);
      Inc(Longword(p2), 4 * 4);
      TempNode := TreeView.Items.AddChild(TreeNode, 'Matrix');
      for i := 1 to 4 do 
      begin
        s := Format('[%.2f; %.2f; %.2f; %.2f]',
          [Matrix[i][1], Matrix[i][2], Matrix[i][3], Matrix[i][4]]);
        TreeView.Items.AddChild(TempNode, s);
      end;
      Move(p2^, Matrix[1][1], 4 * 4); 
      Inc(Longword(p2), 4 * 4);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Matrix[1][1], Matrix[1][2], Matrix[1][3], Matrix[1][4]]);
      TreeView.Items.AddChild(TempNode, s);
    end;
    XJointTransform:
    begin
      AddNode(Format('XMatrix::XJointTransform [%d]', [CntrIndx]),17);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Rot[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('RotOrient = <%.2f; %.2f; %.2f>',
        [RadToDeg(Rot[0]), RadToDeg(Rot[1]), RadToDeg(Rot[2])]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Rot[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('Rot = <%.2f; %.2f; %.2f>',
        [RadToDeg(Rot[0]), RadToDeg(Rot[1]), RadToDeg(Rot[2])]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Pos[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('Pos = <%.2f; %.2f; %.2f>', [Pos[0], Pos[1], Pos[2]]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Rot[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('Joint Orient = <%.2f; %.2f; %.2f>',
        [RadToDeg(Rot[0]), RadToDeg(Rot[1]), RadToDeg(Rot[2])]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Size[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      s := Format('Size = <%.2f; %.2f; %.2f>', [Size[0], Size[1], Size[2]]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(Longword(p2), 4);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;
      TempNode := TreeView.Items.AddChild(TreeNode, 'Matrix');
      for i := 1 to 4 do 
      begin
        s := Format('[%.2f; %.2f; %.2f; %.2f]',
          [Matrix[i][1], Matrix[i][2], Matrix[i][3], Matrix[i][4]]);
        TreeView.Items.AddChild(TempNode, s);
      end;
    end;

    XTransform:
    begin
      AddNode(Format('XMatrix::XTransform [%d]', [CntrIndx]),17);

      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Move(p2^, Pos[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('Pos = <%.2f; %.2f; %.2f>', [Pos[0], Pos[1], Pos[2]]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Rot[0], 4 * 3); 
      Inc(Longword(p2), 4 * 3);
      s := Format('Rot = <%.2f; %.2f; %.2f>',
        [RadToDeg(Rot[0]), RadToDeg(Rot[1]), RadToDeg(Rot[2])]);
      TreeView.Items.AddChild(TreeNode, s);
      Move(p2^, Size[0], 4 * 3);
      Inc(Longword(p2), 4 * 3);
      s := Format('Size = <%.2f; %.2f; %.2f>', [Size[0], Size[1], Size[2]]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(Longword(p2), 4);
      Move(p2^, Matrix[1][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[1][4] := 0;
      Move(p2^, Matrix[2][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[2][4] := 0;
      Move(p2^, Matrix[3][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[3][4] := 0;
      Move(p2^, Matrix[4][1], 4 * 3); 
      Inc(Longword(p2), 4 * 3); 
      Matrix[4][4] := 1;
      TempNode := TreeView.Items.AddChild(TreeNode, 'Matrix');
      for i := 1 to 4 do 
      begin
        s := Format('[%.2f; %.2f; %.2f; %.2f]',
          [Matrix[i][1], Matrix[i][2], Matrix[i][3], Matrix[i][4]]);
        TreeView.Items.AddChild(TempNode, s);
      end;
    end;
    XOglTextureMap:
    begin
      AddNode(Format('XTextureStage::XTextureMap::XOglTextureMap [%d]', [CntrIndx]),16);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Inc(Longword(p2), 4);
      Inc(Longword(p2), 4 * 4);
      k3 := TestByte128(p2);
      AddClassTree(k3, TreeView, TreeNode);
      Inc(Longword(p2), 4);
      Inc(Longword(p2), 2);
      Inc(Longword(p2), 4 * 5);
      k := TestByte128(p2);
      AddClassTree(k, TreeView, TreeNode);
    end;
    XBlendModeGL:
    begin
      AddNode(Format('XAttribute::XBlendModeGL [%d]', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := Format('Blend = [%d; %d]', [Longword(p2^),
        Longword(Pointer(Longword(p2) + 4)^)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XAlphaTest:
    begin
      AddNode(Format('XAttribute::XAlphaTest [%d]', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Inc(Longword(p2), 1);
      s := Format('AlphaFunc = %d', [Longword(p2^)]);
      TreeView.Items.AddChild(TreeNode, s);
      s := Format('Alpha = %.2f', [Single(Pointer(p2)^)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XDepthTest:
    begin
      AddNode(Format('XAttribute::XDepthTest [%d]', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := Format('Depth [%d; %d; %f]', [Longword(Pointer(Longword(p2))^),
        Longword(Pointer(Longword(p2) + 4)^), Single(Pointer(Longword(p2) + 9)^)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XCullFace:
    begin
      AddNode(Format('XAttribute::XCullFace [%d] ', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := BoolToStr(Boolean(Longword(p2^)), true);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XLightingEnable:
    begin
      AddNode(Format('XAttribute::XLightingEnable [%d] ', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k3 := TestByte128(p2);
      px := Longword(p2) + 6;
      s := Format('Light %s %d [%.2f; %.2f; %.2f; %.2f]',
        [BoolToStr(Boolean(k3), true), Longword(Pointer(Longword(p2) + 2)^),
        Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^),
        Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XZBufferWriteEnable:
    begin
      AddNode(Format('XAttribute::XZBufferWriteEnable [%d] ', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      s := BoolToStr(Boolean(Byte(p2^)), true);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XPointLight:
    begin
      AddNode(Format('XLight::XPointLight [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      px := Longword(p2);
      s := Format('Pos = [%.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,12);
      s := Format('Radius = [%.2f]',[Single(Pointer(px)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,4);
      s := Format('Power = [%.2f]',[Single(Pointer(px)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,4);
      s := Format('? = [%.2f; %.2f]',[Single(Pointer(px)^), Single(Pointer(px + 4)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,2*4);
      Inc(px,2); //?
      s := Format('? = [%.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,12);
      s := Format('? = [%.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,12);
      s := Format('? = [%.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px,12);
      s1 := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(px+5*4);
      s := GetString(TestByte128(p2));
      s1 := Format(' "%s" %s', [s, s1]);
      TreeNode.Text := TreeNode.Text + s1;
    end;
    XMaterial:
    begin
      AddNode(Format('XAttribute::XMaterial [%d]', [CntrIndx]),18);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      px := Longword(p2);
      s := Format('Ambient = [%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px, 16);
      s := Format('Diffuse = [%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px, 16);
      s := Format('Specular = [%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px, 16);
      s := Format('Emission = [%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px, 16);
      s := Format('? = [%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      TreeView.Items.AddChild(TreeNode, s);
      Inc(px, 16);
      s := Format('Shininess = [%.2f]', [Single(Pointer(px)^)]);
      TreeView.Items.AddChild(TreeNode, s);
    end;
    XInteriorNode:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 2;
        TreeNode.SelectedIndex := 2;
      end;
      AddNode(Format('XNode::XInteriorNode [%d]', [CntrIndx]),19);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 0 to k - 1 do 
      begin
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      end;
      px := Longword(p2);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s1 := Format(' "%s;" ', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s1 + s;
    end;
    XTexFont:
    begin
      AddNode(Format('XShader::XSimpleShader::XTexFont [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);  
      s := Format('UV0 = [%d]', [k]);
      for x := 1 to k do 
      begin
        if Length(s) < 100 then
          s := s + Format(' <%.2f;%.2f>', [Single(p2^),
            Single(Pointer(Longword(p2) + 4)^)]);
        Inc(Longword(p2), 8);
      end;
      TreeView.Items.AddChild(TreeNode, s);
      k := TestByte128(p2);  
      s := Format('UV1 = [%d]', [k]);
      for x := 1 to k do 
      begin
        if Length(s) < 100 then
          s := s + Format(' <%.2f;%.2f>', [Single(p2^),
            Single(Pointer(Longword(p2) + 4)^)]);
        Inc(Longword(p2), 8);
      end;
      TreeView.Items.AddChild(TreeNode, s);
      k := TestByte128(p2);
      for x := 1 to k do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      k := TestByte128(p2);
      for x := 1 to k do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 4);
      s := Format(' "%s"', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XEnvironmentMapShader:
    begin
      AddNode(Format('XShader::XEnvironmentMapShader [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
              Inc(Longword(p2), 1);
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
       s := Format('Unknown = [%d]',[Integer(p2^)]);
      TreeView.Items.AddChild(TreeNode, s);
              Inc(Longword(p2), 8);
       s := Format(' "%s"', [GetString(TestByte128(p2))]);
       TreeNode.Text := TreeNode.Text + s;
            end;
    XMultiTexShader:
    begin
      AddNode(Format('XShader::XMultiTexShader [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      Inc(Longword(p2), 1);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 4);
       s := Format(' "%s"', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XSimpleShader:
    begin
      AddNode(Format('XShader::XSimpleShader  [%d]', [CntrIndx]),11);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      k := TestByte128(p2);
      for x := 1 to k do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 4);
      s := Format(' "%s"', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s;
    end;
    XSkinShape:
    begin
      AddNode(Format('XSkinShape [%d]', [CntrIndx]),13);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      k := TestByte128(p2);
      for x := 1 to k do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode, true);
      Inc(Longword(p2), 4);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 5);
      px := Longword(p2);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s1 := Format(' "%s" ', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s1 + s;
    end;
    XShape:
    begin
      AddNode(Format('XCore::XShape [%d]', [CntrIndx]),15);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      x := Byte(p2^);
      Inc(Longword(p2), 4);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 5);
      px := Longword(p2);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s1 := Format(' "%s" ', [GetString(TestByte128(p2))]);
      TreeNode.Text := TreeNode.Text + s1 + s;
      TreeNode.ImageIndex:=15;
      TreeNode.SelectedIndex:=15;
    end;
    XSkin:
    begin
      AddNode(Format('XSkin [%d]', [CntrIndx]),14);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);

      k := TestByte128(p2);
      for x := 0 to k - 1 do 
      begin
        inx := TestByte128(p2);
        AddClassTree(inx, TreeView, TreeNode);
      end;

      px := Longword(p2);
      s := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      s1 := Format(' "%s" ', [s]);
      TreeNode.Text := TreeNode.Text + s1;
    end;
    XChildSelector:
    begin
      AddNode(Format('XChildSelector [%d]', [CntrIndx]));
    end;
    XBinModifier:
    begin
      AddNode(Format('XBinModifier [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 4);
      k := TestByte128(p2);
      AddClassTree(TestByte128(p2), TreeView, TreeNode); // Matrix
      //    for x:=1 to k do
      //    if k>1 then
      //    Inc(LongWord(p2));

      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode); // Matrix
      px := Longword(p2);
      s1 := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      s1 := Format(' "%s" %s', [s, s1]);
      TreeNode.Text := TreeNode.Text + s1;
    end;
    XGroup:
    begin
      AddNode(Format('XGroup [%d]', [CntrIndx]),5);
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);
      AddClassTree(TestByte128(p2), TreeView, TreeNode); // Matrix
      //   Mult:=true;
      //   glPushMatrix;
      //    glMultMatrixf(@CurMatrix[1][1]);
      k := TestByte128(p2);
      for x := 0 to k - 1 do 
      begin
        inx := TestByte128(p2);
        AddClassTree(inx, TreeView, TreeNode);
      end;
      //    glPopMatrix;
      px := Longword(p2);
      s1 := Format('[%.2f; %.2f; %.2f; %.2f]',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^),
        Single(Pointer(px + 8)^), Single(Pointer(px + 12)^)]);
      p2 := Pointer(Longword(p2) + 4 * 5);
      s := GetString(TestByte128(p2));
      s1 := Format(' "%s" %s', [s, s1]);
      TreeNode.Text := TreeNode.Text + s1;
    end;
    XPalette:
      AddNode(Format('XPalette [%d]', [CntrIndx]));
    XIndexSet:
      AddNode(Format('XIndexArray::XIndexSet [%d]', [CntrIndx]));
    XCoord3fSet:
      AddNode(Format('XVertexDataSet::XCoordSet::XCoord3fSet [%d]', [CntrIndx]));
    XNormal3fSet:
      AddNode(Format('XVertexDataSet::XNormalSet::XNormal3fSet [%d]', [CntrIndx]));
    XColor4ubSet:
      AddNode(Format('XVertexDataSet::XColorSet::XColor4ubSet [%d]', [CntrIndx]));
    XConstColorSet:
      AddNode(Format('XVertexDataSet::XColorSet::XConstColorSet [%d]', [CntrIndx]));
    XTexCoord2fSet:
      AddNode(Format('XVertexDataSet::XTexCoordSet::XTexCoord2fSet [%d]',
        [CntrIndx]));
    XMultiTexCoordSet:
    begin  AddNode(Format('XVertexDataSet::XTexCoordSet::XMultiTexCoordSet [%d]',
        [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].Point) + 3);   
      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
    end;
    XWeightSet:
      AddNode(Format('XVertexDataSet::XWeightSet [%d]', [CntrIndx]));
    XPaletteWeightSet:
      AddNode(Format('XVertexDataSet::XWeightSet::XPaletteWeightSet [%d]',
        [CntrIndx]));
    XCollisionData:
    begin
      if TreeNode <> nil then
      begin
        TreeNode.ImageIndex := 5;
        TreeNode.SelectedIndex := 5;
      end;
      AddNode(Format('XCollisionData [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].Point) + 5);
      k := TestByte128(p2); 
      k3 := 0;
      if k <> 0 then 
        k3 := TestByte128(p2);
      AddClassTree(k3, TreeView, TreeNode);
    end;
    XPositionData:
      AddNode(Format('XPositionData [%d]', [CntrIndx]));
    XAnimInfo:
      AddNode(Format('XAnimInfo [%d]', [CntrIndx]));
    XExpandedAnimInfo:
      AddNode(Format('XExpandedAnimInfo [%d]', [CntrIndx]));
    XDetailObjectsData:
    begin
      AddNode(Format('XDetailObjectsData [%d]', [CntrIndx]));
      p2 := Pointer(Longword(StrArray[CntrIndx].point) + 3);

      k := TestByte128(p2);
      for x := 1 to k do 
        AddClassTree(TestByte128(p2), TreeView, TreeNode);
      k := TestByte128(p2);
      for x := 1 to k do
        AddClassTree(TestByte128(p2), TreeView, TreeNode);

      k := TestByte128(p2);
      if k > 0 then 
      begin
        s := '';
        for x := 1 to k do
        begin
          if Length(s) < 100 then
            s := s + Format(' <%.2f;%.2f;%.2f>',
              [Single(p2^), Single(Pointer(Longword(p2) + 4)^),
              Single(Pointer(Longword(p2) + 8)^)]);
          Inc(Longword(p2), 3 * 4);
        end;
        TreeView.Items.AddChild(TreeNode, s);
      end;
      k := TestByte128(p2);
      if k > 0 then 
      begin
        s := '';
        for x := 1 to k do
        begin
          if Length(s) < 100 then
            s := s + Format(' <%.2f;%.2f;%.2f>',
              [Single(p2^), Single(Pointer(Longword(p2) + 4)^),
              Single(Pointer(Longword(p2) + 8)^)]);
          Inc(Longword(p2), 3 * 4);
        end;
        TreeView.Items.AddChild(TreeNode, s);
      end;
      k := TestByte128(p2);
      if k > 0 then 
      begin
        s := '';
        for x := 1 to k do
        begin
          if Length(s) < 100 then
            s := s + Format(' <%.2f;%.2f;%.2f>',
              [Single(p2^), Single(Pointer(Longword(p2) + 4)^),
              Single(Pointer(Longword(p2) + 8)^)]);
          Inc(Longword(p2), 3 * 4);
        end;
        TreeView.Items.AddChild(TreeNode, s);
      end;
    end;
    XFortsExportedData:
      AddNode(Format('XFortsExportedData [%d]', [CntrIndx]));
    XCollisionGeometry:
    begin
      AddNode(Format('XCollisionGeometry [%d]', [CntrIndx]));
      p2 := Pointer(Integer(StrArray[CntrIndx].Point) + 7 + 8);
      s := Format('Faces = %d', [TestByte128(p2)]);
      TreeView.Items.AddChild(TreeNode, s);
      // glBuildCollGeo(CntrIndx);
    end;
    XIndexedTriangleSet:
    begin
      AddNode(Format('XGeometry::XGeoSet::XIndexedGeoSet::XIndexedTriangleSet [%d]',
        [CntrIndx]),12);
      p2 := Pointer(Longword(StrArray[CntrIndx].Point) + 3);
      // get Faces
      k3 := TestByte128(p2);   //5
      Inc(Longword(p2), 4);
      TreeView.Items.AddChild(TreeNode, Format('Faces = %d', [Word(p2^)]));
      Inc(Longword(p2), 4);

      AddClassTree(k3, TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      TempNode := TreeView.Items.AddChild(TreeNode, 'SizeBox');
      px := Longword(p2);
      TreeView.Items.AddChild(TempNode,
        Format('MinVertex = <%.2f; %.2f; %.2f>',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]));
      Inc(px, 12);
      TreeView.Items.AddChild(TempNode,
        Format('MaxVertex = <%.2f; %.2f; %.2f>',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]));
      //    glBuildTriangleGeo(Mesh,CntrIndx,TreeView,TreeNode);
    end;
    XIndexedTriangleStripSet:
    begin
      AddNode(Format('XGeometry::XGeoSet::XIndexedGeoSet::XIndexedTriangleStripSet [%d]',
        [CntrIndx]),12);
      //    glBuildTriangleGeo(Mesh,CntrIndx,TreeView,TreeNode);
      p2 := Pointer(Longword(StrArray[CntrIndx].Point) + 6);
      // DrawTree
      TreeView.Items.AddChild(TreeNode, Format('Faces = %d',
        [Word(Pointer(Longword(StrArray[CntrIndx].Point) + 4)^)]));

      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      Inc(Longword(p2), 8);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);
      AddClassTree(TestByte128(p2), TreeView, TreeNode);

      TempNode := TreeView.Items.AddChild(TreeNode, 'SizeBox');
      px := Longword(p2);
      TreeView.Items.AddChild(TempNode,
        Format('MinVertex = <%.2f; %.2f; %.2f>',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]));
      Inc(px, 12);
      TreeView.Items.AddChild(TempNode,
        Format('MaxVertex = <%.2f; %.2f; %.2f>',
        [Single(Pointer(px)^), Single(Pointer(px + 4)^), Single(Pointer(px + 8)^)]));
    end;
    else
      if CntrIndx <> 0 then 
        AddNode(Format('XNone [%d]', [CntrIndx]));
  end;
  Result := TreeNode;
end;

// 3ds File Format
//------------------------
const
CHUNK_RGB                 = $0011;
CHUNK_INT_PERCENTAGE      = $0030;
CHUNK_VERSION             = $0002;
CHUNK_3DSVERSION          = $0003;
CHUNK_HEAD                = $0006;
CHUNK_MAIN                = $4D4D; // [-] �����
  CHUNK_OBJMESH           = $3D3D; // [-] ��������� �������
  CHUNK_MESH_VERSION      = $3D3E;
    CHUNK_OBJBLOCK        = $4000; // [+] ������
      CHUNK_TRIMESH       = $4100; // [-] trimesh-������
        CHUNK_VERTLIST    = $4110; // [+] ������ ������
        CHUNK_FACELIST    = $4120; // [+] ������ ������
        CHUNK_FACEMAT     = $4130; // [+] ��������� ������
        CHUNK_MAPLIST     = $4140; // [+] ���������� ����������
        CHUNK_TRMATRIX    = $4160; // [+] ������� ��������
      CHUNK_CAMERA        = $4700; // [+] ������-������
  CHUNK_MATERIAL          = $AFFF; // [-] ��������
    CHUNK_MATNAME         = $A000; // [+] �������� ���������
    CHUNK_MAT_AMBIENT     = $A010;
    CHUNK_MAT_DIFFUSE     = $A020;
    CHUNK_MAT_SPECULAR    = $A030;
    CHUNK_MAT_SHININESS   = $A040;
    CHUNK_MAT_SHIN2PCT    = $A041;
    CHUNK_MAT_TRANSPARENCY= $A050;
    CHUNK_MAT_XPFALL      = $A052;
    CHUNK_MAT_REFBLUR     = $A053;
    CHUNK_MAT_SHADING     = $A100;
    CHUNK_MAT_SELF_ILPCT  = $A084;
    CHUNK_MAT_XPFALLIN    = $A08A;
    CHUNK_MAT_WIRESIZE    = $A087;


    CHUNK_TEXTURE         = $A200; // [-] �������� ���������
      CHUNK_MAPFILE       = $A300; // [+] ��� ����� ��������
CHUNK_KEYFRAMER           = $B000; // [-] ���������� �� ��������
  CHUNK_TRACKINFO         = $B002; // [-] ��������� �������
    CHUNK_TRACKFRAMETIME  = $B009;
    CHUNK_TRACKSCENANAME  = $B00A;
    CHUNK_TRACKOBJNAME    = $B010; // [+] �������� ����� �������
    CHUNK_TRACKDUMMYNAME  = $B011; // [+] �������� ����� �������
    CHUNK_TRACKPIVOT      = $B013; // [+] ����� �������� �������
    CHUNK_TRACKPOS        = $B020; // [+] ���������� �������
    CHUNK_TRACKROTATE     = $B021; // [+] ���������� ��������
    CHUNK_TRACKSCALE      = $B022; // [+] ������
                                    //     �������
  CHUNK_TRACKCAMERA       = $B003; // [-] ��������� ������
    CHUNK_TRACKFOV        = $B023; // [+] ��������� FOV ������
    CHUNK_TRACKROLL       = $B024; // [+] ��������� roll ������
  CHUNK_TRACKCAMTGT       = $B004; // [-] ��������� "����" ������
 //������������ ������ chunk'��
{function GetChunkObjLen(Texture:Boolean):Dword;
var
 i:integer;
begin
  MaxFace := Length(Face);
   FacesMat:=0;
   for i:=0 to Length(Materials)-1 do
    FacesMat:=FacesMat+CHUNK_HEAD+2+2*Length(Materials[i])+Length(MaterialsName[i])+1;
  TextCoordlen:=0;
if Texture then TextCoordlen:=CHUNK_HEAD+2+8*MaxVert;
  FacesLen := CHUNK_HEAD+2+8*MaxFace+FacesMat;
  MaxVert := Length(Vert);
  VertsLen := CHUNK_HEAD+2+12*MaxVert;
  TrimeshLen := CHUNK_HEAD+VertsLen+FacesLen+TextCoordlen;
  NameLen := Length(Name)+1;
  ObjBlockLen := CHUNK_HEAD+NameLen+TrimeshLen;
  Result := ObjBlockLen;
end;   }
(*
function SaveXomTo3DS(FileName: string;Textures:Boolean): Boolean;
var

  VirtualBufer: TMemoryStream;
  FileLength,i,j,val: Integer;
 version_len,ObjMeshLen, KeyFramesLen,MaterialLen,
 main_len,v: Integer;
// Name:PChar;
 MolelsLen,TrackInfoLen:integer;
 TrMatrix: TGLMatrixd4;
 MatStrList:TStringList;
//------------
 MatData: array of TMaterial;
 ModelData: AMesh;
 Matix:TMatrix;

 procedure WriteChunk(CHUNK_ID:Word; ChunkLen:DWord);
 begin
        VirtualBufer.Write(CHUNK_ID,2);
        VirtualBufer.Write(ChunkLen,4);
 end;

 procedure WriteChunkObject(ChObj:TMesh);
 var i,j,v,zero:integer;
 begin

 // LogFile.WriteLog(ChObj.Name);
  //------------------
  WriteChunk(CHUNK_OBJBLOCK,ChObj.ObjBlockLen);
  VirtualBufer.Write(Pchar(ChObj.Name)^,ChObj.NameLen);
  //------------------
   WriteChunk(CHUNK_TRIMESH,ChObj.TrimeshLen);
   //------------------
    WriteChunk(CHUNK_VERTLIST,ChObj.VertsLen);
    VirtualBufer.Write(ChObj.MaxVert,2);
    for i:=0 to ChObj.MaxVert-1 do
        for j:=0 to 2 do  VirtualBufer.Write(ChObj.Vert[i][j],4);
     //-----------------
    if Textures then begin
    WriteChunk(CHUNK_MAPLIST,ChObj.TextCoordlen);
    VirtualBufer.Write(ChObj.MaxVert,2);
    for i:=0 to ChObj.MaxVert-1 do
        for j:=0 to 1 do  VirtualBufer.Write(ChObj.TextCoord[i][j],4);
    end;
     //-----------------
    WriteChunk(CHUNK_FACELIST,ChObj.FacesLen);
    VirtualBufer.Write(ChObj.MaxFace,2);
   // zero := 7;
    for i:=0 to ChObj.MaxFace-1 do begin
     VirtualBufer.Write(ChObj.Face[i][2],2);
     VirtualBufer.Write(ChObj.Face[i][1],2);
     VirtualBufer.Write(ChObj.Face[i][0],2);
     VirtualBufer.Write(ChObj.Face[i][3],2);
    end;

    for i:=0 to Length(ChObj.Materials)-1 do
    begin
    WriteChunk(CHUNK_FACEMAT,CHUNK_HEAD+2+2*Length(ChObj.Materials[i])+Length(ChObj.MaterialsName[i])+1);
    VirtualBufer.Write(Pchar(ChObj.MaterialsName[i])^,Length(ChObj.MaterialsName[i])+1);
    v:=Length(ChObj.Materials[i]);
    VirtualBufer.Write(v,2);
    for j:=0 to v-1 do
         VirtualBufer.Write(ChObj.Materials[i][j],2);
    end;

 end;


procedure  WriteChunkMaterial(Mat:TMaterial);
var i:word;
  begin
   WriteChunk(CHUNK_MATNAME,CHUNK_HEAD+Mat.NameLen);
     VirtualBufer.Write(Pchar(Mat.Name)^,Mat.NameLen);
   WriteChunk(CHUNK_MAT_DIFFUSE,15);//
     WriteChunk(CHUNK_RGB,9);
      VirtualBufer.Write(Mat.Color,3);
   WriteChunk(CHUNK_MAT_SPECULAR,15);//
     WriteChunk(CHUNK_RGB,9);
      VirtualBufer.Write(Mat.Color,3);
   WriteChunk(CHUNK_MAT_TRANSPARENCY,14);//
     WriteChunk(CHUNK_INT_PERCENTAGE,8);
      VirtualBufer.Write(Mat.Trans,2);
   if Textures then begin
   WriteChunk(CHUNK_TEXTURE,Mat.TextureLen);//
     WriteChunk(CHUNK_INT_PERCENTAGE,8);
     i:=50;      VirtualBufer.Write(i,2);
     WriteChunk(CHUNK_MAPFILE,CHUNK_HEAD+Mat.TextNameLen);
     VirtualBufer.Write(Pchar(Mat.TextName)^,Mat.TextNameLen);
   end;
//    CHUNK_TEXTURE         = 0xA200; // [-] �������� ���������
 //     CHUNK_MAPFILE       = 0xA300; // [+] ��� ����� ��������
  end;

   function GetLast(TreeChild:TtreeNode):TTreeNode;
   begin
    if TreeChild.GetLastChild=nil then result:=TreeChild
    else result:=(GetLast(TreeChild.GetLastChild));
   end;

begin

  VirtualBufer := TMemoryStream.Create;
   Texture3DS:=Textures;
// ��� ������� ���������������

// Test Vertex
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();  // ��������� ����
  glRotatef(90,1,0,0);
  SetLength(ModelData,0);
//  LoadPoxToMesh(ModelData,RootPoxel,TextureList);
  glLoadIdentity();  // ��������� �����       }
  SetLength(UniqVertArrayLayer,0);
  SetLength(UniqVertArray,0);
  SetLength(FaceArray,0);
  SetLength(TCoordArray,0);

{  Export3DModel.FullProcess.Caption:='Generating Materials';
  Export3DModel.ProgressMain.Min:=0;
  Export3DModel.Repaint;     }
  MatStrList:=TStringList.Create;

if Textures then   TgaFiles:=TStringList.Create;

  for i:=0 to Length(ModelData)-1 do
   with ModelData[i] do
    for j:=0 to length(MaterialsName)-1 do
        if (MatStrList.IndexOf(MaterialsName[j]) = -1) then
                MatStrList.Add(MaterialsName[j]);
  MatStrList.Sort;
  SetLength(MatData,MatStrList.Count);
{  Export3DModel.ProgressMain.Position:=0;
  Export3DModel.ProgressMain.Max:=MatStrList.Count;      }
  for i:=0 to MatStrList.Count-1 do
    begin
     MatData[i]:=TMaterial.Create;
     MatData[i].Name := MatStrList.Strings[i];
     Export3DModel.Repaint;
     MatData[i].Color := GetColor(MatStrList.Strings[i]);
    if Textures then  MatData[i].TextName := GetTGA(MatStrList.Strings[i]);
     MatData[i].Trans := 0;
    end;
  MatStrList.Free;

if Textures then    begin
{    Export3DModel.FullProcess.Caption:='Saving Textures';
    Export3DModel.ProgressMain.Position:=0;
    Export3DModel.ProgressMain.Max:=TgaFiles.Count;
    Export3DModel.Repaint;       }
  for i:=0 to TgaFiles.Count-1 do begin
{    Export3DModel.ProgressMain.Position:=i;
     Export3DModel.PoxelProcess.Caption:=format('Texture File: %s',
        [TgaFiles.Strings[i]]);
     Export3DModel.Repaint;    }
    SaveTgaNameToFile(TgaFiles.Strings[i],ExtractFileDir(FileName)+'\'+TgaFiles.Strings[i]+'.tga');
    end;
  TgaFiles.Free;
  end;

  MaterialLen:=0;
  for i:=0 to length(MatData)-1 do
        MaterialLen:=MaterialLen+ MatData[i].GetMaterialLen(Textures);

  MolelsLen:=0;
  for i:=0 to length(ModelData)-1 do
        MolelsLen:=MolelsLen+ ModelData[i].GetChunkObjLen(Textures);

  version_len := CHUNK_HEAD + 4;
  ObjMeshLen :=CHUNK_HEAD+version_len+ MaterialLen  + MolelsLen;
  KeyFramesLen:=0;
  main_len := CHUNK_HEAD+version_len + ObjMeshLen +KeyFramesLen;
  Export3DModel.ProgressMain.Max:=main_len;
 //-----------------
 WriteChunk(CHUNK_MAIN,main_len);
 //-----------------
 WriteChunk(CHUNK_VERSION,version_len);
 v :=CHUNK_3DSVERSION;   VirtualBufer.Write(v,4);
 //------object-----------
 WriteChunk(CHUNK_OBJMESH,ObjMeshLen);
  //-----------------
  WriteChunk(CHUNK_MESH_VERSION,version_len);
  v :=CHUNK_3DSVERSION;
  VirtualBufer.Write(v,4);
 //-------material----------
   for i:=0 to length(MatData)-1 do begin
   WriteChunk(CHUNK_MATERIAL,MatData[i].GetMaterialLen(Textures));
    WriteChunkMaterial(MatData[i]);
   end;
  //--------mesh---------
   for i:=0 to length(ModelData)-1 do
     WriteChunkObject(ModelData[i]);

// ����� ���������������

  VirtualBufer.SaveToFile(FileName);
  VirtualBufer.Free;
  Export3DModel.FullProcess.Caption:='Done';

  for i:=0 to Length(ModelData)-1 do
  ModelData[i].Free;
  for i:=0 to Length(MatData)-1 do
  MatData[i].Free;

  Result := true;
end;

const MaxSingle: single = 1E34;
GeomZero: single = 1E-5;


function Open3DSModel(FileName:string;var ObjArray3D:TObjArray3D):boolean;
var
  i,j:integer;
  VirtualBufer: TMemoryStream;
  ChankID, ChunkLength,ver:integer;
  Ch:char;Name:string;
  Obj3D:TObj3D;
  MaxVert:integer;
  Array_3DS:AVer;
  FaceArray_3DS:AFace;
  FaceCount_3DS,len:integer;
  ArrayIndex,IndxGroup:integer;
  LenChunk,k,n,g:integer;
  val:single;
  Matrix: TMatrix;
  TestTable:array of byte;
  MatrixJoin: array of array of byte;
  ConvertTable,Groups:array of integer;

  procedure    ReadChunk(var ID,Length:integer);
  begin
     ID:=0;
     VirtualBufer.ReadBuffer(ID, 2);
     VirtualBufer.ReadBuffer(Length, 4);
  end;

  function FindChunk(Chunk_ID:integer):boolean;
  begin
  Result:=false;
  while ChankID<>Chunk_ID do
    begin
    ReadChunk(ChankID,ChunkLength);
    if ChankID<>Chunk_ID then
    if VirtualBufer.Seek(ChunkLength-CHUNK_HEAD,soFromCurrent)=VirtualBufer.Size then exit;
    end;
  Result:=true;
  end;

  function GenChunkName():string;
  var
  s:String;
  begin
     S:='';
      Repeat
            VirtualBufer.ReadBuffer(Ch, 1);//Name of Object;
            S := S + Ch;
      until Ch = #0;
      result:=copy(s,0,Length(s)-1);
  end;

  function  TestFaceConnect(ii,jj:integer;FaceArr:AFace):boolean;
      var pt,ps,kk,rr,tt:integer;
       begin
       tt:= 0;
       for kk:=0 to 2 do  for rr:=0 to 2 do
                if (FaceArr[ii][kk] = FaceArr[jj][rr]) then inc(tt);
       result:=tt>0;
      end;

  procedure Grouping(i:Integer);
  var j:integer;
  begin
         Groups[i]:=IndxGroup;
         for j:=i+1 to FaceCount_3DS-1 do
                if (Groups[j]=0)and (MatrixJoin[i][j]=1) then Grouping(j);
         for j:=0 to i-1 do
                if (Groups[j]=0) and (MatrixJoin[j][i]=1) then Grouping(j);
  end;

begin
Result:=false;
// ������ ����, � ��� ��� ������������, ���� �� ��������� ������.
  VirtualBufer := TMemoryStream.Create;
  VirtualBufer.LoadFromFile(FileName);
  ReadChunk(ChankID,ChunkLength); // CHUNK_MAIN
//  if ChunkLength<>VirtualBufer.Size
  ReadChunk(ChankID,ChunkLength);//CHUNK_VERSION
  VirtualBufer.Read(ver,4);//version
  ReadChunk(ChankID,LenChunk);   // CHUNK_OBJMESH
  if ChankID<>CHUNK_OBJMESH then begin VirtualBufer.Free; exit; end;

//  VirtualBufer.Read(ver,4);//CHUNK_3DSVERSION

// �������
len:=Length(ObjArray3D);
for i:=0 to len-1 do
begin
ObjArray3D[i].PointsArr:=nil;
ObjArray3D[i].FaceArr:=nil;
ObjArray3D[i].Name:='';
end;

SetLength(ObjArray3D,100);
ArrayIndex:=0;

  for i:=1 to 4 do for j:=1 to 4 do
        Matrix[i][j]:=0;

  Matrix[1][1]:=1;
  Matrix[2][3]:=-1;
  Matrix[3][2]:=1;
  Matrix[4][4]:=1;
//LenChunk:=VirtualBufer.Position+LenChunk;
while //VirtualBufer.Position<LenChunk
   FindChunk(CHUNK_OBJBLOCK) //CHUNK_MATERIAL
     //������ �������
     do begin

     // ��� ������ ������ ����
     // ��������� ���
     ObjArray3D[ArrayIndex].Name:=GenChunkName();  //CHUNK_OBJBLOCK
 // Import3DModel.PoxelProcess.Caption := format('Load 3DS Object [%s]',[ObjArray3D[ArrayIndex].Name]);
    FindChunk(CHUNK_TRIMESH);
    FindChunk(CHUNK_VERTLIST);
     MaxVert:=0;
     VirtualBufer.ReadBuffer(MaxVert,2);
//     Array_3DS:=nil;
     SetLength(Array_3DS,MaxVert);
     VirtualBufer.ReadBuffer(Array_3DS[0],sizeof(TVer)*MaxVert);
// �������� �����
    ConvertTable:=nil;

     SetLength(ConvertTable,MaxVert);
    TestTable:=nil;
     SetLength(TestTable,MaxVert);
     for i:=0 to MaxVert-1 do ConvertTable[i]:=i;

     for i:=0 to MaxVert-1 do
        for j:=i+1 to MaxVert-1 do
          if (ConvertTable[j]=j) and
          CompareMem(@Array_3DS[i],@Array_3DS[j],sizeof(TVer))
          then ConvertTable[j]:=i;

     j:=0;
     for i:=0 to MaxVert-1 do
      if (i=ConvertTable[i]) then
      begin
      ConvertTable[i]:=j;
      TestTable[i]:=1;
      inc(j);
      end;

      SetLength(ObjArray3D[ArrayIndex].PointsArr,j);
      j:=0;
        with   ObjArray3D[ArrayIndex].SizeBox do begin
    Xmax := -MaxSingle;
    Xmin := MaxSingle;
    Ymax := -MaxSingle;
    Ymin := MaxSingle;
    Zmax := -MaxSingle;
    Zmin := MaxSingle;
    end;

     for i:=0 to MaxVert-1 do
     begin
      j:=ConvertTable[i];
//      if (TestTable[i]=0) then j:=ConvertTable[j];
      if (TestTable[i]=1) then
      With ObjArray3D[ArrayIndex].SizeBox, ObjArray3D[ArrayIndex]  do begin
      Array_3DS[i]:=MatrXVert(Matrix,Array_3DS[i]);
      val:=Array_3DS[i][0];
       PointsArr[j][0]:=val;
       IfMinMax(Xmin,Xmax,val);
      val:=Array_3DS[i][1];
       PointsArr[j][1]:=val;
       IfMinMax(Ymin,Ymax,val);
      val:=Array_3DS[i][2];
       PointsArr[j][2]:=val;
       IfMinMax(Zmin,Zmax,val);
       end;

     end;
     Array_3DS:=nil;
     // ������� ������ � ��������� � �������.
     // ����� ��������� ��� ���������� ��������
     // �������� �������
     // ������� ����� ������ �����
     // �������� �������

     // ������� ������ Face � �� ������� ��������.

   FindChunk(CHUNK_FACELIST); //CHUNK_FACELIST
   FaceCount_3DS:=0;
   VirtualBufer.ReadBuffer(FaceCount_3DS,2);

//   FaceArray_3DS:=nil;
   SetLength(FaceArray_3DS,FaceCount_3DS);
   VirtualBufer.ReadBuffer(FaceArray_3DS[0],Sizeof(TFace)*FaceCount_3DS);
// �������� ���
   SetLength(ObjArray3D[ArrayIndex].FaceArr,FaceCount_3DS);   //!!!!!!!!!!!!!!!!!!!!!!!

   for i:=0 to FaceCount_3DS-1 do
    for n:=0 to 2 do
        begin
        j:= ConvertTable[FaceArray_3DS[i][n]];
        if (TestTable[FaceArray_3DS[i][n]]=0) then j:=ConvertTable[j];
        ObjArray3D[ArrayIndex].FaceArr[i][n]:=j;
        end;
    FaceArray_3DS:=nil;
   // ����� ��������� ������ �����, ������� ������� ������
   // ������� ������� ����������, ���� ��������
   // ���������� � ������ ��� ������ �������.
   // ������� SizeBox
   // ��������� ������� �� ������, ������ �������� ����������� ���� �������.
//   IndxGroup:=1;
// �������������
   SetLength(MatrixJoin,0,0);
   SetLength(MatrixJoin,FaceCount_3DS, FaceCount_3DS);

   for i:=0 to FaceCount_3DS-1   do  begin
    for j:=i+1 to FaceCount_3DS-1   do
      if (MatrixJoin[i,j] = 0) and TestFaceConnect(i,j,ObjArray3D[ArrayIndex].FaceArr) then
      MatrixJoin[i][j]:=1;
   end;
//   SetLength(Groups,0);
   Groups:=nil;
   SetLength(Groups,FaceCount_3DS);
   IndxGroup:=0;
   for i:=0 to FaceCount_3DS-1 do
         if (Groups[i]=0) then begin
         inc(IndxGroup);
         Grouping(i);
         end;
   SetLength(MatrixJoin,0,0);
// �������������
   if IndxGroup<>1 then begin
         Obj3D:=ObjArray3D[ArrayIndex];
         Obj3D.Name:=ObjArray3D[ArrayIndex].Name;
         Obj3D.PointsArr:=Copy(ObjArray3D[ArrayIndex].PointsArr);
         ObjArray3D[ArrayIndex].PointsArr:=nil;
         Obj3D.FaceArr:=Copy(ObjArray3D[ArrayIndex].FaceArr);
         ObjArray3D[ArrayIndex].FaceArr:=nil;
         end;

   for g:=1 to IndxGroup do  // ���� �� �������
        begin
        if IndxGroup<>1 then  begin   // ���� ����� ������ �����

        ObjArray3D[ArrayIndex].Name:=Format('%s_Group_%d',[Obj3D.Name,g]);
        SetLength(ObjArray3D[ArrayIndex].FaceArr,FaceCount_3DS);
        // �������������� ������� �������
        for i:=0 to MaxVert-1 do ConvertTable[i]:=-1;

        j:=0; k:=0;
        for i:=0 to FaceCount_3DS-1 do  if (g=Groups[i]) then
        begin  // ����� ���� �� ���� ������
         ObjArray3D[ArrayIndex].FaceArr[j]:=Obj3D.FaceArr[i];
         // �������� ��� � ������ �� �������
         for n:=0 to 2 do   // ��������� ������� ��������
         if ConvertTable[Obj3D.FaceArr[i][n]]=-1 then
         begin  // ��������� ����� ����� �����������
         ConvertTable[Obj3D.FaceArr[i][n]]:=k;
         ObjArray3D[ArrayIndex].FaceArr[j][n]:=k;
         inc(k);
         end else  // ��������� ���������� ����� �����������
         ObjArray3D[ArrayIndex].FaceArr[j][n]:=ConvertTable[Obj3D.FaceArr[i][n]];

        inc(j);
        end;
        SetLength(ObjArray3D[ArrayIndex].FaceArr,j);
        // �������� �����������
//        SetLength(Groups,0);
        SetLength(ObjArray3D[ArrayIndex].PointsArr,k);
        // �������� �����

          with   ObjArray3D[ArrayIndex].SizeBox do begin
                Xmax := -MaxSingle;
                Xmin := MaxSingle;
                Ymax := -MaxSingle;
                Ymin := MaxSingle;
                Zmax := -MaxSingle;
                Zmin := MaxSingle;
          end;


        for i:=0 to k-1 do  // ����������� �����
        for j:=0 to MaxVert-1 do
         if ConvertTable[j]=i then
           begin // ���� ����� ���� � ����� ������� ��
                 With ObjArray3D[ArrayIndex].SizeBox, ObjArray3D[ArrayIndex] do begin
                val:=Obj3D.PointsArr[j][0];
                PointsArr[i][0]:=val;
                IfMinMax(Xmin,Xmax,val);
                val:=Obj3D.PointsArr[j][1];
                PointsArr[i][1]:=val;
                IfMinMax(Ymin,Ymax,val);
                val:=Obj3D.PointsArr[j][2];
                PointsArr[i][2]:=val;
                IfMinMax(Zmin,Zmax,val);
                end;
           end;

        end;
   Inc(ArrayIndex);
   if  Length(ObjArray3D)=ArrayIndex then SetLength(ObjArray3D,ArrayIndex+100);
        end;
   // ��������� ��������� ������, ��������� ��� � ������ ��������.
   // objArray[].Name, .PointsArr, .FaceArr, .SizeBox
   // ����� ������������ ������������ �������, ����� �� ������� ������
end;
   SetLength(ObjArray3D,ArrayIndex);
// find CHUNK_OBJMESH, CHUNK_OBJBLOCK ,CHUNK_TRIMESH,CHUNK_VERTLIST,CHUNK_FACELIST
    VirtualBufer.Free;
Result:=true;
end;                    *)


end.
