unit xomview_2_7;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, ExtCtrls, ValEdit, OpenGLx, OpenGL_Box,
  OpenGLLib,Clipbrd,
  Buttons, Menus, IdGlobal, Math, ImgList, XomLib, XTrackPanel,GraphUtil,
  ToolWin;

type
  TFormXom = class(TForm)
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    TreeView1: TTreeView;
    PanelView: TPanel;
    StringsTable: TValueListEditor;
    Panel2: TPanel;
    RichEdit1: TRichEdit;
    StatusBar1: TStatusBar;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    ImageT32: TImage;
    ScrollBox1: TScrollBox;
    Button3: TButton;
    SaveDialog2: TSaveDialog;
    Panel3: TPanel;
    OpenGLBox: TOpenGLBox;
    Timer1: TTimer;
    RotateBut: TSpeedButton;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    ClassTree: TTreeView;
    XLabel: TLabel;
    Panel5: TPanel;
    TreeLabel: TLabel;
    Button4: TButton;
    SaveXom3D: TSaveDialog;
    Button5: TButton;
    Button6: TButton;
    OpenDialog2: TOpenDialog;
    SaveDialog3: TSaveDialog;
    ButSaveXom: TButton;
    OpenDialog3: TOpenDialog;
    PopupMenu1: TPopupMenu;
    Expand1: TMenuItem;
    Collupse1: TMenuItem;
    ExportAnim: TButton;
    SaveAnim: TSaveDialog;
    Panel6: TPanel;
    Panel7: TPanel;
    AnimBox: TComboBox;
    ImportAnim: TButton;
    LabelTime: TLabel;
    Panel8: TPanel;
    Panel9: TPanel;
    XImageLab: TLabel;
    Panel10: TPanel;
    Panel11: TPanel;
    AnimButton: TSpeedButton;
    DrawBoxes: TSpeedButton;
    DrawBones: TSpeedButton;
    TreeImages: TImageList;
    Panel3D: TPanel;
    Panel12: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    TrackPanel1: TTrackPanel;
    EditAnim: TSpeedButton;
    ImageList1: TImageList;
    Panel13: TPanel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    Panel14: TPanel;
    ShowXTypes: TSpeedButton;
    StringsMenu: TPopupMenu;
    EditST: TMenuItem;
    AddNewST: TMenuItem;
    StringST: TMenuItem;
    N1: TMenuItem;
    ShowDummy: TSpeedButton;
    TreeView2: TTreeView;
    Splitter1: TSplitter;
    LoadAnim: TOpenDialog;
    AnimMenu: TPopupMenu;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    AnimTreeMenu: TPopupMenu;
    Add1: TMenuItem;
    Delete1: TMenuItem;
    AddElement1: TMenuItem;
    DelElement1: TMenuItem;
    Button7: TButton;
    Button8: TButton;
    OpenDialog4: TOpenDialog;
    SaveDialog4: TSaveDialog;
    ChkWUM: TCheckBox;
    Changevalue1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure OpenGLBoxResize(Sender: TObject);
    procedure OpenGLBoxPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ButSaveXomClick(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Expand1Click(Sender: TObject);
    procedure Collupse1Click(Sender: TObject);
    procedure ExportAnimClick(Sender: TObject);
    procedure ScrollBox1Resize(Sender: TObject);
    procedure AnimBoxChange(Sender: TObject);
    procedure ImportAnimClick(Sender: TObject);
    procedure OpenGLBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure OpenGLBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure OpenGLBoxMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure OpenGLBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TrackPanel1DrawGraph(Canvas: TCanvas; Rect: TRect; XDiv,
      YDiv: Single);
    procedure EditAnimClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ToolButtonSMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);

    procedure OpenGLBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLBoxClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ShowXTypesClick(Sender: TObject);
    procedure StringsMenuPopup(Sender: TObject);
    procedure StringSTClick(Sender: TObject);
    procedure EditSTClick(Sender: TObject);
    procedure AddNewSTClick(Sender: TObject);
    procedure StringsTableSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure StringsTableSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ToolButton11Click(Sender: TObject);
    procedure TrackPanel1Paint(Sender: TObject);
    procedure UpdateGraph;
    procedure TrackPanel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TrackPanel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackPanel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ClassTreeCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure TreeView2Changing(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure TreeView2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackPanel1DblClick(Sender: TObject);
    procedure TreeView2DblClick(Sender: TObject);
    procedure DrawBonesClick(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure TrackPanel1ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure AddElement1Click(Sender: TObject);
    procedure DelElement1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Changevalue1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  
  private
    { Private declarations }
  public
    { Public declarations }
    sortcount: Integer;
    maxst, MaxCount: Integer;
    LoadXom: Boolean;
    StPanel, StPanel2: TStatusPanel;
    procedure InitGLOptions;
    procedure OpenGLGenObject;
    procedure display;
    procedure Zoomer(Step: Extended);
    function DoSelect(x: GLInt; y: GLInt): GLint;
  end;

const
  crZoom   = 1;
  crPan    = 2;
  crRotXY  = 3;
  crPer    = 4;
  crDolly  = 5;
  crMove   = 6;
  crRotate = 9;
  crSelect = 8;
  crRotZ   = 7;
  crScale  = 10;
  crUpDown = 11;
  crFill   = 12;
  crGet    = 13;
  crErase  = 14;
  crOpenHand = 15;
  crClosedHand  = 16;

  MaxSelect = 500;     // ������������ ���������� �������� ��� ��������
  MaxDeph   = 100000.0;  // ������������ �������

  BrdScr = 16;
var

  TempCur: TCursor;
  Scrn: TPoint;
  FormXom: TFormXom;
  TempX, TempY: Integer;
  ToolButTemp, ToolButTemp2: TToolButton;
  GpMove,TimeDrag:Boolean;
  FdTime,Fddiv:Single;

  selectBuf: array [0..MaxSelect * 4] of GLInt;
implementation

uses ImportComp, AnimF,AnimEditForm,ValueForm;

{$R *.dfm}

procedure TFormXom.Button1Click(Sender: TObject);
var 
  s: string;
begin
  if OpenDialog1.Execute then
  begin
    try
      LoadXom := false;
      WUM:= ChkWUM.Checked;
      Xom.LoadXomFileName(OpenDialog1.FileName, s);
      Caption := s;
    finally
      XomImport := Xom.saidx = Xom.Conteiners;
      //        RichEdit1.Text:=sTemp;
      TreeView1.Visible := true and ShowXTypes.Down;
      TreeView1.Select(Xom.BaseNode);
      LoadXom := true;
      ButSaveXom.Enabled := false;
      //    FreeMem(VBuf);
    end;
  end;
end;

procedure TFormXom.ExportAnimClick(Sender: TObject);
var
 // inx: Integer;
  s: string;
begin
//  Application.MessageBox('This version for test.', 'Testing', MB_OK);
//  Exit;
//  inx := AnimBox.ItemIndex;
  s := ExtractFileName(OpenDialog1.FileName);
  SaveAnim.FileName := Format('%s[%s].xac',
    [s, CurAnimClip.Name]);
  if SaveAnim.Execute and (SaveAnim.FileName <> '') then
    CurAnimClip.SaveXAnimClip(SaveAnim.FileName);
end;

const
  MaxSingle: Single = 1E34;

procedure TFormXom.TreeView1Change(Sender: TObject; Node: TTreeNode);
var 
  i,k: Integer;
//  NodeTemp: TTreeNode ;
begin
  i := Integer(Node.Data);
  if (TreeView1.Selected.Level = 1) then
  if LoadXom then begin
        k:=0;
      //  ClassTree.Visible := false;
      //  ClassTree.Items.GetFirstNode.Collapse(true);
        while integer(ClassTree.Items[k].Data)<>i do  inc(k);
        ClassTree.Select(ClassTree.Items[k]);
      //  ClassTree.Visible := true;
        end
  else
  begin
    // clear
    ClassTree.Items.Clear;
    ClassTree.Visible := false;
    Xom.TreeArray     := nil;
    SetLength(Xom.TreeArray, Xom.Conteiners + 1);
    Xom.TreeCount := 0;
    Xom.AddClassTree(i, ClassTree, nil);
    //for j:=1 to Conteiners do
    ///        if TreeArray[j] then inc(TreeCount);
    TreeLabel.Caption := Format('%.1f %%', [(Xom.TreeCount) / Xom.Conteiners * 100]);
    ClassTree.Items.GetFirstNode.Expand(false);
    //ClassTree.FullExpand;
{MaxTree:=ClassTree.Items.Count;
NodeTemp:=ClassTree.Items.GetFirstNode;
while NodeTemp<>nil do begin
        if NodeTemp.Level<2 then
        begin
        Panel4.Caption:=format('%.2f %%',[NodeTemp.AbsoluteIndex/MaxTree*100]);
        Panel4.Repaint;
        NodeTemp.Expand(False);
        NodeTemp:=NodeTemp.GetNext;
        end else
        NodeTemp:=NodeTemp.getNextVisible;
        end;    }
    ClassTree.Select(ClassTree.Items.GetFirstNode);
    ClassTree.Visible := true;
  end;

//  StPanel.Text :=  ClassTree.Selected.Text;
end;


procedure TFormXom.FormCreate(Sender: TObject);
var
  Stream: TCustomMemoryStream;
begin
  Caption := APPVER;
  Xom := TXom.Create;
  But.GlValueList := StringsTable;
  AddClick:=AddElement1Click;
  DeleteClick:=DelElement1Click;
  But.TreeViewM := TreeView1;
  TreeView1.Constraints.MinWidth:=200;
//  But.ValueList := StringsTable;
  But.AnimButton := @AnimButton.Down;
  But.EditAnimBut := @EditAnim.Down;
  But.TrackPanel:=TrackPanel1;
  But.DrawBoxes := @DrawBoxes.Down;
  But.AnimBox := AnimBox;
  But.RotateButton := @RotateBut.Down;
  //But.JointButton := Joint;
  But.XLabel := XLabel;
  But.XImageLab := XImageLab;
  But.TreeLabel := TreeLabel;
  But.ClassTree := ClassTree;
  But.XImage := ImageT32;
  But.Handle := Handle;
  But.Dummy := @ShowDummy.Down;
  But.Canvas := Canvas;
  But.Move := @ToolButton8.Down;
  But.Rotate := @ToolButton9.Down;
  But.Scale := @ToolButton10.Down;

  StPanel := StatusBar1.Panels[0];
  But.StatusM := StPanel;
  //StPanel.Width:=250;
  StPanel2 := StatusBar1.Panels[1];
  But.Status := StPanel2;
  //StPanel2.Width:=50; }
  RichEdit1.Clear;
  RichEdit1.defAttributes.Color := clBlack;
  //RichEdit1.defAttributes.Style := [fsBold];
  Stream := TResourceStream.Create(hInstance, 'Dummy', 'TGA');
  bBitPoint2 := AllocMem(Stream.Size);
  Move(Stream.Memory^, bBitPoint2^, Stream.Size);
  Inc(Integer(bBitPoint2), 18);
  Stream.Free;
  // ��������� ������� ������
  Screen.Cursors[crSelect] := LoadCursor(HInstance, 'SELECT');
  Screen.Cursors[crMove] := LoadCursor(HInstance, 'MOVE');
  Screen.Cursors[crRotate] := LoadCursor(HInstance, 'ROTATE');
  Screen.Cursors[crScale] := LoadCursor(HInstance, 'SCALE');
  Screen.Cursors[crPan] := LoadCursor(HInstance, 'PAN');
  Screen.Cursors[crZoom] := LoadCursor(HInstance, 'ZOOM');
  Screen.Cursors[crDolly] := LoadCursor(HInstance, 'ZOOM2');
  Screen.Cursors[crPer] := LoadCursor(HInstance, 'PER');
  Screen.Cursors[crRotXY] := LoadCursor(HInstance, 'ROTXY');
  Screen.Cursors[crRotZ] := LoadCursor(HInstance, 'ROTZ');
  Screen.Cursors[crFill] := LoadCursor(HInstance, 'FILL');
  Screen.Cursors[crGet] := LoadCursor(HInstance, 'GETFILL');
  Screen.Cursors[crErase] := LoadCursor(HInstance, 'ERASE');
  Screen.Cursors[crOpenHand] := LoadCursor(HInstance, 'OPENHAND');
  Screen.Cursors[crClosedHand] := LoadCursor(HInstance, 'CLOSEDHAND');
  //Screen.Cursors[crUpDown]:=LoadCursor(HInstance, 'UPDOWN');
  // ��������� ������ � GL � �������������� ���
  OpenGLBox.GL_Font := FontGL;
    // ������ ���������� ������
  ToolButton11.Down := true;
  ToolButTemp := ToolButton11;
  SelectObj := 0;
  // ��������� �������� ������ � �����
  TempCur := OpenGLBox.Cursor;
  Scrn.X := screen.DesktopRect.Right - 1;
  Scrn.Y := screen.DesktopRect.Bottom - 1;
  
  OpenGLBox.glBoxInit;
  InitGLOptions;
  OpenGLGenObject;
end;

procedure TFormXom.OpenGLGenObject;
begin
  // ������� ������ ���� XY
  glNewList(objAxes, GL_COMPILE);
  oglAxes;
  glEndList;

  // ������� �����
  glNewList(objGrid, GL_COMPILE);
  oglGrid(16);
  glEndList;
end;

procedure TFormXom.InitGLOptions;
begin
  glEnable(GL_DEPTH_TEST);
  glAlphaFunc(GL_GREATER, 0.8);
  glEnable(GL_COLOR_MATERIAL);
  glShadeModel(GL_SMOOTH);
  // glEnable(GL_NORMALIZE);
  //  glEnable(GL_CULL_FACE);
  glEnable(GL_AUTO_NORMAL);
  // ��������� �����
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glPolygonOffset(1.0, 1.0);
  glLightfv(GL_LIGHT0, GL_AMBIENT, @ambient);
  glLightfv(GL_LIGHT0, GL_POSITION, @l_position);
  // ��������� �����
  glMaterialfv(GL_FRONT, GL_DIFFUSE, @mat_diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, @mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, @mat_shininess);

  // glSelectBuffer(SizeOf(selectBuf), @selectBuf);  // �������� ������ ������
  // �������  TransView
  // zeromemory(@TransView, SizeOf(TransView));

  TransView.xrot := -20.0;
  TransView.yrot := 136.0;
  TransView.Per := 35.0;
  TransView.zoom := 50.0;
  AnimTimer := THRTimer.Create;
end;

// ������� ��������� ��� �� ������
function TFormXom.DoSelect(x: GLInt; y: GLInt): GLint;
var
  SelObject: GLInt;
begin
//������ ������������ ����� ������
  PSelect.x := x;
  PSelect.y := y;
  SelectMode:=True;
display;//(true);
 SelectMode:=False;
glReadBuffer(GL_BACK);
SelObject:=0;
glReadPixels(0, 0, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, @SelObject);
//StatusBar1.Panels[1].text := format('%d',[SelObject]);
Result:=selObject;
end;

procedure TFormXom.display();
var
    vp: TVector4i;
 //   fi,fj,
    fzoom:Glfloat;
    PMatrix, MvMatrix: TGLMatrixd4;
  wdx, wdy, wdz,
  wtx, wty, wtz,
  wtx2, wty2, wtz2: GLdouble;
label
    Move,Jump;
begin
  // ��������������� �����
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();// ��������� ��������� �������
  glViewport(0, 0, GLwidth, GLheight);

    if SelectMode then
  begin  // ���� ����� ���������
    glGetIntegerv(GL_VIEWPORT, @vp);
    glLoadIdentity;
    // ��������� ������� ���������
    gluPickMatrix(PSelect.x, GLHeight - PSelect.y - 4, 2, 2, vp);
    glViewport(0, 0, 1, 1);
  end;
  //  glGetFloatv(GL_PROJECTION_MATRIX, @PrMatrix);

  //  TestMinMax(10.0, 150.0, TransView.Per);
  // ������� �����������

  gluPerspective(TransView.Per, GLwidth / GLheight, TransView.zoom / 50, MaxDeph);
  if TransView.yrot > 360.0 then
    TransView.yrot := TransView.yrot - 360;
  if TransView.yrot<-360.0 then
    TransView.yrot := TransView.yrot + 360;
  // ������� ������
  gluLookAt(0, 0, - TransView.zoom, 0, 0, 0, 0, 1, 0);
  glRotatef(TransView.xrot, 1, 0, 0);
  glRotatef(TransView.yrot, 0, 1, 0); // �������� ����
  glTranslatef(TransView.xpos, TransView.ypos, TransView.zpos);
  //  glGetFloatv(GL_PROJECTION_MATRIX,@PrMatrix);
  //  WPar:=PrMatrix[4,4];
  // ����� ������
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();  // ��������� �����
  //  glClearDepth(0.0);
  glDepthMask(GL_TRUE);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  //  Light
  Light(position2);
  glLightfv(GL_LIGHT0, GL_POSITION, @position2);
  glDisable(GL_LIGHTING);
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_ALPHA_TEST);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
    if SelectMode then  begin
      glPushMatrix;
      if Xom.Mesh3D <> nil then Xom.Mesh3D.Draw(DrSelect);
      glPopMatrix;
      goto Move;
    end;
  // Grid
  //  glPushMatrix;
  glColor3f(0.5, 0.5, 0.5);
  glCallList(objGrid);
  //  glPopMatrix;

  // glEnable(GL_LIGHTING);

  // ---------------
  if glIsList(objCollGeo) then begin
  glPushMatrix;

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glColor3f(1.0, 1.0, 1.0);
  glCallList(objCollGeo);
  //    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  // 
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glPushAttrib(GL_ENABLE_BIT);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
  glcolor4f(1.0, 1.0, 1.0, 0.8);
  glCallList(objCollGeo);

  glPopAttrib;

  glPopMatrix;
  end;
  //-------------------
  glEnable(GL_LIGHTING);
  glPushMatrix;
{  if  (Xom.Mesh3D <> nil) and AnimReady and But.AnimButton.Down and (CurAnimClip <> nil)   then
  begin
  // ������ ��������
      Xom.Mesh3D.Draw(DrGenAnim);
  end;}
  //    If mainBox.Xmax>10000 then glScalef(0.001,0.001,0.001);
  if Xom.Mesh3D <> nil then
  begin
    LastTexture:= 0;
    Xom.Mesh3D.Draw(DrMesh);
    Xom.Mesh3D.Draw(DrBlend);
    glDisable(GL_DEPTH_TEST);
    if DrawBones.Down then
      Xom.Mesh3D.Draw(DrBone);

  glClear( GL_DEPTH_BUFFER_BIT);
  glEnable(GL_POINT_SMOOTH);
    if EditAnim.Down then
      Xom.Mesh3D.Draw(DrBoxes);

 //    glEnable(GL_DEPTH_TEST);
  glDisable(GL_POINT_SMOOTH);
  end;
  //
  glPopMatrix;
  //*********************
  glDisable(GL_LIGHTING);

Move:
     glClear(GL_DEPTH_BUFFER_BIT);

    fzoom:=TransView.zoom/15;

    if SelectMode and not MoveMode then else
    if ToolButton8.Down or (ToolButton3.Down and(ToolButton8=ToolButTemp2))  then begin
    glLoadMatrixf(@ObjMatrix);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glScalef(fzoom, fzoom, fzoom);
    oglDynAxes(AAxis,mMove,SelectMode,false);
    glPopAttrib;
    goto Jump;
    end;

    if ToolButton9.Down or (ToolButton3.Down and(ToolButton9=ToolButTemp2)) then begin
    glLoadMatrixf(@ObjMatrix);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glScalef(fzoom, fzoom, fzoom);

    // �������� ���������� ��������� ����������� ������
  glGetIntegerv(GL_VIEWPORT, @vp);
  glGetDoublev(GL_MODELVIEW_MATRIX, @MvMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, @PMatrix);
  gluProject(0, 0, 0, MvMatrix, PMatrix, vp,
    @wdx, @wdy, @wdz);
  gluUnProject(wdx, wdy, wdz, MvMatrix, PMatrix, vp,
      @wtx, @wty, @wtz);
  gluUnProject(wdx, wdy, -1, MvMatrix, PMatrix, vp,
      @wtx2, @wty2, @wtz2);
    eqn[0]:=wtx2-wtx;
    eqn[1]:=wty2-wty;
    eqn[2]:=wtz2-wtz;
    glGetFloatv(GL_MODELVIEW_MATRIX,@MatrixRot);
    oglDynAxes(AAxis,mRotate,SelectMode);
    glPopAttrib;
    goto Jump;
    end;

    if ToolButton10.Down or (ToolButton3.Down and(ToolButton10=ToolButTemp2))  then begin
    glLoadMatrixf(@ObjMatrix);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glScalef(fzoom, fzoom, fzoom);
    oglDynAxes(AAxis,mScale,SelectMode,false);
    glPopAttrib;
    end;

Jump:
if not SelectMode then begin
  // Axes X,Y,Z   -  ������ ��� ����� ����� ��� �������
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, GLwidth div 7, GLheight div 7);
  gluPerspective(20, GLwidth / GLheight, 5, 20);
  gluLookAt(0, 0, - 10, 0, 0, 0, 0, 1, 0);
  glRotatef(TransView.xrot, 1, 0, 0);
  glRotatef(TransView.yrot, 0, 1, 0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glCallList(objAxes);
  // End Axes
  // ������� �� ���� ��� ���������� �� ������ �� �����
  SwapBuffers(OpenGLBox.GL_DC);
end;

end;


procedure TFormXom.Button2Click(Sender: TObject);
var
  VirtualBufer: TMemoryStream;
  i: Integer;
begin
  i := Integer(ClassTree.Selected.Data);
  if i <> 0 then 
  begin
    SaveDialog1.FileName := Format('(%d).bin', [i]);

    if SaveDialog1.Execute then
      if SaveDialog1.FileName <> '' then 
      begin
        VirtualBufer := TMemoryStream.Create;
        VirtualBufer.Write(StrArray[i].point^, StrArray[i].size);
        VirtualBufer.SaveToFile(SaveDialog1.FileName);
        VirtualBufer.Free;
      end;
  end;
end;

procedure TFormXom.Button3Click(Sender: TObject);
var
  i: Integer;
  s: string;
begin
  s := ClassTree.Selected.Text;
  Delete(s, 1, Pos('"', s));
  Delete(s, Pos('.tga"', s), 5);
  Delete(s, Pos('.TGA"', s), 5);
  Delete(s, Pos('"', s), 1);
  for i := 0 to Length(s) do
    if (s[i] = '/') or (s[i] = ':') or (s[i] = '\') then 
      s[i] := '_';

  SaveDialog2.FileName := s + '.tga';
  if SaveDialog2.Execute then
    if (SaveDialog2.FileName <> '') then
      SaveTGA(SaveDialog2.FileName, ImageT32, Integer(ClassTree.Selected.Data),
        XLabel);
end;

procedure TFormXom.OpenGLBoxResize(Sender: TObject);
begin
  GLwidth := OpenGLBox.Width;
  GLheight := OpenGLBox.Height;
end;

procedure TFormXom.OpenGLBoxPaint(Sender: TObject);
begin
 display;
end;

procedure TFormXom.Timer1Timer(Sender: TObject);
begin
  if RotateBut.Down then
    TransView.yrot := TransView.yrot + 0.2;
  if AnimReady then
    AnimTimer.ReadTimer;
  If FormXom.Active then OpenGLBox.Repaint;
end;

procedure TFormXom.ClassTreeChange(Sender: TObject; Node: TTreeNode);
var
  i, k,size, i3d: Integer;
  s, Str: string;
  p: Pointer;
  MaxZoom: Single;

        function  FindXAnimClip(FNode:TTreeNode;var m3D:integer):Boolean;
        begin
        Result:=False;
        If FNode.Level=0 then exit;
        If StrArray[Integer(FNode.Data)].Xtype=XAnimClipLibrary then
                begin
                Result:=True;
                m3D:=Integer(FNode.Parent.Parent.Data);
                end
                else Result:=FindXAnimClip(FNode.Parent,m3D);
        end;

begin
  i := Integer(Node.Data);
      // select View Tab
    if PageControl1.ActivePageIndex<>2 then
    case Node.ImageIndex of
    1,3:PageControl1.ActivePageIndex:=1;
    2,4,5,12,13,15,19:PageControl1.ActivePageIndex:=0;
    end;

  if (i = 0) and (Node.getFirstChild <> nil) and (Node.getFirstChild.ImageIndex=20) then
  begin
    Node := Node.getFirstChild;
    i    := Integer(Node.Data);
  end;

  if (i > 0) and (i<10000) then
  begin
    // clear
    ImageT32.Picture.Bitmap.Width  := 0;
    ImageT32.Picture.Bitmap.Height := 0;
    AnimBox.Clear;
    LabelTime.Caption := Format('%.2f sec', [0.0]);
    Xom.Mesh3D.glClearCollGeo();
    Xom.Mesh3D.Free;
    Xom.Mesh3D := nil;

    //NTexture := 0;
    //Material.use:=false;
    //ClassTree.Items.Clear;

    with   MainBox do
    begin
      Xmax := -MaxSingle;
      Xmin := MaxSingle;
      Ymax := -MaxSingle;
      Ymin := MaxSingle;
      Zmax := -MaxSingle;
      Zmin := MaxSingle;
    end;
    p   := StrArray[i].point;
    Str := '';
    size := StrArray[i].size;
    if size>10000 then size :=10000;
    for k := 0 to size - 1 do
    begin
      if ((k + 1) mod 8) = 0 then
        s := nVn
      else 
        s := ' ';
      Str := Str + Format('%.2x%s', [Byte(Pointer(Longword(p) + k)^), s]);
    end;
    RichEdit1.Text := Str;
    ImageReady     := false;
    Xom3DReady     := false;
    AnimReady      := false;
    StPanel2.Text  := IntToStr(i);
    ShowGraph      := false;
    // ���� �������� XAnimClipLibrary ���� , �� ���������� Mesh.
    if FindXAnimClip(Node,i3d) then i:=i3d;
    Xom.SelectClassTree(i, Xom.Mesh3D);
    ScrollBox1Resize(self);
    Button2.Enabled := true;
    Button3.Enabled := ImageReady;
    Button6.Enabled := XomImport and Xom3DReady;
    Button5.Enabled := XomImport and ImageReady;
    Button4.Enabled := Xom3DReady;
    ImportAnim.Enabled := AnimReady;
    ExportAnim.Enabled := AnimReady;
    EditAnim.Enabled := AnimReady;
    Panel1.Visible:= AnimReady;
    Splitter2.Visible:= AnimReady;
    if AnimReady then
      AnimBoxChange(self);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    if Xom.Mesh3D <> nil then
      with MainBox do
      begin
        Xom.Mesh3D.GetBox(MainBox);
        TransView.Xpos := -(XMax + Xmin) / 2;
        TransView.Ypos := -(YMax + Ymin) / 2;
        TransView.Zpos := -(ZMax + Zmin) / 2;
        MaxZoom := Max((Xmax - Xmin), (Ymax - Ymin));
        MaxZoom := Max(MaxZoom, (Zmax - Zmin));
        TransView.zoom := MaxZoom * 3;
        TransView.xrot := -20.0;
        TransView.Per := 35.0;
        if XMax > 8000 then  //sky
        begin
          TransView.Xpos := 0;
          TransView.Ypos := -86;
          TransView.Zpos := 0;
          TransView.zoom := 100;
          TransView.xrot := 30;
          TransView.Per := 90;
          //   glDisable(GL_DEPTH_TEST);
          //   glEnable(GL_CULL_FACE);
        end;
        TVModel:=TransView;
      end;
  end;
    StPanel.Text :=  Node.Text;
end;

procedure TFormXom.Button4Click(Sender: TObject);
var 
  s: string;
  i: Integer;
begin
 // Application.MessageBox('This version for test.', 'Testing', MB_OK);
 // Exit;

  s := Xom.Mesh3D.Name;
  for i := 0 to Length(s) do
    if (s[i] = '/') or (s[i] = ':') or (s[i] = '\') then 
      s[i] := '_';

  SaveXom3D.FileName := Format('(%s).xom3d', [s]);
  if SaveXom3D.Execute and (SaveXom3D.FileName <> '') then
  begin
    Xom.Mesh3D.SaveAsXom3d(SaveXom3D.FileName);
  end;
end;

procedure TFormXom.Button5Click(Sender: TObject);
begin
  if OpenDialog2.Execute and (OpenDialog2.FileName <> '') then
  begin
    ButSaveXom.Enabled := ImportXImage(Xom.LastXImageIndex, OpenDialog2.FileName);
    ClassTreeChange(ClassTree, ClassTree.Items.GetFirstNode);
  end;
end;

procedure TFormXom.ButSaveXomClick(Sender: TObject);
begin
  FormXom.SaveDialog3.FileName := FormXom.OpenDialog1.FileName;
  Xom.SaveXom(SaveDialog3, StrArray);
  Caption := Format('%s - [%s]', [APPVER,ExtractFileName(SaveDialog3.FileName)]);
end;

  //var
  //ColorUse:boolean;

procedure TFormXom.Button6Click(Sender: TObject);
var
  VirtualBufer: TMemoryStream;
  s: string;
  //  i: Integer;
  MeshXom: TMesh;
  // XTexture:integer;
  XTextureStr: TStringList;
begin
  Application.MessageBox('Only in Full Version!!!', 'Testing', MB_OK);
  Exit;
  if OpenDialog3.Execute and (OpenDialog3.FileName <> '') then
  begin
    // read Mesh
    VirtualBufer := TMemoryStream.Create;
    VirtualBufer.LoadFromFile(OpenDialog3.FileName);
    XTextureStr := TStringList.Create;
    ImportComp.CompareTree.InTree.Items.Clear;
    ImportComp.CompareTree.OutTree.Items.Clear;
    //    ColorUse:=false;
    S := ReadName(VirtualBufer);
    if s <> 'X3D' then 
    begin
      MessageBox(FormXom.Handle,
        PChar('Bad format Xom3D!!!'),
        PChar('Error'), MB_OK);
      VirtualBufer.Free;
      Exit;
    end;
    MeshXom := TMesh.Create;
    MeshXom.ReadXNode(VirtualBufer, XTextureStr,
      ImportComp.CompareTree.InTree, nil);
    BuildTree(ClassTree, ImportComp.CompareTree.OutTree, ClassTree.Selected, nil);
    ImportComp.CompareTree.OutTree.FullExpand;
    ImportComp.CompareTree.InTree.FullExpand;
    VirtualBufer.Free;
    // compare treees
    if ImportComp.CompareTree.ShowModal = mrOk then
    begin
      Xom.XomImportObj(ImportComp.CompareTree.InTree.Items.GetFirstNode,
        ImportComp.CompareTree.OutTree.Items.GetFirstNode);
      ButSaveXom.Enabled := true;
    end;
    // import
    MeshXom.Free;
    XTextureStr.Free;
    ClassTreeChange(ClassTree, ClassTree.Items.GetFirstNode);
  end;
end;

procedure TFormXom.Expand1Click(Sender: TObject);
begin
  if ClassTree.Selected <> nil then
    ClassTree.Selected.Expand(true);
end;

procedure TFormXom.Collupse1Click(Sender: TObject);
begin
  if ClassTree.Selected <> nil then
    ClassTree.Selected.Collapse(true);
end;

procedure TFormXom.ScrollBox1Resize(Sender: TObject);
begin
  ImageT32.Left := FormXom.Scrollbox1.Width div 2 - ImageT32.Width div 2;
  ImageT32.Top := FormXom.Scrollbox1.Height div 2 - ImageT32.Height div 2;
  if ImageT32.Left < 0 then 
    ImageT32.Left := 0;
  if ImageT32.Top < 0 then 
    ImageT32.Top := 0;
end;

procedure TFormXom.UpdateGraph;
  var MaxValue,MinValue:Single;
  i,ii,j,jj:integer;
begin
  if CurAnimClip=nil then exit;
  AnimEd:=true;
  TrackPanel1.AreaSize.MaxWidth:= CurAnimClip.Time;
  j:=Length(CurAnimClip.Keys)-1;
  MaxValue:=-10000;
  MinValue:=10000;
  for i:=0 to j do
        with CurAnimClip.Keys[i] do begin
        if EditAnim.Down and (ActiveMesh<>nil)
          and (CurAnimClip.Keys[i].keytype.objname<>ActiveMesh.name)
          then Continue;
        if (SelectType<>0) and  (SelectType<>CurAnimClip.Keys[i].keytype.ktype) then
           Continue;
        CurAnimClip.SortKeyData(@CurAnimClip.Keys[i]);
        jj:=Length(data)-1;
                for ii:=0 to jj do begin
        //         if data[ii][4]>CurAnimClip.Time then begin
       //          CurAnimClip.Time:=data[ii][4];
      //           LabelTime.Caption := Format('%.2f sec', [CurAnimClip.Time]);
       //          end;
                 if data[ii][5]>MaxValue then MaxValue:=data[ii][5];
                 if data[ii][5]<MinValue then MinValue:=data[ii][5];
                end;
        end;
  if MaxValue=-10000 then MaxValue:=1;
  if MinValue=10000 then MinValue:=-1;
  if MinValue =MaxValue then MinValue:=MinValue-1;
  If not GpMove then begin
  TrackPanel1.AreaSize.MaxHeight:=MaxValue;
  TrackPanel1.AreaSize.MinHeight:=MinValue;
  end;
  AnimTimer.MaxTime := CurAnimClip.Time;
  TrackPanel1.Repaint
end;

procedure TFormXom.AnimBoxChange(Sender: TObject);
begin
  // update timer
  AnimTimer.StartTimer;
  CurAnimClip := AnimClips.GetItemID(AnimBox.ItemIndex);
  ShowGraph:=True;
  UpdateGraph;
  LabelTime.Caption := Format('%.2f sec', [AnimTimer.MaxTime]);
end;

procedure TFormXom.ImportAnimClick(Sender: TObject);
var id:integer;
begin
//  Application.MessageBox('This version for test.', 'Testing', MB_OK);
  if LoadAnim.Execute and (LoadAnim.FileName <> '') then
    begin
    CurAnimClip.Clear;
    CurAnimClip.LoadXAnimClip(LoadAnim.FileName);
    CurAnimClip.SortKeys;
    id:=AnimBox.ItemIndex;
    AnimBox.Items[id]:=CurAnimClip.Name;
    AnimBox.ItemIndex:=id;
        ActiveMesh:=nil;
        SelectObj := 0;
        SelectKey := nil;
        SelectType:= 0;
        TrackPanel1.Repaint;
     If EditAnim.Down then
     begin
      UpdateAnimTree(TreeView2);
      UpdateAnimTreeMenu(Add1,Delete1);
     end;
    AnimBoxChange(Sender);
    end;
end;

procedure TFormXom.OpenGLBoxMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  Handled:=True;
  TransView.zoom := TransView.zoom + TransView.zoom * 0.1;
   If not Timer1.Enabled then OpenGLBox.Repaint;
end;

procedure TFormXom.OpenGLBoxMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  Handled:=True;
  TransView.zoom := TransView.zoom - TransView.zoom * 0.1;
   If not Timer1.Enabled then OpenGLBox.Repaint;
end;

procedure TFormXom.Zoomer(Step: Extended);
var
  sinX, cosX, sinY, cosY: Extended;
begin
  SinCos((TransView.yrot - 90) * Pi / 180, sinY, cosY);
  SinCos((TransView.xrot - 90) * Pi / 180, sinX, cosX);
  Transview.ypos := Transview.ypos + Step * cosX;
  Transview.xpos := Transview.xpos + Step * cosY * sinX;
  Transview.zpos := Transview.zpos + Step * sinY * sinX;
end;

procedure TFormXom.OpenGLBoxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  CurPoint: TPoint;
  sinX, cosX, sinY, cosY, Step: Extended;
  hit: Integer;
  Text: string;

begin
  if OpenGLBox.OnOpenGL then
  begin
    Text := '';
    //if not (ssMiddle in Shift) and (TempCur<>OpenGLBox.Cursor) then OpenGLBox.Cursor:=TempCur;
    if (ssLeft in Shift) or (ssMiddle in Shift) then
    begin
      Ctrl:=ssCtrl in Shift;
      ShiftOn:=ssShift in Shift;
      GetCursorPos(CurPoint);
      CurPoint.X := TempX - CurPoint.X;
      CurPoint.Y := TempY - CurPoint.Y;
      //if (CurPoint.X=0) and (CurPoint.Y=0) then exit;

      case OpenGLBox.Cursor of
        crDolly:
        begin
          Zoomer(-CurPoint.Y / 10);
      //    Text := Format('%s [X:%.2f Y:%.2f Z:%.2f]',
        //    [Translate('[sCameraPosition]'),Transview.xpos, Transview.ypos, Transview.zpos]);
        end;
        crPan:
        begin
      {   PTarget.X:=CurPoint.X;
          PTarget.Y:=CurPoint.Y;
          BPan:=true;        }
          SinCos((TransView.yrot - 90) * Pi / 180, sinY, cosY);
          SinCos(TransView.xrot * Pi / 180, sinX, cosX);
          Step := CurPoint.X / GLwidth * (TransView.zoom) *
            DegToRad(TransView.Per);
          Transview.xpos := Transview.xpos - Step * sinY;
          Transview.zpos := Transview.zpos + Step * cosY;
          Step := CurPoint.Y / GLheight * (TransView.zoom) *
            DegToRad(TransView.Per);
          Transview.ypos := Transview.ypos + Step * cosX;
          Transview.zpos := Transview.zpos + Step * sinY * sinX;
          Transview.xpos := Transview.xpos + Step * cosY * sinX;
       //   Text := Format('%s [X:%.2f Y:%.2f Z:%.2f]',
         //   [Translate('[sCameraPosition]'),Transview.xpos, Transview.ypos, Transview.zpos]);
        end;
        crRotXY:
        begin
          Transview.Xrot := Transview.Xrot + CurPoint.Y / 5;
          Transview.yrot := Transview.yrot - CurPoint.X / 5;
       //   Text := Format('%s [X:%.2f� Y:%.2f�]',
         //   [Translate('[sCameraRotation]'),Transview.Xrot, Transview.yrot]);
        end;
        crPer:
        begin
          TransView.Per := TransView.Per - CurPoint.Y / 10;
         // Text := Format('%s [%.2f�]', [Translate('[sCameraPerspective]'),TransView.Per]);
        end;
        crZoom:
        begin //UpdateActiveWindow:=true;
          TransView.zoom := TransView.zoom - CurPoint.Y / 10;
        //  Text := Format('%s [%.2f]', [Translate('[sCameraZoom]'),TransView.zoom]);
        end;
        crMove:
        begin
          PTarget.X :=X;
          PTarget.Y:=GLheight-Y-1;
     //     Text := Format('%s [X:%.2f Y:%.2f Z:%.2f]', [Translate('[sMove]'),MovePoxPos.X+wdup.x, MovePoxPos.Y+wdup.y, MovePoxPos.Z+wdup.z]);
        end;
        crRotate:
        begin
          PTarget.X := X;
          PTarget.Y:=GLheight-Y-1;
     //     Text := Format('%s [X:%.2f Y:%.2f Z:%.2f]', [Translate('[sRotate]'),RotatePoxPos.x, RotatePoxPos.y, RotatePoxPos.z]);
        end;
        crScale:
        begin
          PTarget.X := X;
          PTarget.Y:=GLheight-Y-1;
   //       Text := Format('%s [X:%.2f Y:%.2f Z:%.2f]', [Translate('[sScale]'),ScalePoxPos.X*wdup.x, ScalePoxPos.X*wdup.y, ScalePoxPos.X*wdup.z]);
        end;
      end;


      GetCursorPos(CurPoint);
      TempX := CurPoint.X;
      TempY := CurPoint.Y;
      if CurPoint.X > Scrn.X - BrdScr then
      begin 
        TempX := BrdScr;
        setcursorpos(TempX, CurPoint.y);
      end;
      if CurPoint.X < BrdScr then 
      begin 
        TempX := Scrn.X - BrdScr;
        setcursorpos(TempX, CurPoint.y);
      end;
      if CurPoint.y > Scrn.Y - BrdScr then 
      begin 
        TempY := BrdScr;
        setcursorpos(CurPoint.x, TempY);
      end;
      if CurPoint.y < BrdScr then 
      begin
        TempY := Scrn.Y - BrdScr;
        setcursorpos(CurPoint.x, TempY);
      end;
       If not Timer1.Enabled then OpenGLBox.Repaint;
    end
    else
      case OpenGLBox.Cursor of
        crDefault, crSelect, crFill,crErase,
        crGet, crMove, crRotate, crScale:
        begin
        //  Text := Translate('[sSelectMode]');

          if ToolButton8.Down then
          begin
     //       Text := Translate('[sMoveMode]');
            MoveMode := true;
          end;
          if ToolButton9.Down then
          begin
      //      Text := Translate('[sRotateMode]');
            RotateMode := true;
          end;
          if ToolButton10.Down then
          begin
     //       Text := Translate('[sScaleMode]');
            ScaleMode := true;
          end;

          GetCursorPos(CurPoint);
          if (TempX = CurPoint.X) and (TempY = CurPoint.Y) then
            Exit;
          TempX := CurPoint.X;
          TempY := CurPoint.Y;
          // ������������ ����
          hit:=0;
     //     if TestView(x,y) then
          if EditAnim.Down then
          hit := DoSelect(X, Y);


          if hit > 0 then
          begin

            if ToolButton8.Down then
            begin
              if hit<10000 then begin OpenGLBox.Cursor :=crDefault;  exit;end;
              OpenGLBox.Cursor := crMove;
           {   case hit of
                clrX: s:='X';
                clrY: s:='Y';
                clrZ: s:='Z';
                clrXY: s:='XY';
                clrYZ: s:='YZ';
                clrXZ: s:='XZ';
                else
               // s:=Translate('[sObject]');
              end;
              Text := Format('%s [%s]', [Translate('[sMoveMode]'),s]);  }
              if hit>10000 then  begin
                AAxis:=TAxis(hit-10001);
                OpenGLBox.Repaint;
                end else AAxis:=MAxis;
              //Text := 'Move Mode [On]';
            end
            else if ToolButton9.Down then
            begin
              if hit<10000 then begin OpenGLBox.Cursor :=crDefault;  exit;end;
              OpenGLBox.Cursor := crRotate;
           {   case hit of
                clrX: s:='X';
                clrY: s:='Y';
                clrZ: s:='Z';
                clrXY: s:='XY';
                clrYZ: s:='YZ';
                clrXZ: s:='XZ';
                else s:=Translate('[sObject]');
              end;
              Text := Format('%s [%s]', [Translate('[sRotateMode]'),s]);   }
              if hit>10000 then  begin
                AAxis:=TAxis(hit-10001);
                OpenGLBox.Repaint;
                end else AAxis:=MAxis;
              //Text := 'Move Mode [On]';
            end
            else if ToolButton10.Down then
            begin
              if hit<10000 then begin OpenGLBox.Cursor :=crDefault;  exit;end;
              OpenGLBox.Cursor := crScale;
       {       case hit of
                clrX: s:='X';
                clrY: s:='Y';
                clrZ: s:='Z';
                clrXY: s:='XY';
                clrYZ: s:='YZ';
                clrXZ: s:='XZ';
                clrXYZ: s:='XYZ';
                else s:=Translate('[sObject]');
              end;
              Text := Format('%s [%s]', [Translate('[sScaleMode]'),s]);    }
              if hit>10000 then  begin
                AAxis:=TAxis(hit-10001);
                OpenGLBox.Repaint;
                end else AAxis:=MAxis;
              //Text := 'Move Mode [On]';
            end
            else
            begin
              OpenGLBox.Cursor := crSelect;
              begin
            //  Text := Format('%s [%d]', [Translate('[sSelectMode]'),hit]);
              RotateMode := false;
              ScaleMode := false;
              MoveMode := false;
              SelectObjOn := hit;
              end;
            end;
          end
          else
            OpenGLBox.Cursor := crDefault;
          //Label4.Caption:=format('Select: %d',[hit]);
          //  if SelectObj>0 then display(time,false);
        end;
    {    crDolly:
          Text := Translate('[sCameraDollyMode]');
        crPan:
          Text := Translate('[sCameraPanMode]');
        crZoom:
          Text := Translate('[sCameraZoomMode]');
        crRotXY:
          Text := Translate('[sCameraRotationMode]');
        crPer:
          Text := Translate('[sCameraPerspectiveMode]');    }
      end;
    StatusBar1.Panels.Items[0].Text := Text;
    //
  end;
end;

procedure TFormXom.OpenGLBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case Key of
  VK_NUMPAD7:  TransView.xrot:=TransView.xrot+10;
  VK_NUMPAD1:  TransView.xrot:=TransView.xrot-10;
  VK_NUMPAD8:  TransView.zpos:=TransView.zpos+10;
  VK_NUMPAD2:  TransView.zpos:=TransView.zpos-10;
  VK_NUMPAD4:  TransView.xpos:=TransView.xpos+10;
  VK_NUMPAD6:  TransView.xpos:=TransView.xpos-10;
  VK_NUMPAD9:  TransView.Per:=TransView.Per+10;
  VK_NUMPAD3:  TransView.Per:=TransView.Per-10;
  end;
         if (Key=Ord('C')) and (ToolButton6.Enabled) then ToolButton6Click(ToolButton6);
         if (Key=Ord('Q')) then ToolButton11Click(ToolButton11);
         if (Key=Ord('W')) and (ToolButton8.Enabled)then ToolButton1Click(ToolButton8);
         if (Key=Ord('E')) and (ToolButton9.Enabled)then ToolButton1Click(ToolButton9);
         if (Key=Ord('R')) and (ToolButton10.Enabled)then ToolButton1Click(ToolButton10);
         if (Key=Ord('X')) then    Zoomer(+1);
         if (Key=Ord('S')) then    Zoomer(-1);
   If not Timer1.Enabled then  OpenGLBox.Repaint;
end;

procedure TFormXom.TrackPanel1DrawGraph(Canvas: TCanvas; Rect: TRect; XDiv,
  YDiv: Single);
  var
  i,ii,j,jj,k:integer;
    P:Array of TPoint;
    P1,P2:TPoint;
 //   PushColor:TColor;
    TmRect:TRect;
  CurActiveMesh:Boolean;
  NumCurves:integer;
begin
 if ShowGraph and (CurAnimClip<>nil) then begin
 j:=Length(CurAnimClip.Keys)-1;
 SetLength(GrafKeys,0);
 NumCurves:=0;
// AnimClip.Time                        
  for i:=0 to j do
        with CurAnimClip.Keys[i] do begin
          CurActiveMesh:=(ActiveMesh<>nil)
          and (CurAnimClip.Keys[i].keytype.objname<>ActiveMesh.name);

          if EditAnim.Down and CurActiveMesh then
                Continue;

        if (SelectType<>0) and  (SelectType<>CurAnimClip.Keys[i].keytype.ktype) then
           Continue;
        Canvas.Pen.Color:=ColorHLSToRGB((i*13 mod 255), 190, 200);
        jj:=Length(data);
         SetLength(P,jj);
                for ii:=0 to jj-1 do begin
                P[ii].X:=Rect.Left+Round(XDiv*data[ii][4]);
                P[ii].Y:=Rect.Top+Round(+YDiv*TrackPanel1.AreaSize.MaxHeight-YDiv*data[ii][5]);
                end;
       //  Canvas.PolyLine(P);
        { work
                 for ii:=0 to jj-2 do
         Canvas.PolyBezier([
         Point(P[ii].x,P[ii].y),
         Point(P[ii].x+round((P[ii+1].x-P[ii].x)*data[ii][2]), P[ii].y+round((P[ii].y-P[ii+1].y)*data[ii][3])),
         Point(P[ii+1].x+round((P[ii].x-P[ii+1].x)*data[ii+1][0]), P[ii+1].y+round((P[ii+1].y-P[ii].y)*data[ii+1][1])),
         Point(P[ii+1].x,P[ii+1].y)
         ]);}

         for ii:=0 to jj-2 do begin
                P1.x := P[ii].x + round((P[ii+1].x-P[ii].x)*cos(data[ii][3])*data[ii][2]/3);
                P1.y := P[ii].y + round((P[ii+1].y-P[ii].y)*sin(data[ii][3])*data[ii][2]/3);
                P2.x := P[ii+1].x - round((P[ii+1].x-P[ii].x)*cos(data[ii+1][1])*data[ii+1][0]/3);
                P2.y := P[ii+1].y - round((P[ii+1].y-P[ii].y)*sin(data[ii+1][1])*data[ii+1][0]/3);
         Canvas.PolyBezier([P[ii],P1,P2,P[ii+1]]);
        {   PushColor:=Canvas.Pen.Color;
       Canvas.Pen.Color:=clYellow;
         Canvas.PolyLine([P[ii],P1]);
         Canvas.PolyLine([P[ii+1],P2]);
         Canvas.Pen.Color:=PushColor; }
         end;
         if EditAnim.Down then begin
         Canvas.Brush.Color:=clLime;
          k:=Length(GrafKeys);
        if  ActiveMesh<>nil then begin
                SetLength(GrafKeys,k+jj);
                SelectKdata:=@CurAnimClip.Keys[i];
                inc(NumCurves);
                end;
                for ii:=0 to jj-1 do begin
                  if SelectKey=@Data[ii] then Canvas.Brush.Color:=clRed
                  else Canvas.Brush.Color:=clLime;
                  TmRect:=Classes.Rect(P[ii].X-3,P[ii].Y-3,P[ii].X+3,P[ii].Y+3);
                if  ActiveMesh<>nil then begin
                 GrafKeys[k+ii].Frame:=@Data[ii];
                 GrafKeys[k+ii].Rect:=TmRect;
                  end else Canvas.Brush.Color:=clYellow;
                 Canvas.FillRect(TmRect);
                 Canvas.FillRect(Classes.Rect(P[ii].X-3,P[ii].Y-3,P[ii].X+3,P[ii].Y+3));
                 end;
         end;
        end;

   if NumCurves<>1 then SelectKdata:=nil;
 end;
end;


procedure TFormXom.EditAnimClick(Sender: TObject);
var TmClip:TAnimClip;
id:integer;
New:boolean;
begin
If EditAnim.Down then begin
Timer1.Enabled:=false;
new:=false;
Case AnimEditForm.FormAsk.ShowModal of
2:      begin
        TmClip:=TAnimClip.Create;
        TmClip.Copy(CurAnimClip);
        TmClip.Name:=AnimEditForm.FormAsk.Edit1.Text;
        id:=AnimClips.AddClip(TmClip); // clone;
        AnimBox.AddItem(TmClip.Name,nil);
        AnimBox.ItemIndex:=id;
        CurAnimClip:=TmClip;
        New:=true;
        end;
3:      begin
        TmClip:=TAnimClip.Create;
        // add zero keys;
        TmClip.Time:=1.0;
        TmClip.Name:=AnimEditForm.FormAsk.Edit1.Text;
        id:=AnimClips.AddClip(TmClip); // ;
        AnimBox.AddItem(TmClip.Name,nil);
        AnimBox.ItemIndex:=id;
        CurAnimClip:=TmClip;
        New:=true;
        end;
end;
ActiveMesh:=nil;
        SelectObj := 0;
        SelectKey := nil;
        SelectType:= 0;
TrackPanel1.CanSlide:=true;
TrackPanel1.Repaint;
RotateBut.Down:=False;
RotateBut.Enabled:=False;

Panel2.Visible:=False;
AnimBox.Enabled:=False;
AnimButton.Enabled:=False;
ImportAnim.Enabled:=False;
ExportAnim.Enabled:=False;
ToolButton8.Enabled :=False;
ToolButton9.Enabled :=False;
ToolButton10.Enabled :=False;
TabSheet2.TabVisible:=False;
TabSheet3.TabVisible:=False;
Splitter1.Visible:=true;
TreeView2.Width:=220;
UpdateAnimTree(TreeView2);
UpdateAnimTreeMenu(Add1,Delete1);
TreeView2.Visible:=True;
AnimEd:=false or New;
// ������ �������� � �������� ������ �����������
// ��� ��������� �������� �� ������ ��������� ����������� ����, ������� ����� ����� ������� � ��������
end else
begin
TrackPanel1.CanSlide:=False;
TrackPanel1.Slider:=0;
ActiveMesh:=nil;
        SelectObj := 0;
        SelectKey := nil;
        SelectType:= 0;
TrackPanel1.Repaint;
Timer1.Enabled:=True;
Panel2.Visible:=True;
AnimBox.Enabled:=True;
RotateBut.Enabled:=True;
AnimButton.Enabled:=True;
ImportAnim.Enabled:=True;
ExportAnim.Enabled:=True;
TabSheet2.TabVisible:=True;
TabSheet3.TabVisible:=True;
ToolButton8.Down :=False;
ToolButton9.Down :=False;
ToolButton10.Down :=False;
ToolButton8.Enabled :=False;
ToolButton9.Enabled :=False;
ToolButton10.Enabled :=False;
TreeView2.Visible:=False;
TreeView2.Width:=200;
Splitter1.Visible:=False;
if AnimEd then begin
UpdateGraph;
id:=AnimBox.ItemIndex;
// ������������� ������� ��������
AnimClips.SaveAnimToXom(Xom);
LoadXom:=False;
TreeView1Change(Self,Xom.BaseNode);
LoadXom := true;
ButSaveXom.Enabled := True;
AnimBox.ItemIndex:=id;
AnimBoxChange(self);
end;
end;

end;

procedure TFormXom.ToolButton13Click(Sender: TObject);
begin
  ToolButton13.Down := false;
  ToolButton14.Down := false;
  ToolButton15.Down := false;
  ToolButton16.Down := false;
  ToolButton17.Down := false;
  ToolButton18.Down := false;
  TToolButton(Sender).Down := true;
  MAxis:=TAxis(TToolButton(Sender).Index-10);
  AAxis:=MAxis;
   If not Timer1.Enabled then  OpenGLBox.Repaint;
end;

procedure TFormXom.ToolButton1Click(Sender: TObject);
begin
  if (TToolButton(Sender) = ToolButton8) and (MAxis = Axis_XYZ) then   ToolButton13Click(ToolButton13);
  if ToolButTemp = TToolButton(Sender) then
  begin
    ToolButTemp.Down  := false;
    if (ToolButTemp2 <> nil) and(ToolButTemp2.Enabled) then
        begin
        if (ToolButTemp2 = ToolButton8) or
             (ToolButTemp2 = ToolButton9) or
             (ToolButTemp2 = ToolButton10) then begin ToolButton1Click(ToolButTemp2); exit; end
         else ToolButTemp2 := nil;
        end;
    OpenGLBox.Cursor  := crDefault;
    ToolButton11.Down := true;
    ToolButTemp       := ToolButton11;
  end
  else
  begin
    TToolButton(Sender).Down := true;
    OpenGLBox.Cursor         := TToolButton(Sender).Index + 1;
    if ToolButTemp <> nil then
      ToolButTemp.Down := false;
    ToolButTemp2 := ToolButTemp;
    ToolButTemp := TToolButton(Sender);
    TempCur     := OpenGLBox.Cursor;
  end;

 {  if (TToolButton(Sender) = ToolButton8) or
             (TToolButton(Sender) = ToolButton9) or
             (TToolButton(Sender) = ToolButton10) then  HmPickBut.Down:= false;
//  if TToolButton(Sender)=ToolButton8 then HmPickBut.Down:= false;    }
   If not Timer1.Enabled then  OpenGLBox.Repaint;
end;

procedure TFormXom.ToolButtonSMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar1.Panels.Items[0].Text := TToolButton(Sender).Hint;
end;

procedure TFormXom.OpenGLBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  CurPoint: TPoint;
 // b, a, t, q: Integer;

begin
  if OpenGLBox.OnOpenGL then
  begin
    if Button = mbLeft then
    begin
      if OpenGLBox.Cursor = crDefault then
      if ToolButton8.Down or  ToolButton9.Down or
        ToolButton10.Down
        then else
      begin
        SelectObj := 0;
        SelectKey := nil;
        SelectType:= 0;
     //   ToolButton6.Enabled := false;
        ActiveMesh:=nil;

        if EditAnim.Down then begin
         UpdateAnimTree(TreeView2);
         UpdateGraph;
        ToolButton8.Enabled :=EdMode;
        ToolButton9.Enabled :=EdMode;
        ToolButton10.Enabled :=EdMode;
         end;
        OpenGLBox.Repaint;
      end;

      if OpenGLBox.Cursor = crSelect then
        if SelectObjOn > 0 then
        begin

          begin
          SelectObj := SelectObjOn;
    //      ToolButton6.Enabled := true;
          OpenGLBox.Repaint;
        If ActiveMesh <> nil then begin
        UpdateAnimTree(TreeView2);
        UpdateGraph;
        EdMode:=ActiveMesh.Transform.TransType<>TTNone;
        ToolButton8.Enabled :=EdMode;
        ToolButton9.Enabled :=EdMode;
        ToolButton10.Enabled :=EdMode;
        end;
    {      if ActivePoxel <> nil then
          begin
            if ImportMapActive then
            begin
              Label4.Caption := ActivePoxel.Name;
              TreeView3.Selected := ActivePoxel.TreeNodeLink;
              RefrashList(ActivePoxel);
            end
            else if SelectPoxel<>ActivePoxel then
            begin
              SelectLabel.Caption := ActivePoxel.Name;
              TreeView1.Selected := ActivePoxel.TreeNodeLink;
              RefrashList(ActivePoxel);
              SelectDone;
            end;
          end;    }
          end;
        end
        else

        begin
      //    SelectLabel.Caption := 'None';
          SelectObj := 0;
      //    ToolButton6.Enabled := false;
                ToolButton8.Enabled :=False;
                ToolButton9.Enabled :=False;
                ToolButton10.Enabled :=False;
                ActiveMesh:=nil;
          OpenGLBox.Repaint;
        end;

 {     if MoveMode and (SelectObj=0) then
      begin
      ToolButton11Click(ToolButton11);
      end;  }

      case OpenGLBox.Cursor of
      crMove,crRotate,crScale:
        case AAxis of
                Axis_X: ToolButton13Click(ToolButton13);
                Axis_Y: ToolButton13Click(ToolButton14);
                Axis_Z: ToolButton13Click(ToolButton15);
                Axis_XY: ToolButton13Click(ToolButton16);
                Axis_YZ: ToolButton13Click(ToolButton17);
                Axis_XZ: ToolButton13Click(ToolButton18);
                Axis_XYZ: begin
                        ToolButton13.Down := false;
                        ToolButton14.Down := false;
                        ToolButton15.Down := false;
                        ToolButton16.Down := false;
                        ToolButton17.Down := false;
                        ToolButton18.Down := false;
                        MAxis:=Axis_XYZ;
                        AAxis:=MAxis;
                        OpenGLBox.Repaint;
                               // ToolButton13Click(ToolButton18)
                        end;
                end;
      end;

      if OpenGLBox.Cursor = crMove then
      begin
        MoveReady := true;
        MoveFirts:=true;
      end
      else
        MoveMode := false;

      if OpenGLBox.Cursor = crScale then
      begin
        ScaleReady := true;
        ScaleFirts:=true;
      end
      else
        ScaleMode := false;

      if OpenGLBox.Cursor = crRotate then
      begin
        RotateReady := true;
        RotateFirts:=true;
      end
      else
        RotateMode := false;
        
      GetCursorPos(CurPoint);
      TempX := CurPoint.X;
      TempY := CurPoint.Y;
    end;
    if Button = mbMiddle then
    begin
      GetCursorPos(CurPoint);
      TempX := CurPoint.X;
      TempY := CurPoint.Y;
      TempCur := OpenGLBox.Cursor;
      OpenGLBox.Cursor := crPan;
    end;
    if Button = mbRight then
      ToolButton1Click(ToolButton3);
  end;
end;

procedure TFormXom.OpenGLBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
//var
//TempPoxel:TPoxel;
//tempTN : TTreeNode;
//k:Single;
//i,j:integer;

begin
  if mbMiddle = Button then
    OpenGLBox.Cursor := TempCur;

   if (CurAnimClip<>nil) and (MoveReady or
   ScaleReady or RotateReady) then
   begin
   CurAnimClip.UpdateAnimClip;
   UpdateGraph;
   if EditAnim.Down then UpdateAnimTree(TreeView2);
   end;

   MoveReady:=false;
   ScaleReady:=false;
   RotateReady:=false;
      If not Timer1.Enabled then  OpenGlBox.Repaint;
 //     TrackPanel1.Repaint;
      ;
      OpenGLBox.Repaint;
end;

procedure TFormXom.OpenGLBoxClick(Sender: TObject);
begin
  OpenGLBox.OnOpenGL := true;
end;

procedure TFormXom.ToolButton6Click(Sender: TObject);
begin
if Xom.Mesh3D<>nil then
 TransView:=TVModel else begin
  TransView.xpos:=0;
  TransView.ypos:=0;
  TransView.zpos:=0;
  TransView.xrot := -20.0;
  TransView.yrot := 136.0;
  TransView.Per := 35.0;
  TransView.zoom := 50.0;
  end;
   If not Timer1.Enabled then  OpenGlBox.Repaint;
end;

procedure TFormXom.ShowXTypesClick(Sender: TObject);
begin
 if ShowXTypes.Down then begin
  TreeView1.Visible:=true;
 end else
 begin

 TreeView1.Visible:=false;
  Panel2.Width:= Panel2.Width-TreeView1.Width;
 end;

end;

var
Edited:Boolean;

procedure TFormXom.StringsMenuPopup(Sender: TObject);
var
Enable:Boolean;
Index:integer;
Str:String;
begin
Index:=StringsTable.Selection.Top;
Str:=StringsTable.Cells[1,Index];
StringST.Caption:=Str;
Enable:=Str<>'';
StringST.Enabled:=Enable;
EditST.Enabled:=Enable;
AddNewST.Enabled:=Enable;
end;

procedure TFormXom.StringSTClick(Sender: TObject);
var
Index:integer;
Str:String;
begin
Index:=StringsTable.Selection.Top;
Str:=StringsTable.Cells[1,Index];
ClipBoard.AsText:=Str;
end;

procedure TFormXom.EditSTClick(Sender: TObject);
//var
//Index:integer;
//Str:String;
begin
//Index:=StringsTable.Selection.Top;
StringsTable.Options:=StringsTable.Options+[goEditing];
StringsTable.EditorMode:=true;
end;

procedure TFormXom.AddNewSTClick(Sender: TObject);
var
Index:integer;
S:String;
Rect:TGridRect;
begin
Index:=StringsTable.RowCount;
    s := Format('%.2x', [byte128(Index + 1)]);
Index:=StringsTable.InsertRow(s, 'NewString', true);
Rect:=STringsTable.Selection;
Rect.Top:=Index;
Rect.Bottom:=Index;
STringsTable.Selection:=Rect;
StringsTable.Options:=StringsTable.Options+[goEditing];
StringsTable.EditorMode:=true;
Edited:=true;
end;

procedure TFormXom.StringsTableSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
begin
StringsTable.Options:=StringsTable.Options-[goEditing];
if Value<>StringsTable.Cells[ACol,ARow] then
Edited:=true;

end;

procedure TFormXom.StringsTableSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
If Edited then begin
LoadXom:=False;
TreeView1Change(Self,Xom.BaseNode);
LoadXom := true;
ButSaveXom.Enabled := True;
end;
Edited:=false;

end;

procedure TFormXom.ToolButton11Click(Sender: TObject);
begin
  ToolButTemp.Down := false;
  OpenGLBox.Cursor := crDefault;
  ToolButton11.Down := true;
//  HmPickBut.Down:=false;
  ToolButTemp := ToolButton11;
   If not Timer1.Enabled then  OpenGLBox.Repaint;
end;

procedure TFormXom.TrackPanel1Paint(Sender: TObject);
begin
     If not Timer1.Enabled then OpenGLBox.Repaint;
end;


procedure TFormXom.TrackPanel1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
 Key:TGrafKey;
 i,n:integer;
 Found:Boolean;
 CurPos:TPoint;
 fY:Single;
begin
 Found:=false;

 if TrackPanel1.Cursor=crDefault then
 If GrafKeys<>nil then begin
   n:=Length(GrafKeys);
   for i:=0 to n-1 do
    if (GrafKeys[i].Rect.Left<X) and  (GrafKeys[i].Rect.Right>X) and
      (GrafKeys[i].Rect.Top<Y) and  (GrafKeys[i].Rect.Bottom>Y) then
       begin
       Key:=GrafKeys[i];
       Found:=true;
       Break;
       end;

 if Found then begin
        if  (Key.Frame=SelectKey)  then
        begin
        TrackPanel1.Cursor:=crMove;
        end
        else begin
        TrackPanel1.Cursor:=crSelect;
        SKey:=Key.Frame;
        end;
 end;
 end;

 if GpMove and (SelectKey<>nil) then begin
        GetCursorPos(CurPos);
        if X<40 then  SelectKey[4]:=0.0 else
                SelectKey[4]:=TrackPanel1.GetFloatX;
        fY:=TrackPanel1.GetFloatY;
        If ChillMode and ((fY>ChillMax) or (fY<0))
        then
        // error
        else
        SelectKey[5]:=fY;
        UpdateGraph;
 end;
 if EditAnim.Down  and (not TimeDrag) and
 (X<TrackPanel1.Width) and  (X>40) and
 (Y<TrackPanel1.Height) and  (Y>TrackPanel1.Height-12) then
    TrackPanel1.Cursor:=crOpenHand;

 if TimeDrag then  begin
 if X<41 then X:=41;
 CurAnimClip.Time:=FdTime*((TrackPanel1.Width-40)/(X-40));
 LabelTime.Caption := Format('%.2f sec', [CurAnimClip.Time]);
 UpdateGraph;
 end;

end;

procedure TFormXom.TrackPanel1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
case  TrackPanel1.Cursor of
crSelect:
 begin
   SelectKey:=SKey;
   UpdateGraph;
 //  TrackPanel1.Repaint;
   SKey:=nil;
 // select
 end;
crMove:
 begin
 GpMove:=true;
 end;
crDefault:
 begin
   SelectKey:=nil;
 //  TrackPanel1.Repaint;
 end;
crOpenHand:
 begin
 TimeDrag:=true;
 FdTime:=TrackPanel1.GetFloatX;
 TrackPanel1.Cursor:=crClosedHand;
 end;
end;

end;

procedure TFormXom.TrackPanel1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 // if TrackPanel1.Cursor=crSelect then
 if TimeDrag then
 TimeDrag:=false;

 If GpMove then begin
 UpdateAnimTree(TreeView2);
  GpMove:=false;
 UpdateGraph;
 end;

end;

procedure TFormXom.ClassTreeCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
if not ClassTree.Focused and Node.Selected   then
  TCustomTreeView(Sender).Canvas.Font.Color := clGreen;
end;

var
NeedUpdate:Boolean;
MainNodeSelect:Boolean;

procedure TFormXom.TreeView2Changing(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
// ��������� �������
MainNodeSelect:=Node.Level=0;
if (Node.Data<>nil) then
        begin
        case Node.level of
        1: if (TMesh(Node.Data) is TMesh) then
        begin
        SelectType:=0;
        ActiveMesh:=TMesh(Node.Data);
        SelectObj:=ActiveMesh.Indx;
        OpenGLBox.Repaint;
        ToolButton8.Enabled :=True;
        ToolButton9.Enabled :=True;
        ToolButton10.Enabled :=True;
        UpdateGraph;
        NeedUpdate:=true;
        end;
        3:
        begin
        SelectType:=Integer(Node.Data);
        SelectObjName:=Node.Parent.Parent.Text;
        TrackPanel1.Repaint;
        UpdateGraph;
        NeedUpdate:=true;
        end;
        4:
        begin
        SelectKey:=Node.Data;
        TrackPanel1.Slider:=SelectKey[4];
        TrackPanel1.Repaint;
        end;

        end;
   end;
end;


procedure TFormXom.TreeView2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if   NeedUpdate then
        UpdateAnimTree(TreeView2);
        NeedUpdate:=False;
        ToolButton8.Enabled :=EdMode;
        ToolButton9.Enabled :=EdMode;
        ToolButton10.Enabled :=EdMode;
end;

procedure TFormXom.TrackPanel1DblClick(Sender: TObject);
begin
if (TrackPanel1.Cursor= crMove) then
        begin
        // delete SelectKey;
        CurAnimClip.DeleteKey(SelectKey);
        SelectKey:=nil;
        GpMove:=False;
        UpdateAnimTree(TreeView2);
        UpdateGraph;
        exit;
        end;

if (TrackPanel1.Cursor= crDefault)and(SelectKdata<>nil) then
        begin
        CurAnimClip.AddKey(SelectKdata,TrackPanel1.GetFloatX,0,TrackPanel1.GetFloatY,SelectType);
        UpdateAnimTree(TreeView2);
        UpdateGraph;
        end;
end;

procedure TFormXom.TreeView2DblClick(Sender: TObject);
begin
if (MainNodeSelect) then
begin
                SelectObj := 0;
                SelectKey := nil;
                SelectType:=0;
                MoveMode:=False;
                RotateMode:=False;
                ScaleMode:=False;
                ActiveMesh:=nil;
                EdMode:=false;
                UpdateAnimTree(TreeView2);
                UpdateGraph;
                ToolButton8.Down :=false;
                ToolButton9.Down :=false;
                ToolButton10.Down :=false;
                ToolButton8.Enabled :=EdMode;
                ToolButton9.Enabled :=EdMode;
                ToolButton10.Enabled :=EdMode;
                OpenGLBox.Repaint;
        end;
end;

procedure TFormXom.DrawBonesClick(Sender: TObject);
begin
OpenGLBox.Repaint;
end;

procedure TFormXom.Copy1Click(Sender: TObject);
var
  TextBuffer: string;
  i, j: Integer;
  KData:PKeyData;
begin
  TextBuffer := '';
  //FloatToStrF(LValClip,ffFixed,16,6);
  if SelectType <> 0 then
    begin
      KData:=CurAnimClip.FindKeyNameType(SelectObjName,SelectType);
      j:=Length(KData.Data);
      for i := 0 to j-1 do
      begin
        TextBuffer := TextBuffer + Format('%.6f%s%.6f%s%.6f%s%.6f%s%.6f%s%.6f%s',
          [KData.Data[i][0], Char(#09), KData.Data[i][1],
          Char(#09), KData.Data[i][2], Char(#09),
          KData.Data[i][3], Char(#09), KData.Data[i][4],
          Char(#09), KData.Data[i][5],
          Char(13) + Char(10)]);
      end;
      //
    end
    else
      Exit;
  ClipBoard.AsText := TextBuffer;
end;

procedure TFormXom.Paste1Click(Sender: TObject);
var
  TextBuffer: string;
  i, j, k: Integer;
  v: Extended;
  KData:PKeyData;
  tData:TKeyFrame;
  IsFloat:Boolean;
begin
  TextBuffer := ClipBoard.AsText;
  if (SelectType<>0) and (TextBuffer <> '') then
    begin
      KData:=CurAnimClip.FindKeyNameType(SelectObjName,SelectType);
      SetLength(KData.Data,0);
      j := 0;
      IsFloat:=true;
      While IsFloat do begin
       for i:=0 to 5 do begin
        if i=5 then k := Pos(Char(13), TextBuffer) else
        k := Pos(Char(#09), TextBuffer);
        IsFloat := TextToFloat(PChar(Copy(TextBuffer, 0, k - 1)), v, fvExtended);
        if not IsFloat then break;
        Delete(TextBuffer, 1, k);
        tData[i] := v;
        end;
        if not IsFloat then Break;
        Delete(TextBuffer, 1, 1);
        inc(j);
        SetLength(KData.Data,j);
        KData.Data[j-1]:= tData;
      end;
      UpdateGraph;
  end;
end;

procedure TFormXom.TrackPanel1ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
 // BufPoint: TPoint;
  TextBuffer: string;
  v: Extended;
begin
  TextBuffer := ClipBoard.AsText;
  Copy1.Enabled := (SelectType<>0);
  Paste1.Enabled := (SelectType<>0) and
    TextToFloat(PChar(Copy(TextBuffer, 0, Pos(Char(#09), TextBuffer) - 1)),
    v, fvExtended);
end;

procedure TFormXom.AddElement1Click(Sender: TObject);
begin
//
CurAnimClip.AddKData(TMenuItem(Sender).KeyType);
CurAnimClip.SortKeys;
UpdateAnimTreeMenu(Add1,Delete1);
MainNodeSelect:=True;
TreeView2DblClick(Sender);
end;

procedure TFormXom.DelElement1Click(Sender: TObject);
begin
//
CurAnimClip.DeleteKData(TMenuItem(Sender).KeyType);
UpdateAnimTreeMenu(Add1,Delete1);
MainNodeSelect:=True;
TreeView2DblClick(Sender);
end;

procedure TFormXom.Button7Click(Sender: TObject);
begin
//������
end;

procedure TFormXom.Button8Click(Sender: TObject);
begin
//�������
end;

procedure TFormXom.Changevalue1Click(Sender: TObject);
var
CntrVal:TCntrVal;
ParentNode,CurNode:TTreeNode;
begin
// �������� ��������
CntrVal:=TCntrVal(ClassTree.Selected.Data);
ValueForm.CntrVal:=CntrVal;
if ValueForm.ChgValForm.ShowModal=mrOk then
begin
// UpdateXom;
//StrArray[CntrVal.Cntr].Update:=true;
//ClassTree.Selected

//LoadXom:=False;
//TreeView1Change(Self,Xom.BaseNode);
//LoadXom := true;

ParentNode:=ClassTree.Selected.Parent;
ParentNode.DeleteChildren;
CurNode:=Xom.AddClassTree(CntrVal.Cntr, ClassTree, nil);
CurNode.MoveTo(ParentNode,naInsert);
ParentNode.Delete;
CurNode.Expand(true);

ButSaveXom.Enabled := True;
end;
end;

procedure TFormXom.PopupMenu1Popup(Sender: TObject);
var
Test:boolean;
CntrVal:TCntrVal;
begin
Test:=(integer(ClassTree.Selected.Data)>10000);
Changevalue1.Enabled:=Test;
if Test then
begin
  CntrVal:=TCntrVal(ClassTree.Selected.Data);
  Changevalue1.Caption:=format('Change [%s]',[CntrVal.IdName]);
end else
  Changevalue1.Caption:='Change value';
end;

end.
